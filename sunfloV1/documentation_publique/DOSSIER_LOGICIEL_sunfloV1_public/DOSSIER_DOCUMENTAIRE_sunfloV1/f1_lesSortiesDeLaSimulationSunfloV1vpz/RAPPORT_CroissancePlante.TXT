

*****************************************************************************
*
*
*   RAPPORT PRODUIT PAR CroissancePlante
*
*
*****************************************************************************
-----------------------------------------------------------------------------
LAI, indice foliaire, 
lors du  passage de la phase phenologique plante JUVENILE a CROISSANCEACTIVE (stade E1, bouton etoile)  : 2.01
Pour memo valeur complete : 2.01489
-----------------------------------------------------------------------------
LAI, indice foliaire, 
lors du  passage de la phase phenologique plante CROISSANCEACTIVE a FLORAISON (stade F1, debut floraison)  : 3.41
Pour memo valeur complete : 3.41317
-----------------------------------------------------------------------------
TDM, biomasse totale (g/m2) 
lors du  passage de la phase phenologique plante DESSICATION a RECOLTEE (qui correspond a la recolte)  : 1007
Pour memo valeur complete : 1007.31