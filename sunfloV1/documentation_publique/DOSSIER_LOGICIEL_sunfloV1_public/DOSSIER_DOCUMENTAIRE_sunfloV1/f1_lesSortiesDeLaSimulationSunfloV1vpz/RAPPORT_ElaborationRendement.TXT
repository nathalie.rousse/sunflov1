

*****************************************************************************
*
*
*   RAPPORT PRODUIT PAR ElaborationRendement
*
*
*****************************************************************************
-----------------------------------------------------------------------------
TDM, biomasse totale (g/m2) 
lors du  passage de la phase phenologique plante CROISSANCEACTIVE a FLORAISON (stade F1, debut floraison)  : 575
Pour memo valeur complete : 574.624
Il s'agit de vi.photo_TDM_CROISSANCEACTIVE_A_FLORAISON (ex TDMF1).
-----------------------------------------------------------------------------
INN, 
lors du  passage de la phase phenologique plante CROISSANCEACTIVE a FLORAISON (stade F1, debut floraison)  : 0.63
Pour memo valeur complete : 0.630873
Il s'agit de vs.photo_INN_CROISSANCEACTIVE_A_FLORAISON (ex INNF1).
-----------------------------------------------------------------------------
IRs, indice de recolte 
lors du  passage de la phase phenologique plante MATURATION a DESSICATION (stade M3)  : 0.369
Pour memo valeur complete : 0.369027
Il s'agit de vi.photo_IRs_aFinMATURATION (ex IR).
-----------------------------------------------------------------------------
RDT, rendement (q/ha)
lors du  passage de la phase phenologique plante MATURATION a DESSICATION (stade M3)  : 37.17
Pour memo valeur complete : 37.1724
Il s'agit de vi.photo_RDT_aFinMATURATION.
-----------------------------------------------------------------------------
Jours de stress cumules, 
les valeurs sont relevees lors du  passage de la phase phenologique plante DESSICATION a RECOLTEE (qui correspond a la recolte)  : 

JSE : 0
JSE correspond aux Jours de stress en phase vegetative (E1 - F1) (covariables statistiques).

JSF : 4
JSF correspond aux Jours de stress en floraison (F1 - M0) (covariables statistiques).

JSM : 15
JSM correspond aux Jours de stress après la floraison (M0 - M3) (covariables statistiques).