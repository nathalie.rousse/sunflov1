
Ce repertoire donne a titre illustratif les resultats d'une simulation du modele sunfloV1 selon le scenario sunfloV1.vpz (du paquet sunfloV1) :

- sorties numeriques : le fichier numerique SUNFLO_cmpMMpageDebug.csv d'observation de toutes les variables du modele sunfloV1

- sorties textuelles : les rapports textuels RAPPORT_... .TXT produits par la simulation (dont LE_RAPPORT_ENTIER.pdf est une compilation)

- sorties graphiques : le repertoire lesRepresentationsGraphiques des representations graphiques des variables du modele sunfloV1

- pour memo sauvegarde de la trace ecran tracesEcran.txt produite lors du deroulement de la simulation (obtenue par 'vle -P sunfloV1 sunfloV1.vpz > tracesEcran.txt)

- pour memo : le scenario vpz simule/execute sunfloV1.vpz
