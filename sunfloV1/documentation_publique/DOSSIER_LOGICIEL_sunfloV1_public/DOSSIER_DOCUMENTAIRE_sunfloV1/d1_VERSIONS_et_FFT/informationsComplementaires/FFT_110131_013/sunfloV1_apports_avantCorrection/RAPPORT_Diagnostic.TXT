

*****************************************************************************
*
*
*   RAPPORT PRODUIT PAR Diagnostic
*
*
*****************************************************************************
-----------------------------------------------------------------------------
Jours de stress cumules relatifs a la floraison, 
les valeurs sont relevees lors du  passage de la phase phenologique plante DESSICATION a RECOLTEE (qui correspond a la recolte)  (date a laquelle le calcul cumule est termine depuis longtemps) :

ISH1 : 5
ISH1 correspond aux Jours de stress jusqu'a la floraison 
(estimationTTentreSemisEtLevee_casPhaseSemisLeveeSimulee - F1).

ISH2 : 17
ISH2 correspond aux Jours de stress autour de la floraison
(F1 + 350 °Cd).

ISH3 28
ISH3 correspond aux Jours de stress après la floraison
(F1+ 350 °Cd - M3).
-----------------------------------------------------------------------------
******************************************************* Fin de rapport ******