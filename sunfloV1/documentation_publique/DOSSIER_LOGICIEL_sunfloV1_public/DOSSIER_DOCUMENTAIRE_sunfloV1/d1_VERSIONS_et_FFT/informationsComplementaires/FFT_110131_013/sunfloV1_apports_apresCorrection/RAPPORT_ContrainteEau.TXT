

*****************************************************************************
*
*
*   RAPPORT PRODUIT PAR ContrainteEau
*
*
*****************************************************************************
-----------------------------------------------------------------------------
Cumuls calcules depuis la levee, relatifs a ETR (mm) et ETM (mm), 
releves lors du  passage de la phase phenologique plante DESSICATION a RECOLTEE (qui correspond a la recolte)  : 

ETR cumule (mm) : 388
(correspond a vs.ETRcumulDepuisLevee).
Pour memo valeur complete : 387.875

ETM cumule (mm) : 498
(correspond a vs.ETMcumulDepuisLevee).
Pour memo valeur complete : 498.353

rapport ETR cumule / ETM cumule : 0.778
(correspond a vs.ETRcumulDepuisLevee / vs.ETMcumulDepuisLevee).
Pour memo valeur complete : 0.778314
-----------------------------------------------------------------------------
Cumuls calcules depuis la levee, relatifs a Pluie (mm) et ETP (mm), 
releves lors du  passage de la phase phenologique plante DESSICATION a RECOLTEE (qui correspond a la recolte)  : 

Pluie cumulee (mm) : 80
(correspond a vs.PluieCumulDepuisLevee).
Pour memo valeur complete : 79.5

ETP cumule (mm) : 636
(correspond a vs.ETPcumulDepuisLevee).
Pour memo valeur complete : 636.2