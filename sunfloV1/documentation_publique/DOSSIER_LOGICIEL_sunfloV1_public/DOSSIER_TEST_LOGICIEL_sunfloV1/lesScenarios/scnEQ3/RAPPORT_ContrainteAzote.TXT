

*****************************************************************************
*
*
*   RAPPORT PRODUIT PAR ContrainteAzote
*
*
*****************************************************************************
-----------------------------------------------------------------------------
Nabs (kg N/ha), 
lors du  passage de la phase phenologique plante CROISSANCEACTIVE a FLORAISON (stade F1, debut floraison)  : 75.1
Pour memo valeur complete : 75.0751
Nabs est la quantité d'azote correspondant à l'ofm.f du sol (Mass flow + Aborption Active)
-----------------------------------------------------------------------------
Nabs (kg N/ha), 
lors du  passage de la phase phenologique plante DESSICATION a RECOLTEE (qui correspond a la recolte)  : 136.1
Pour memo valeur complete : 136.087
Nabs est la quantité d'azote correspondant à l'ofm.f du sol (Mass flow + Aborption Active)