

*****************************************************************************
*
*
*   RAPPORT PRODUIT PAR CroissancePlante
*
*
*****************************************************************************
-----------------------------------------------------------------------------
LAI, indice foliaire, 
lors du  passage de la phase phenologique plante JUVENILE a CROISSANCEACTIVE (stade E1, bouton etoile)  : 1.48
Pour memo valeur complete : 1.48014
-----------------------------------------------------------------------------
LAI, indice foliaire, 
lors du  passage de la phase phenologique plante CROISSANCEACTIVE a FLORAISON (stade F1, debut floraison)  : 2.20
Pour memo valeur complete : 2.20099
-----------------------------------------------------------------------------
TDM, biomasse totale (g/m2) 
lors du  passage de la phase phenologique plante DESSICATION a RECOLTEE (qui correspond a la recolte)  : 573
Pour memo valeur complete : 572.551