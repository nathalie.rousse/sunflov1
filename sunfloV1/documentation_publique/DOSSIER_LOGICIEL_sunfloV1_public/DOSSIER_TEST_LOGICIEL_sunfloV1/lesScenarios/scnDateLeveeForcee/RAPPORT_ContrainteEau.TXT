

*****************************************************************************
*
*
*   RAPPORT PRODUIT PAR ContrainteEau
*
*
*****************************************************************************
-----------------------------------------------------------------------------
Cumuls calcules depuis la levee, relatifs a ETR (mm) et ETM (mm), 
releves lors du  passage de la phase phenologique plante DESSICATION a RECOLTEE (qui correspond a la recolte)  : 

ETR cumule (mm) : 391
(correspond a vs.ETRcumulDepuisLevee).
Pour memo valeur complete : 390.796

ETM cumule (mm) : 512
(correspond a vs.ETMcumulDepuisLevee).
Pour memo valeur complete : 512.006

rapport ETR cumule / ETM cumule : 0.763
(correspond a vs.ETRcumulDepuisLevee / vs.ETMcumulDepuisLevee).
Pour memo valeur complete : 0.763266
-----------------------------------------------------------------------------
Cumuls calcules depuis la levee, relatifs a Pluie (mm) et ETP (mm), 
releves lors du  passage de la phase phenologique plante DESSICATION a RECOLTEE (qui correspond a la recolte)  : 

Pluie cumulee (mm) : 79
(correspond a vs.PluieCumulDepuisLevee).
Pour memo valeur complete : 79

ETP cumule (mm) : 615
(correspond a vs.ETPcumulDepuisLevee).
Pour memo valeur complete : 615.5