

*****************************************************************************
*
*
*   RAPPORT PRODUIT PAR ContrainteAzote
*
*
*****************************************************************************
-----------------------------------------------------------------------------
Nabs (kg N/ha), 
lors du  passage de la phase phenologique plante CROISSANCEACTIVE a FLORAISON (stade F1, debut floraison)  : 78.3
Pour memo valeur complete : 78.327
Nabs est la quantité d'azote correspondant à l'ofm.f du sol (Mass flow + Aborption Active)
-----------------------------------------------------------------------------
Nabs (kg N/ha), 
lors du  passage de la phase phenologique plante DESSICATION a RECOLTEE (qui correspond a la recolte)  : 138.6
Pour memo valeur complete : 138.649
Nabs est la quantité d'azote correspondant à l'ofm.f du sol (Mass flow + Aborption Active)