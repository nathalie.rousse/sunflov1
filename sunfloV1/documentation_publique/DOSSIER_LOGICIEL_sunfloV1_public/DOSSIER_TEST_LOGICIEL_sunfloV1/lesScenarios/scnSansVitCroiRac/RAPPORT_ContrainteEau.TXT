

*****************************************************************************
*
*
*   RAPPORT PRODUIT PAR ContrainteEau
*
*
*****************************************************************************
-----------------------------------------------------------------------------
Cumuls calcules depuis la levee, relatifs a ETR (mm) et ETM (mm), 
releves lors du  passage de la phase phenologique plante DESSICATION a RECOLTEE (qui correspond a la recolte)  : 

ETR cumule (mm) : 389
(correspond a vs.ETRcumulDepuisLevee).
Pour memo valeur complete : 389.44

ETM cumule (mm) : 505
(correspond a vs.ETMcumulDepuisLevee).
Pour memo valeur complete : 504.946

rapport ETR cumule / ETM cumule : 0.771
(correspond a vs.ETRcumulDepuisLevee / vs.ETMcumulDepuisLevee).
Pour memo valeur complete : 0.771252
-----------------------------------------------------------------------------
Cumuls calcules depuis la levee, relatifs a Pluie (mm) et ETP (mm), 
releves lors du  passage de la phase phenologique plante DESSICATION a RECOLTEE (qui correspond a la recolte)  : 

Pluie cumulee (mm) : 80
(correspond a vs.PluieCumulDepuisLevee).
Pour memo valeur complete : 79.5

ETP cumule (mm) : 636
(correspond a vs.ETPcumulDepuisLevee).
Pour memo valeur complete : 636.2