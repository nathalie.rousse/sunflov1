UpdateCTestConfiguration  from :/home/nrousse/.vle/pkgs/sunfloV1_test_logiciel/build/DartConfiguration.tcl
Start processing tests
UpdateCTestConfiguration  from :/home/nrousse/.vle/pkgs/sunfloV1_test_logiciel/build/DartConfiguration.tcl
Test project /home/nrousse/.vle/pkgs/sunfloV1_test_logiciel/build
Constructing a list of tests
Done constructing a list of tests
Changing directory into /home/nrousse/.vle/pkgs/sunfloV1_test_logiciel/build/test
  6/ 10 Testing test_scnFichierInexistant     
Test command: /home/nrousse/.vle/pkgs/sunfloV1_test_logiciel/build/test/test_scnFichierInexistant
Test timeout computed to be: 9.99988e+06
Running 1 test case...
 
 
*************************************************************************
* 
*                          Scenario scnFichierInexistant
* debut : 
*************************************************************************
 

 
-------------------------------------------------------------------------
[TST] Operations de construction du fichier vpz du scenario de test : 

[TST] Ce fichier TST_sunfloV1_scnFichierInexistant.vpz est construit a partir de TST_sunfloV1_scenario_laBase.vpz

[TST] Pour memo, au sujet des sorties : le fichier SUNFLO_cmpMMpageDebug.csv de sortie de la simulation (contenant les resultats obtenus qu'il faudra comparer aux resultats attendus) n'est pas touche a ce niveau-la (pas de renommage).

[TST] Pas de modification des entrees.
[TST] Pas de connection de test.

[TST] Modification du parametre datas_file (de la famille des parametres CONFIG_ClimatNomFichier) : il valait 11_2003_AUZ_B.txt et est mis a NESTPASLA.txt.


[TST] Le fichier TST_sunfloV1_scnFichierInexistant.vpz construit est range dans output.

[TST] fin Operations de construction du fichier vpz du scenario de test.
-------------------------------------------------------------------------

 
-------------------------------------------------------------------------
[TST] Operations d'execution/simulation du scenario de test : 
 
.........................................................................
. 
. Debut de la simulation du scenario de test TST_sunfloV1_scnFichierInexistant.vpz
. 
.........................................................................
 


 
 ***** CalculsEntreesDeTest::CalculsEntreesDeTest fichier du code C++ : TST_CalculsEntreesDeTest_laBase.cpp
ParametresPlante::initialiser : Nom associe a CONFIG_Plante, la famille des parametres plante : Tournesol
ParametresVariete::initialiser : Nom associe a CONFIG_Variete, la famille des parametres variete : Melody
 
 ***** CroissancePlante::CroissancePlante valeurs des parametres : 
Eb_0 : 1
Eb_c : 4.5
Eb_fin : 0.015
Eb_max : 3
LAI_a : 400
LAI_b : 1
LAI_c : 153
LAI_d : 851.33
LAI_e : -0.03783
LAI_f : 0.78469
LAI_Kei : 0.01379
Phy1 : 71.43
Phy2 : 16.34
date_TT_F1 : 920
date_TT_M0 : 1160
date_TT_M3 : 2060
PHS : 1
bSF : 15.4
cSF : 613
TLN : 26.2
ext : 0.96
b : 0.0421085
a : -2.47532
 
ParametresPlante::initialiser : Nom associe a CONFIG_Plante, la famille des parametres plante : Tournesol
ParametresVariete::initialiser : Nom associe a CONFIG_Variete, la famille des parametres variete : Melody
Avertissement ParametresSimuInit::initialiser : La date de debut de la simulation est renseignee/parametree dans le menu project du vpz (experiment/begin) et non pas dans une condition CONFIG_ comme les parametres. Les valeurs donnees aux parametres rh1, rh2, Hini_C1, Hini_C2 DOIVENT CORRESPONDRE a cette date de debut de simulation.
ParametresSimuInit::initialiser : Nom associe a CONFIG_SimuInit, la famille des parametres de simulation et d'initialisation : indetermine
ParametresSimuInit::initialiser : Pour information, le reliquat N total (= rh1+rh2) vaut : 22
ParametresSimuInit::initialiser : Pour memo, le parametre dateLevee_casForcee s'appelait date_A2 dans code ModelMaker. La valeur de desactivation de dateLevee_casForcee est '00/00' (si datelevee_casForcee vaut '00/00' alors la levee n'est pas forcee, elle est estimee).
 
 ***** Phenologie::Phenologie valeurs des parametres : 
AP_a : 0.1
date_TT_germination : 86.2
dHE : 1.19
date_TT_E1 : 540
date_TT_F1 : 920
date_TT_M0 : 1160
date_TT_M3 : 2060
dateLevee_casForcee : 00/00
simulationPhaseSemisLevee : 1
 
 ---------------- Phenologie::Phenologie LEGENDE debut ----- 
 *     Signification des valeurs que PhasePhenoPlante peut prendre : PHASEPHENOPLANTE_NONSEMEE correspond a 0, PHASEPHENOPLANTE_GERMINATION correspond a 1, PHASEPHENOPLANTE_JUVENILE correspond a 2, PHASEPHENOPLANTE_CROISSANCEACTIVE correspond a 3, PHASEPHENOPLANTE_FLORAISON correspond a 4, PHASEPHENOPLANTE_MATURATION correspond a 5, PHASEPHENOPLANTE_DESSICATION correspond a 6, PHASEPHENOPLANTE_RECOLTEE correspond a 7 (attention le passage dans la phase RECOLTEE ne survient pas forcement a partir de la phase DESSICATION)
 ----- LEGENDE fin --------------------------------------------------- 
 
 ---------------- Phenologie::Phenologie LEGENDE debut ----- 
 *     Signification des valeurs que ChgtPhasePhenoPlante peut prendre : CHGTPHASEPHENOPLANTE_INACTIF correspond a 0 (INACTIF pour pas de changement de PhasePhenoPlante), CHGTPHASEPHENOPLANTE_NONSEMEE_A_GERMINATION correspond a 1, CHGTPHASEPHENOPLANTE_GERMINATION_A_JUVENILE correspond a 2, CHGTPHASEPHENOPLANTE_JUVENILE_A_CROISSANCEACTIVE correspond a 3, CHGTPHASEPHENOPLANTE_CROISSANCEACTIVE_A_FLORAISON correspond a 4, CHGTPHASEPHENOPLANTE_FLORAISON_A_MATURATION correspond a 5, CHGTPHASEPHENOPLANTE_MATURATION_A_DESSICATION correspond a 6, CHGTPHASEPHENOPLANTE_MATURATION_A_RECOLTEE correspond a 7, CHGTPHASEPHENOPLANTE_DESSICATION_A_RECOLTEE correspond a 8, CHGTPHASEPHENOPLANTE_ACTIF_AUTRE correspond a 10 (ACTIF_AUTRE pour tout autre changement PhasePhenoPlante)
 ----- LEGENDE fin --------------------------------------------------- 
 
ParametresPlante::initialiser : Nom associe a CONFIG_Plante, la famille des parametres plante : Tournesol
 
 ***** TemperatureEfficace::TemperatureEfficace valeurs des parametres : 
Tbase : 4.8
 
ParametresPlante::initialiser : Nom associe a CONFIG_Plante, la famille des parametres plante : Tournesol
ParametresVariete::initialiser : Nom associe a CONFIG_Variete, la famille des parametres variete : Melody
Avertissement ParametresSimuInit::initialiser : La date de debut de la simulation est renseignee/parametree dans le menu project du vpz (experiment/begin) et non pas dans une condition CONFIG_ comme les parametres. Les valeurs donnees aux parametres rh1, rh2, Hini_C1, Hini_C2 DOIVENT CORRESPONDRE a cette date de debut de simulation.
ParametresSimuInit::initialiser : Nom associe a CONFIG_SimuInit, la famille des parametres de simulation et d'initialisation : indetermine
ParametresSimuInit::initialiser : Pour information, le reliquat N total (= rh1+rh2) vaut : 22
ParametresSimuInit::initialiser : Pour memo, le parametre dateLevee_casForcee s'appelait date_A2 dans code ModelMaker. La valeur de desactivation de dateLevee_casForcee est '00/00' (si datelevee_casForcee vaut '00/00' alors la levee n'est pas forcee, elle est estimee).
 
 ***** Diagnostic::Diagnostic valeurs des parametres : 
date_TT_F1M0 : 350
date_TT_F1 : 920
date_TT_M3 : 2060
SeuilETRETM : 0.6
 
ParametresConduite::initialiser : Nom associe a CONFIG_Conduite, la famille des parametres conduite : indetermine
ParametresConduite::initialiser : Pour information, le parametre densite est la densite de LEVEE.
 
 ***** ConduiteCulture::ConduiteCulture valeurs des parametres : 
densite : 6.8
jsemis : 16/04
jrecolte : 23/08
zSemis : 30
lesApportsIrrigation (date, dose) : 
09/07 56
20/06 51.5
24/04 25.3
lesApportsFertilisation (date, dose) : 
01/05 60
lesApports (date, action irrigation, dose irrigation, action fertilisation, dose fertilisation) par ordre chronologique : 
(pour chaque apport affichage dans l'ordre de date, dose irrigation, dose fertilisation) : 
24/04, 25.300000, 0.000000
01/05, 0.000000, 60.000000
20/06, 51.500000, 0.000000
09/07, 56.000000, 0.000000
 
 ------------------- ConduiteCulture::ConduiteCulture LEGENDE debut ----- 
 *     Signification des valeurs que ActionSemis, ActionRecolte, ActionFerti et ActionIrrig peuvent prendre : ACTIONCONDUITE_INACTIVE correspond a 0, ACTIONCONDUITE_ACTIVE correspond a 1
 ----- LEGENDE fin ------------------------------------------------------ 
 
ParametresPlante::initialiser : Nom associe a CONFIG_Plante, la famille des parametres plante : Tournesol
ParametresSol::initialiser : Nom associe a CONFIG_Sol, la famille des parametres sol : SolCasNominal
Avertissement ParametresSimuInit::initialiser : La date de debut de la simulation est renseignee/parametree dans le menu project du vpz (experiment/begin) et non pas dans une condition CONFIG_ comme les parametres. Les valeurs donnees aux parametres rh1, rh2, Hini_C1, Hini_C2 DOIVENT CORRESPONDRE a cette date de debut de simulation.
ParametresSimuInit::initialiser : Nom associe a CONFIG_SimuInit, la famille des parametres de simulation et d'initialisation : indetermine
ParametresSimuInit::initialiser : Pour information, le reliquat N total (= rh1+rh2) vaut : 22
ParametresSimuInit::initialiser : Pour memo, le parametre dateLevee_casForcee s'appelait date_A2 dans code ModelMaker. La valeur de desactivation de dateLevee_casForcee est '00/00' (si datelevee_casForcee vaut '00/00' alors la levee n'est pas forcee, elle est estimee).
 
 ***** ContrainteAzote::ContrainteAzote valeurs des parametres : 
AA_a : 0.1
FNLEm : 0.3
INNseuil : 0.6
PNCc_a : 5
PNCc_b : 0.5
TDMc_seuil : 100
PNCm_a : 7
PNCm_b : 0.5
TDMm_seuil : 100
Fpf : 0.2
Vp : 0.5
rh1 : 10.2
rh2 : 11.8
 
ParametresPlante::initialiser : Nom associe a CONFIG_Plante, la famille des parametres plante : Tournesol
ParametresVariete::initialiser : Nom associe a CONFIG_Variete, la famille des parametres variete : Melody
ParametresSol::initialiser : Nom associe a CONFIG_Sol, la famille des parametres sol : SolCasNominal
Avertissement ParametresSimuInit::initialiser : La date de debut de la simulation est renseignee/parametree dans le menu project du vpz (experiment/begin) et non pas dans une condition CONFIG_ comme les parametres. Les valeurs donnees aux parametres rh1, rh2, Hini_C1, Hini_C2 DOIVENT CORRESPONDRE a cette date de debut de simulation.
ParametresSimuInit::initialiser : Nom associe a CONFIG_SimuInit, la famille des parametres de simulation et d'initialisation : indetermine
ParametresSimuInit::initialiser : Pour information, le reliquat N total (= rh1+rh2) vaut : 22
ParametresSimuInit::initialiser : Pour memo, le parametre dateLevee_casForcee s'appelait date_A2 dans code ModelMaker. La valeur de desactivation de dateLevee_casForcee est '00/00' (si datelevee_casForcee vaut '00/00' alors la levee n'est pas forcee, elle est estimee).
 
 ***** ContrainteEau::ContrainteEau valeurs des parametres : 
a_Pho : -25
zRac_max : 1800
VitCroiRac : 0.7
Kc : 1.2
a_LE : -3.81
a_TR : -5.651
profondeur : 1800
da_C1 : 1.5
da_C2 : 1.5
Hcc_C1 : 19.7
Hcc_C2 : 19.7
Hpf_C1 : 9.7
Hpf_C2 : 9.7
TC : 0
zC1 : 300
Hini_C1 : 19.7
Hini_C2 : 19.7
 ... parametres internes : 
RUmmC1 : 0.15
RUmmC2 : 0.15
zBilan : 1800
 
 ------------------- ContrainteEau::ContrainteEau LEGENDE debut ----- 
 *     La variable ETRETM n'a pas de sens lorqu'elle a pour valeur : -1
 ----- LEGENDE fin ------------------------------------------------------ 
 
 
 ***** ContrainteLumiere::ContrainteLumiere n'a pas de parametres
 
ParametresPlante::initialiser : Nom associe a CONFIG_Plante, la famille des parametres plante : Tournesol
 
 ***** ContrainteTemperature::ContrainteTemperature valeurs des parametres : 
Tbase : 4.8
Topt1_PHS : 20
Topt2_PHS : 28
Tmax_PHS : 37
 
ParametresVariete::initialiser : Nom associe a CONFIG_Variete, la famille des parametres variete : Melody
 
 ***** ElaborationQualite::ElaborationQualite valeurs des parametres : 
date_TT_F1 : 920
thp : 46.5
ext : 0.96
 
ParametresVariete::initialiser : Nom associe a CONFIG_Variete, la famille des parametres variete : Melody
Avertissement ParametresSimuInit::initialiser : La date de debut de la simulation est renseignee/parametree dans le menu project du vpz (experiment/begin) et non pas dans une condition CONFIG_ comme les parametres. Les valeurs donnees aux parametres rh1, rh2, Hini_C1, Hini_C2 DOIVENT CORRESPONDRE a cette date de debut de simulation.
ParametresSimuInit::initialiser : Nom associe a CONFIG_SimuInit, la famille des parametres de simulation et d'initia
*** 1 failure detected in test suite "package_test"
lisation : indetermine
ParametresSimuInit::initialiser : Pour information, le reliquat N total (= rh1+rh2) vaut : 22
ParametresSimuInit::initialiser : Pour memo, le parametre dateLevee_casForcee s'appelait date_A2 dans code ModelMaker. La valeur de desactivation de dateLevee_casForcee est '00/00' (si datelevee_casForcee vaut '00/00' alors la levee n'est pas forcee, elle est estimee).
 
 ***** ElaborationRendement::ElaborationRendement valeurs des parametres : 
IRg : 0.4
SeuilETRETM : 0.6
 unknown location(0): fatal error in "test_scnFichierInexistant": memory access violation at address: 0x00000011: no mapping at fault address
Test is aborted
-- Process completed
***Failed 

0% tests passed, 1 tests failed out of 1
	  6 - test_scnFichierInexistant (Failed)
