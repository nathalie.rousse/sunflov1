
[TST_sunfloV1_VerifJeuDeTest_jeuEQ5.R]
Jeu appele/deroule sans MODE_DEBUG. Jeu appele/deroule pour generer un RAPPORT_DETAILLE. 

***********************************************************************************
* 
* Modele VLE sunfloV1, jeu de test jeuEQ5
* 
* Comparaison entre les resultats obtenus et les resultats attendus. 
* 
* Les verifications automatiques sont effectuees, tandis que celles qui sont a effectuer manuellement sont signalees. 
* 
* Le jeu de test jeuEQ5 s'appuie sur le scenario de test scnSunfloV1intact.
* 
***********************************************************************************

Les resultats de simulation sont recuperes dans le repertoire  ./../output/lesScenarios/scnSunfloV1intact/ 
Le repertoire des resultats des verifications est  ./../output/lesJeuxDeTest/jeuEQ5/ 
Notamment le repertoire des representations graphiques est  ./../output/lesJeuxDeTest/jeuEQ5/representationsGraphiques/ 
Le fichier des donnees obtenues en simulation de test est  ./../output/lesScenarios/scnSunfloV1intact/TST_sortiesObtenues_scnSunfloV1intact.csv .
Pour info, le fichier des donnees attendues  ./../test/TST_sortiesAttendues_jeuEQ5.csv N'EXISTE PAS 
Pour memo, resuAttendu[j,nomVar] vaut  -777  si aucune valeur ne lui a ete affectee (ni par fichier, ni par code R) 
Pour info, Le fichier des tolerances de test  ./../test/TST_toleranceTest_jeuEQ5.csv  N'EXISTE PAS.
Pour memo, le tableau des tolerances de test toleranceTest est interprete/utilise de la maniere suivante dans la comparaison des donnees obtenues (resuObtenu) aux donnees attendues (resuAttendu) : si toleranceTest[j,nomVar]= -111 (ie COMPARAISON_INACTIVE) alors resuAttendu[j,nomVar] ne sera pas compare a resuObtenu[j,nomVar]; si toleranceTest[j,nomVar]>=0 alors il sera verifie que resuObtenu[j,nomVar]=resuAttendu[j,nomVar] a toleranceTest[j,nomVar] pres. 

Le fichier  ./../output/lesScenarios/scnSunfloV1intact/TST_sortiesObtenues_scnSunfloV1intact.csv  contient les variables time AAAAnumerique JJnumerique MMnumerique jourDeLannee DBP Eb Ebp Ei LAI SFp TDM AP ChgtPhasePhenoPlante PhasePhenoPlante TT_A0 TT_A2 TT_F1 photo_DatePsgPhasePhenoPlante_CROISSANCEACTIVE_A_FLORAISON photo_DatePsgPhasePhenoPlante_DESSICATION_A_RECOLTEE photo_DatePsgPhasePhenoPlante_FLORAISON_A_MATURATION photo_DatePsgPhasePhenoPlante_GERMINATION_A_JUVENILE photo_DatePsgPhasePhenoPlante_JUVENILE_A_CROISSANCEACTIVE photo_DatePsgPhasePhenoPlante_MATURATION_A_DESSICATION photo_DatePsgPhasePhenoPlante_MATURATION_A_RECOLTEE photo_DatePsgPhasePhenoPlante_NONSEMEE_A_GERMINATION Teff Tmoy ISH1 ISH2 ISH3 ActionFerti ActionIrrig ActionRecolte ActionSemis DoseFerti DoseIrrig EtatConduite densite zSemis CCN CGR CN1 CN2 CN3 CRU CRUm DDM Ea Engrais F1 F2 F3 FE FHN FNIRUE FNLE FR INN INNI N1 N2 N3 Nabs Ncrit Ndenit Nmax Nmine Nsol TNp TNpcrit TNpmax vAA vAA1 vAA2 vDenit vMF vMF1 vMF2 vMine vNA vNAc ATSW C1 C2 C3 D1 D2 D3 ETMcumulDepuisLevee ETP ETPcumulDepuisLevee ETRETM ETRcumulDepuisLevee EVj FHLE FHRUE FHTR FTSW JS PluieCumulDepuisLevee RWCC1 STOCKC1 STOCKC2 STOCKC3 TEC1 TR TRPF TTSW fRacC1 vRac vTR vTRC1 vTRC2 vTRp zC2 zC3 zRac FLe PARi FT FTHN DSF P2 TH photo_TH_aFinMATURATION IRs JSE JSF JSM RDT photo_INN_CROISSANCEACTIVE_A_FLORAISON photo_IRs_aFinMATURATION photo_RDT_aFinMATURATION photo_TDM_CROISSANCEACTIVE_A_FLORAISON ETPP Pluie RG Tn Tx

resuAttendu (sauf jourDeLannee) est affecte par defaut a sa valeur d'invalidite  -777 
toleranceTest (sauf jourDeLannee) est affecte par defaut a sa valeur d'inactivation  -111  (=COMPARAISON_INACTIVE). 

******************************************
* Description du jeu de test  jeuEQ5 , et verifications a effectuer 
******************************************

Jeu de verification de l'equation EQ5 de la publi, ou les resultats attendus correspondent theoriquement a l'equation de la publi. 

********* Les donnees/informations de test :

EQ5 selon la publi : 
[EQ5] T.NM = 36 / ( 1 + (36-1) * exp( -0.119 * (      Tm - 15 ) ) ) 
EQ5 traduite en noms du code vle : 
[EQ5] vs.FTHN = 36 / ( 1 + (36-1) * exp( -0.119 * ( ve.Tmoy - 15 ) ) ) 

Pour memo, pseudo-code du traitement relatif a FTHN du cote du code du modele sunfloV1 : 
FTHN = 36 / ( 1 + (36-1) * exp( -0.119 * (Tmoy-15) ) ) 

Entrees de test injectees, parametres modifies... : voir le rapport TST_rapportScenario_ scnSunfloV1intact .txt 

Definition des resultats attendus : 
Le calcul des resultats attendus [EQ5] servant dans la verification est code sous R. 
Les resultats attendus calcules correspondent theoriquement a l'equation EQ5 de la publi. 
Les indices y respectent FTHN(j) = fonction_de(j). 

********* Les VERIFICATIONS (automatiques et/ou manuelles) :

Automatique :  Verification numerique de la donnee vs.FTHN (du modele contrainte_temperature).  La tolerance d'ecart dans les comparaisons entre 'resu attendu' et 'resu obtenu' est 'resu attendu' x  5e-06 . 
Manuelle : Pas de verification manuelle supplementaire. 
Remarque :  aucune. 

******************************************

******************************************
* Verifications numeriques automatiques 
******************************************

Les resultats attendus et tolerances de test associees (resuAttendu, toleranceTest) sont archives dans les fichiers  ./../output/lesJeuxDeTest/jeuEQ5/complet_TST_sortiesAttendues_jeuEQ5.csv  et  ./../output/lesJeuxDeTest/jeuEQ5/complet_TST_toleranceTest_jeuEQ5.csv . Il s'agit des attendus complets, dont le contenu a ete defini par fichiers et/ou code R. 
ATTENTION, l'interpretation des resultats attendus (informations de resuAttendu) depend des tolerances de test (informations de toleranceTest), qui doivent avoir ete affectees (via fichier ou code R). 

**********************************************************************************
* debut rapport des comparaisons Verifications numeriques automatiques 
 
 test    ok   pour variable  FTHN 
              aux indices (entre parentheses: jourDeLannee,ecart entre resultats obtenu et attendu) :    1 ( 80 , 5.55111512312578e-16 )   2 ( 81 , 4.44089209850063e-16 )   3 ( 82 , 3.33066907387547e-16 )   4 ( 83 , 3.33066907387547e-16 )   5 ( 84 , 0 )   6 ( 85 , 3.33066907387547e-16 )   7 ( 86 , 2.22044604925031e-16 )   8 ( 87 , 2.22044604925031e-16 )   9 ( 88 , 2.22044604925031e-16 )   10 ( 89 , 1.11022302462516e-16 )   11 ( 90 , 4.44089209850063e-16 )   12 ( 91 , 4.44089209850063e-16 )   13 ( 92 , 6.66133814775094e-16 )   14 ( 93 , 1.11022302462516e-16 )   15 ( 94 , 1.11022302462516e-16 )   16 ( 95 , 2.77555756156289e-16 )   17 ( 96 , 1.11022302462516e-16 )   18 ( 97 , 3.88578058618805e-16 )   19 ( 98 , 4.44089209850063e-16 )   20 ( 99 , 0 )   21 ( 100 , 4.44089209850063e-16 )   22 ( 101 , 5.55111512312578e-16 )   23 ( 102 , 1.11022302462516e-16 )   24 ( 103 , 4.44089209850063e-16 )   25 ( 104 , 3.10862446895044e-15 )   26 ( 105 , 4.44089209850063e-15 )   27 ( 106 , 4.44089209850063e-15 )   28 ( 107 , 2.66453525910038e-15 )   29 ( 108 , 1.11022302462516e-15 )   30 ( 109 , 2.22044604925031e-16 )   31 ( 110 , 4.44089209850063e-16 )   32 ( 111 , 3.5527136788005e-15 )   33 ( 112 , 3.33066907387547e-16 )   34 ( 113 , 2.22044604925031e-16 )   35 ( 114 , 4.21884749357559e-15 )   36 ( 115 , 6.66133814775094e-16 )   37 ( 116 , 2.44249065417534e-15 )   38 ( 117 , 6.66133814775094e-16 )   39 ( 118 , 4.44089209850063e-15 )   40 ( 119 , 4.66293670342566e-15 )   41 ( 120 , 2.88657986402541e-15 )   42 ( 121 , 1.11022302462516e-16 )   43 ( 122 , 4.66293670342566e-15 )   44 ( 123 , 5.55111512312578e-16 )   45 ( 124 , 2.22044604925031e-16 )   46 ( 125 , 2.88657986402541e-15 )   47 ( 126 , 4.44089209850063e-16 )   48 ( 127 , 2.22044604925031e-15 )   49 ( 128 , 1.33226762955019e-15 )   50 ( 129 , 2.22044604925031e-16 )   51 ( 130 , 1.11022302462516e-16 )   52 ( 131 , 0 )   53 ( 132 , 8.88178419700125e-16 )   54 ( 133 , 2.88657986402541e-15 )   55 ( 134 , 1.11022302462516e-16 )   56 ( 135 , 1.11022302462516e-16 )   57 ( 136 , 2.88657986402541e-15 )   58 ( 137 , 2.66453525910038e-15 )   59 ( 138 , 4.44089209850063e-15 )   60 ( 139 , 2.66453525910038e-15 )   61 ( 140 , 5.55111512312578e-16 )   62 ( 141 , 0 )   63 ( 142 , 3.5527136788005e-15 )   64 ( 143 , 1.77635683940025e-15 )   65 ( 144 , 2.22044604925031e-16 )   66 ( 145 , 3.33066907387547e-16 )   67 ( 146 , 2.22044604925031e-16 )   68 ( 147 , 8.88178419700125e-16 )   69 ( 148 , 2.44249065417534e-15 )   70 ( 149 , 3.99680288865056e-15 )   71 ( 150 , 3.10862446895044e-15 )   72 ( 151 , 0 )   73 ( 152 , 1.33226762955019e-15 )   74 ( 153 , 3.5527136788005e-15 )   75 ( 154 , 8.88178419700125e-16 )   76 ( 155 , 2.66453525910038e-15 )   77 ( 156 , 1.55431223447522e-15 )   78 ( 157 , 2.66453525910038e-15 )   79 ( 158 , 2.66453525910038e-15 )   80 ( 159 , 4.44089209850063e-15 )   81 ( 160 , 3.5527136788005e-15 )   82 ( 161 , 3.10862446895044e-15 )   83 ( 162 , 4.44089209850063e-16 )   84 ( 163 , 1.77635683940025e-15 )   85 ( 164 , 8.88178419700125e-16 )   86 ( 165 , 8.88178419700125e-16 )   87 ( 166 , 4.44089209850063e-15 )   88 ( 167 , 4.44089209850063e-15 )   89 ( 168 , 1.33226762955019e-15 )   90 ( 169 , 2.66453525910038e-15 )   91 ( 170 , 4.44089209850063e-16 )   92 ( 171 , 1.33226762955019e-15 )   93 ( 172 , 4.44089209850063e-15 )   94 ( 173 , 1.77635683940025e-15 )   95 ( 174 , 2.66453525910038e-15 )   96 ( 175 , 3.10862446895044e-15 )   97 ( 176 , 2.66453525910038e-15 )   98 ( 177 , 3.10862446895044e-15 )   99 ( 178 , 3.5527136788005e-15 )   100 ( 179 , 3.99680288865056e-15 )   101 ( 180 , 4.44089209850063e-16 )   102 ( 181 , 2.66453525910038e-15 )   103 ( 182 , 4.88498130835069e-15 )   104 ( 183 , 1.77635683940025e-15 )   105 ( 184 , 2.22044604925031e-15 )   106 ( 185 , 4.66293670342566e-15 )   107 ( 186 , 4.66293670342566e-15 )   108 ( 187 , 3.5527136788005e-15 )   109 ( 188 , 2.22044604925031e-15 )   110 ( 189 , 4.44089209850063e-15 )   111 ( 190 , 3.10862446895044e-15 )   112 ( 191 , 4.88498130835069e-15 )   113 ( 192 , 3.10862446895044e-15 )   114 ( 193 , 2.66453525910038e-15 )   115 ( 194 , 2.66453525910038e-15 )   116 ( 195 , 1.77635683940025e-15 )   117 ( 196 , 8.88178419700125e-16 )   118 ( 197 , 4.44089209850063e-16 )   119 ( 198 , 1.33226762955019e-15 )   120 ( 199 , 3.10862446895044e-15 )   121 ( 200 , 6.66133814775094e-15 )   122 ( 201 , 1.77635683940025e-15 )   123 ( 202 , 2.66453525910038e-15 )   124 ( 203 , 1.33226762955019e-15 )   125 ( 204 , 2.66453525910038e-15 )   126 ( 205 , 3.10862446895044e-15 )   127 ( 206 , 3.10862446895044e-15 )   128 ( 207 , 4.44089209850063e-16 )   129 ( 208 , 3.5527136788005e-15 )   130 ( 209 , 3.99680288865056e-15 )   131 ( 210 , 4.44089209850063e-15 )   132 ( 211 , 2.22044604925031e-15 )   133 ( 212 , 3.10862446895044e-15 )   134 ( 213 , 8.88178419700125e-16 )   135 ( 214 , 3.99680288865056e-15 )   136 ( 215 , 3.5527136788005e-15 )   137 ( 216 , 2.66453525910038e-15 )   138 ( 217 , 8.88178419700125e-16 )   139 ( 218 , 3.5527136788005e-15 )   140 ( 219 , 3.5527136788005e-15 )   141 ( 220 , 1.77635683940025e-15 )   142 ( 221 , 2.66453525910038e-15 )   143 ( 222 , 1.77635683940025e-15 )   144 ( 223 , 2.66453525910038e-15 )   145 ( 224 , 1.77635683940025e-15 )   146 ( 225 , 2.66453525910038e-15 )   147 ( 226 , 8.88178419700125e-16 )   148 ( 227 , 4.88498130835069e-15 )   149 ( 228 , 4.44089209850063e-15 )   150 ( 229 , 8.88178419700125e-16 )   151 ( 230 , 4.44089209850063e-16 )   152 ( 231 , 1.77635683940025e-15 )   153 ( 232 , 1.77635683940025e-15 )   154 ( 233 , 1.33226762955019e-15 )   155 ( 234 , 1.77635683940025e-15 )   156 ( 235 , 1.77635683940025e-15 )   157 ( 236 , 2.66453525910038e-15 )   158 ( 237 , 8.88178419700125e-16 )   159 ( 238 , 1.77635683940025e-15 )   160 ( 239 , 1.77635683940025e-15 )   161 ( 240 , 1.77635683940025e-15 )   162 ( 241 , 1.77635683940025e-15 )   163 ( 242 , 2.22044604925031e-15 )   164 ( 243 , 2.22044604925031e-16 )   165 ( 244 , 4.44089209850063e-16 )   166 ( 245 , 2.22044604925031e-15 )   167 ( 246 , 4.21884749357559e-15 )   168 ( 247 , 3.5527136788005e-15 )   169 ( 248 , 1.33226762955019e-15 )   170 ( 249 , 1.33226762955019e-15 )   171 ( 250 , 2.66453525910038e-15 )   172 ( 251 , 4.44089209850063e-15 )   173 ( 252 , 1.99840144432528e-15 )   174 ( 253 , 4.44089209850063e-15 )   175 ( 254 , 4.88498130835069e-15 )   176 ( 255 , 2.66453525910038e-15 )   177 ( 256 , 4.44089209850063e-15 )   178 ( 257 , 2.88657986402541e-15 )   179 ( 258 , 4.44089209850063e-15 )   180 ( 259 , 1.77635683940025e-15 )   181 ( 260 , 8.88178419700125e-16 )   182 ( 261 , 2.88657986402541e-15 )   183 ( 262 , 3.10862446895044e-15 )   184 ( 263 , 2.22044604925031e-15 )   185 ( 264 , 2.22044604925031e-15 )   186 ( 265 , 3.5527136788005e-15 )   187 ( 266 , 2.22044604925031e-16 )   188 ( 267 , 0 )   189 ( 268 , 0 )   190 ( 269 , 4.66293670342566e-15 )   191 ( 270 , 4.66293670342566e-15 )   192 ( 271 , 1.99840144432528e-15 )   193 ( 272 , 2.88657986402541e-15 )   194 ( 273 , 2.66453525910038e-15 )   195 ( 274 , 4.88498130835069e-15 )   196 ( 275 , 2.22044604925031e-15 )   197 ( 276 , 2.22044604925031e-16 )   198 ( 277 , 4.66293670342566e-15 )   199 ( 278 , 1.11022302462516e-16 )   200 ( 279 , 2.22044604925031e-16 )   201 ( 280 , 1.11022302462516e-16 )   202 ( 281 , 2.88657986402541e-15 )   203 ( 282 , 1.33226762955019e-15 )   204 ( 283 , 1.11022302462516e-16 )   205 ( 284 , 3.33066907387547e-16 )   206 ( 285 , 4.66293670342566e-15 )   207 ( 286 , 2.66453525910038e-15 )   208 ( 287 , 8.88178419700125e-16 )   209 ( 288 , 2.66453525910038e-15 )   210 ( 289 , 3.5527136788005e-15 )   211 ( 290 , 0 )   212 ( 291 , 6.66133814775094e-16 )   213 ( 292 , 0 )   214 ( 293 , 2.22044604925031e-16 )   215 ( 294 , 1.11022302462516e-16 )   216 ( 295 , 1.11022302462516e-16 )   217 ( 296 , 4.44089209850063e-16 )   218 ( 297 , 0 )   219 ( 298 , 0 )   220 ( 299 , 4.9960036108132e-16 )   221 ( 300 , 5.55111512312578e-17 )

Bilan : 
 Sur l'ensemble des comparaisons numeriques de la variable  FTHN , 
 l'ecart de test calcule maximal vaut  6.66133814775094e-15 , la tolerance de test maximale vaut  3.07445108234011e-05 . 


* fin rapport des comparaisons Verifications numeriques automatiques 
**********************************************************************************


******************************************
* Representations graphiques 
******************************************

Les representations graphiques des valeurs attendues par rapport aux valeurs obtenues (un graphique par donnee) sont rangees sous le repertoire  ./../output/lesJeuxDeTest/jeuEQ5/representationsGraphiques/ 
ATTENTION, toutes les valeurs lues dans resuAttendu sont tracees mais parmi elles, n'ont aucun sens celles qui n'ont pas ete affectees (valeur  -777  (=VALEUR_INVALIDE) (cf cas ou la tolerance de test n'a pas ete affectee (valeur  -111  (=COMPARAISON_INACTIVE)). 
