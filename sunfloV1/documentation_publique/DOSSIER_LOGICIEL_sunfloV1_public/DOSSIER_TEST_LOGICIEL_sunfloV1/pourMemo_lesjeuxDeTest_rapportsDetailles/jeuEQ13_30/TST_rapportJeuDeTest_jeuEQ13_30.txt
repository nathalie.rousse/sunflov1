
[TST_sunfloV1_VerifJeuDeTest_jeuEQ13_30.R]
Jeu appele/deroule sans MODE_DEBUG. Jeu appele/deroule pour generer un RAPPORT_DETAILLE. 

***********************************************************************************
* 
* Modele VLE sunfloV1, jeu de test jeuEQ13_30
* 
* Comparaison entre les resultats obtenus et les resultats attendus. 
* 
* Les verifications automatiques sont effectuees, tandis que celles qui sont a effectuer manuellement sont signalees. 
* 
* Le jeu de test jeuEQ13_30 s'appuie sur le scenario de test scnSunfloV1intact.
* 
***********************************************************************************

Les resultats de simulation sont recuperes dans le repertoire  ./../output/lesScenarios/scnSunfloV1intact/ 
Le repertoire des resultats des verifications est  ./../output/lesJeuxDeTest/jeuEQ13_30/ 
Notamment le repertoire des representations graphiques est  ./../output/lesJeuxDeTest/jeuEQ13_30/representationsGraphiques/ 
Le fichier des donnees obtenues en simulation de test est  ./../output/lesScenarios/scnSunfloV1intact/TST_sortiesObtenues_scnSunfloV1intact.csv .
Pour info, le fichier des donnees attendues  ./../test/TST_sortiesAttendues_jeuEQ13_30.csv N'EXISTE PAS 
Pour memo, resuAttendu[j,nomVar] vaut  -777  si aucune valeur ne lui a ete affectee (ni par fichier, ni par code R) 
Pour info, Le fichier des tolerances de test  ./../test/TST_toleranceTest_jeuEQ13_30.csv  N'EXISTE PAS.
Pour memo, le tableau des tolerances de test toleranceTest est interprete/utilise de la maniere suivante dans la comparaison des donnees obtenues (resuObtenu) aux donnees attendues (resuAttendu) : si toleranceTest[j,nomVar]= -111 (ie COMPARAISON_INACTIVE) alors resuAttendu[j,nomVar] ne sera pas compare a resuObtenu[j,nomVar]; si toleranceTest[j,nomVar]>=0 alors il sera verifie que resuObtenu[j,nomVar]=resuAttendu[j,nomVar] a toleranceTest[j,nomVar] pres. 

Le fichier  ./../output/lesScenarios/scnSunfloV1intact/TST_sortiesObtenues_scnSunfloV1intact.csv  contient les variables time AAAAnumerique JJnumerique MMnumerique jourDeLannee DBP Eb Ebp Ei LAI SFp TDM AP ChgtPhasePhenoPlante PhasePhenoPlante TT_A0 TT_A2 TT_F1 photo_DatePsgPhasePhenoPlante_CROISSANCEACTIVE_A_FLORAISON photo_DatePsgPhasePhenoPlante_DESSICATION_A_RECOLTEE photo_DatePsgPhasePhenoPlante_FLORAISON_A_MATURATION photo_DatePsgPhasePhenoPlante_GERMINATION_A_JUVENILE photo_DatePsgPhasePhenoPlante_JUVENILE_A_CROISSANCEACTIVE photo_DatePsgPhasePhenoPlante_MATURATION_A_DESSICATION photo_DatePsgPhasePhenoPlante_MATURATION_A_RECOLTEE photo_DatePsgPhasePhenoPlante_NONSEMEE_A_GERMINATION Teff Tmoy ISH1 ISH2 ISH3 ActionFerti ActionIrrig ActionRecolte ActionSemis DoseFerti DoseIrrig EtatConduite densite zSemis CCN CGR CN1 CN2 CN3 CRU CRUm DDM Ea Engrais F1 F2 F3 FE FHN FNIRUE FNLE FR INN INNI N1 N2 N3 Nabs Ncrit Ndenit Nmax Nmine Nsol TNp TNpcrit TNpmax vAA vAA1 vAA2 vDenit vMF vMF1 vMF2 vMine vNA vNAc ATSW C1 C2 C3 D1 D2 D3 ETMcumulDepuisLevee ETP ETPcumulDepuisLevee ETRETM ETRcumulDepuisLevee EVj FHLE FHRUE FHTR FTSW JS PluieCumulDepuisLevee RWCC1 STOCKC1 STOCKC2 STOCKC3 TEC1 TR TRPF TTSW fRacC1 vRac vTR vTRC1 vTRC2 vTRp zC2 zC3 zRac FLe PARi FT FTHN DSF P2 TH photo_TH_aFinMATURATION IRs JSE JSF JSM RDT photo_INN_CROISSANCEACTIVE_A_FLORAISON photo_IRs_aFinMATURATION photo_RDT_aFinMATURATION photo_TDM_CROISSANCEACTIVE_A_FLORAISON ETPP Pluie RG Tn Tx

resuAttendu (sauf jourDeLannee) est affecte par defaut a sa valeur d'invalidite  -777 
toleranceTest (sauf jourDeLannee) est affecte par defaut a sa valeur d'inactivation  -111  (=COMPARAISON_INACTIVE). 

******************************************
* Description du jeu de test  jeuEQ13_30 , et verifications a effectuer 
******************************************

Jeu de verification de l'equation EQ13 de la publi, ou le resultat attendu correspond theoriquement a l'equation de la publi. 

********* Les donnees/informations de test :

EQ13 selon la publi : 
[EQ13] dPTR = Kc * ETP * RIE ; where Kc = 1.2 
EQ13 traduite en noms du code vle : 
[EQ13] vi.vTRp  = p.Kc * ve.ETP * ve.Ei ; where p.Kc = 1.2 

Pour memo, pseudo-code du traitement relatif a vTRp du cote du code du modele sunfloV1 : 
vi.vTRp =  Kc * ETP * Ei 

Entrees de test injectees, parametres modifies... : voir le rapport TST_rapportScenario_ scnSunfloV1intact .txt 

Definition des resultats attendus : 
Le calcul des resultats attendus [EQ13] servant dans la verification est code sous R. 
Les resultats attendus calcules correspondent theoriquement a l'equation EQ13 de la publi. 
Les indices y respectent vTRp(j) = fonction_de(j). 

********* Les VERIFICATIONS (automatiques et/ou manuelles) :

Automatique :  Verification numerique de la donnee vi.vTRp (du modele phenologie). La tolerance d'ecart dans les comparaisons entre 'resu attendu' et 'resu obtenu' est 'resu attendu' x  0.3 . 
Manuelle : Pas de verification manuelle supplementaire. 
Remarque :  On peut voir dans le rapport de simulation TST_rapportScenario_ scnSunfloV1intact .txt que le parametre Kc= 1.2 . 

******************************************

******************************************
* Verifications numeriques automatiques 
******************************************

Les resultats attendus et tolerances de test associees (resuAttendu, toleranceTest) sont archives dans les fichiers  ./../output/lesJeuxDeTest/jeuEQ13_30/complet_TST_sortiesAttendues_jeuEQ13_30.csv  et  ./../output/lesJeuxDeTest/jeuEQ13_30/complet_TST_toleranceTest_jeuEQ13_30.csv . Il s'agit des attendus complets, dont le contenu a ete defini par fichiers et/ou code R. 
ATTENTION, l'interpretation des resultats attendus (informations de resuAttendu) depend des tolerances de test (informations de toleranceTest), qui doivent avoir ete affectees (via fichier ou code R). 

**********************************************************************************
* debut rapport des comparaisons Verifications numeriques automatiques 

 test NOTOK   pour vTRp (indice 39 , ie jourDeLannee 118 )
              valeurs obtenue :  0  / attendue :  0.080179212178338 
              ecart calcule :  0.080179212178338  / tolerance de test :  0.0240537636535014
 test NOTOK   pour vTRp (indice 138 , ie jourDeLannee 217 )
              valeurs obtenue :  3.40617160342822  / attendue :  2.29915841314153 
              ecart calcule :  1.10701319028669  / tolerance de test :  0.689747523942459
 test NOTOK   pour vTRp (indice 139 , ie jourDeLannee 218 )
              valeurs obtenue :  2.15647439314403  / attendue :  4.94511606360381e-06 
              ecart calcule :  2.15646944802797  / tolerance de test :  1.48353481908114e-06
 test NOTOK   pour vTRp (indice 142 , ie jourDeLannee 221 )
              valeurs obtenue :  3.12804381259113e-06  / attendue :  2.08201517203887e-06 
              ecart calcule :  1.04602864055226e-06  / tolerance de test :  6.24604551611662e-07
 test NOTOK   pour vTRp (indice 143 , ie jourDeLannee 222 )
              valeurs obtenue :  2.06372514124498e-06  / attendue :  1.57432671274504e-06 
              ecart calcule :  4.89398428499936e-07  / tolerance de test :  4.72298013823513e-07
 test NOTOK   pour vTRp (indice 144 , ie jourDeLannee 223 )
              valeurs obtenue :  1.44642720136989e-06  / attendue :  1.05761481423805e-06 
              ecart calcule :  3.88812387131839e-07  / tolerance de test :  3.17284444271415e-07
 test NOTOK   pour vTRp (indice 145 , ie jourDeLannee 224 )
              valeurs obtenue :  1.16303622658975e-06  / attendue :  8.88465540802133e-07 
              ecart calcule :  2.74570685787617e-07  / tolerance de test :  2.6653966224064e-07
 test NOTOK   pour vTRp (indice 146 , ie jourDeLannee 225 )
              valeurs obtenue :  8.58590237529544e-07  / attendue :  6.24716516480106e-07 
              ecart calcule :  2.33873721049438e-07  / tolerance de test :  1.87414954944032e-07
 test NOTOK   pour vTRp (indice 147 , ie jourDeLannee 226 )
              valeurs obtenue :  4.61212798853694e-07  / attendue :  2.54680109119221e-07 
              ecart calcule :  2.06532689734473e-07  / tolerance de test :  7.64040327357663e-08
 test NOTOK   pour vTRp (indice 148 , ie jourDeLannee 227 )
              valeurs obtenue :  2.03013447638067e-07  / attendue :  1.14459632224228e-07 
              ecart calcule :  8.85538154138386e-08  / tolerance de test :  3.43378896672685e-08
 test NOTOK   pour vTRp (indice 150 , ie jourDeLannee 229 )
              valeurs obtenue :  5.26413696313455e-08  / attendue :  1.91078050701954e-08 
              ecart calcule :  3.35335645611501e-08  / tolerance de test :  5.73234152105861e-09
 test NOTOK   pour vTRp (indice 151 , ie jourDeLannee 230 )
              valeurs obtenue :  5.23387704096656e-08  / attendue :  8.1007246091058e-08 
              ecart calcule :  2.86684756813924e-08  / tolerance de test :  2.43021738273174e-08
 test NOTOK   pour vTRp (indice 153 , ie jourDeLannee 232 )
              valeurs obtenue :  7.08079182660803e-08  / attendue :  0 
              ecart calcule :  7.08079182660803e-08  / tolerance de test :  0 
 test    ok   pour variable  vTRp 
              aux indices (entre parentheses: jourDeLannee,ecart entre resultats obtenu et attendu) :    1 ( 80 , 0 )   2 ( 81 , 0 )   3 ( 82 , 0 )   4 ( 83 , 0 )   5 ( 84 , 0 )   6 ( 85 , 0 )   7 ( 86 , 0 )   8 ( 87 , 0 )   9 ( 88 , 0 )   10 ( 89 , 0 )   11 ( 90 , 0 )   12 ( 91 , 0 )   13 ( 92 , 0 )   14 ( 93 , 0 )   15 ( 94 , 0 )   16 ( 95 , 0 )   17 ( 96 , 0 )   18 ( 97 , 0 )   19 ( 98 , 0 )   20 ( 99 , 0 )   21 ( 100 , 0 )   22 ( 101 , 0 )   23 ( 102 , 0 )   24 ( 103 , 0 )   25 ( 104 , 0 )   26 ( 105 , 0 )   27 ( 106 , 0 )   28 ( 107 , 0 )   29 ( 108 , 0 )   30 ( 109 , 0 )   31 ( 110 , 0 )   32 ( 111 , 0 )   33 ( 112 , 0 )   34 ( 113 , 0 )   35 ( 114 , 0 )   36 ( 115 , 0 )   37 ( 116 , 0 )   38 ( 117 , 0 )   40 ( 119 , 0.0083424050811342 )   41 ( 120 , 0.00558290694487031 )   42 ( 121 , 0.00916658195043478 )   43 ( 122 , 0.0170539907746807 )   44 ( 123 , 0.0395226339170291 )   45 ( 124 , 0.0408983016778309 )   46 ( 125 , 0.0137940818565229 )   47 ( 126 , 0.00237155875163685 )   48 ( 127 , 0.0126839326888918 )   49 ( 128 , 0.0207388687197836 )   50 ( 129 , 0.0179332519857566 )   51 ( 130 , 0.0461763802884415 )   52 ( 131 , 0.0548496778870942 )   53 ( 132 , 0.0622170147959904 )   54 ( 133 , 0.0857649800459455 )   55 ( 134 , 0.0633989614051109 )   56 ( 135 , 0.057066339549946 )   57 ( 136 , 0.0610876372069185 )   58 ( 137 , 0.0844298262854368 )   59 ( 138 , 0.137413538664034 )   60 ( 139 , 0.153773437719468 )   61 ( 140 , 0.131663377525577 )   62 ( 141 , 0.0365008076224060 )   63 ( 142 , 0.139671641218959 )   64 ( 143 , 0.224054163950172 )   65 ( 144 , 0.0224689511014811 )   66 ( 145 , 0.0658520660259523 )   67 ( 146 , 0.00645643566852133 )   68 ( 147 , 0.106472399583359 )   69 ( 148 , 0.292849622005106 )   70 ( 149 , 0.395406303532662 )   71 ( 150 , 0.4786733208721 )   72 ( 151 , 0.522559147860658 )   73 ( 152 , 0.307856561325404 )   74 ( 153 , 0.232200869693549 )   75 ( 154 , 0.234843816989879 )   76 ( 155 , 0.297587397779683 )   77 ( 156 , 0.0175051180814711 )   78 ( 157 , 0.161995734168458 )   79 ( 158 , 0.250592754245553 )   80 ( 159 , 0.0420221699986767 )   81 ( 160 , 0.147268967381269 )   82 ( 161 , 0.189611238153706 )   83 ( 162 , 0.185640770382080 )   84 ( 163 , 0.0975519468189079 )   85 ( 164 , 0.0901563281362927 )   86 ( 165 , 0.0825954052209257 )   87 ( 166 , 0.0471694186434233 )   88 ( 167 , 0.0295529415315716 )   89 ( 168 , 0.0128203168362333 )   90 ( 169 , 0.0138349542777689 )   91 ( 170 , 0.0224838651247996 )   92 ( 171 , 0.0344669011810153 )   93 ( 172 , 0.0279444857456177 )   94 ( 173 , 0.0227158528066251 )   95 ( 174 , 0.00486683043362923 )   96 ( 175 , 0.000234396469782183 )   97 ( 176 , 0.00220728580226925 )   98 ( 177 , 0.00058641573933027 )   99 ( 178 , 0.000783562438534169 )   100 ( 179 , 0.000150598375612709 )   101 ( 180 , 0.0121935662225017 )   102 ( 181 , 0.00155193495635420 )   103 ( 182 , 0.00256036534038095 )   104 ( 183 , 0.00209380541389148 )   105 ( 184 , 0.00261421063866685 )   106 ( 185 , 0.00279284013988246 )   107 ( 186 , 0.0154115552882663 )   108 ( 187 , 0.00251727055605677 )   109 ( 188 , 0.0036303918327274 )   110 ( 189 , 0.00529158621883763 )   111 ( 190 , 0.00640986610767413 )   112 ( 191 , 0.00797408764352348 )   113 ( 192 , 0.0285375914227277 )   114 ( 193 , 0.00800115294879866 )   115 ( 194 , 0.0117858861362645 )   116 ( 195 , 0.0112368925063695 )   117 ( 196 , 0.0485686202755016 )   118 ( 197 , 0.0142851085332056 )   119 ( 198 , 0.0156619001584364 )   120 ( 199 , 0.0196088423020635 )   121 ( 200 , 0.0273400736524660 )   122 ( 201 , 0.0587057343853186 )   123 ( 202 , 0.0251728493514927 )   124 ( 203 , 0.0899484026402124 )   125 ( 204 , 0.0505158888733508 )   126 ( 205 , 0.0951264825137601 )   127 ( 206 , 0.0687011153796124 )   128 ( 207 , 0.182089204108194 )   129 ( 208 , 0.0722328786880153 )   130 ( 209 , 0.207587363088466 )   131 ( 210 , 0.122250961456470 )   132 ( 211 , 0.316463003531463 )   133 ( 212 , 0.221447552476437 )   134 ( 213 , 0.418392285594266 )   135 ( 214 , 0.506498430020446 )   136 ( 215 , 0.757896475382188 )   137 ( 216 , 1.05097660909443 )   140 ( 219 , 1.07850371143802e-06 )   141 ( 220 , 8.62352985020464e-07 )   149 ( 228 , 1.94880683643819e-08 )   152 ( 231 , 1.05067246689574e-08 )   154 ( 233 , 0 )   155 ( 234 , 0 )   156 ( 235 , 0 )   157 ( 236 , 0 )   158 ( 237 , 0 )   159 ( 238 , 0 )   160 ( 239 , 0 )   161 ( 240 , 0 )   162 ( 241 , 0 )   163 ( 242 , 0 )   164 ( 243 , 0 )   165 ( 244 , 0 )   166 ( 245 , 0 )   167 ( 246 , 0 )   168 ( 247 , 0 )   169 ( 248 , 0 )   170 ( 249 , 0 )   171 ( 250 , 0 )   172 ( 251 , 0 )   173 ( 252 , 0 )   174 ( 253 , 0 )   175 ( 254 , 0 )   176 ( 255 , 0 )   177 ( 256 , 0 )   178 ( 257 , 0 )   179 ( 258 , 0 )   180 ( 259 , 0 )   181 ( 260 , 0 )   182 ( 261 , 0 )   183 ( 262 , 0 )   184 ( 263 , 0 )   185 ( 264 , 0 )   186 ( 265 , 0 )   187 ( 266 , 0 )   188 ( 267 , 0 )   189 ( 268 , 0 )   190 ( 269 , 0 )   191 ( 270 , 0 )   192 ( 271 , 0 )   193 ( 272 , 0 )   194 ( 273 , 0 )   195 ( 274 , 0 )   196 ( 275 , 0 )   197 ( 276 , 0 )   198 ( 277 , 0 )   199 ( 278 , 0 )   200 ( 279 , 0 )   201 ( 280 , 0 )   202 ( 281 , 0 )   203 ( 282 , 0 )   204 ( 283 , 0 )   205 ( 284 , 0 )   206 ( 285 , 0 )   207 ( 286 , 0 )   208 ( 287 , 0 )   209 ( 288 , 0 )   210 ( 289 , 0 )   211 ( 290 , 0 )   212 ( 291 , 0 )   213 ( 292 , 0 )   214 ( 293 , 0 )   215 ( 294 , 0 )   216 ( 295 , 0 )   217 ( 296 , 0 )   218 ( 297 , 0 )   219 ( 298 , 0 )   220 ( 299 , 0 )   221 ( 300 , 0 )

Bilan : 
 Sur l'ensemble des comparaisons numeriques de la variable  vTRp , 
 l'ecart de test calcule maximal vaut  2.15646944802797 , la tolerance de test maximale vaut  2.84860742240687 . 


* fin rapport des comparaisons Verifications numeriques automatiques 
**********************************************************************************


******************************************
* Representations graphiques 
******************************************

Les representations graphiques des valeurs attendues par rapport aux valeurs obtenues (un graphique par donnee) sont rangees sous le repertoire  ./../output/lesJeuxDeTest/jeuEQ13_30/representationsGraphiques/ 
ATTENTION, toutes les valeurs lues dans resuAttendu sont tracees mais parmi elles, n'ont aucun sens celles qui n'ont pas ete affectees (valeur  -777  (=VALEUR_INVALIDE) (cf cas ou la tolerance de test n'a pas ete affectee (valeur  -111  (=COMPARAISON_INACTIVE)). 
