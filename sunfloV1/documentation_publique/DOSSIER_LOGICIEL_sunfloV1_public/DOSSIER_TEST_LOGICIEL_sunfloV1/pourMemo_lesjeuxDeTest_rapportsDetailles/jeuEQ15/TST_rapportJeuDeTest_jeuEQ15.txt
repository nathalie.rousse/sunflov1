
[TST_sunfloV1_VerifJeuDeTest_jeuEQ15.R]
Jeu appele/deroule sans MODE_DEBUG. Jeu appele/deroule pour generer un RAPPORT_DETAILLE. 

***********************************************************************************
* 
* Modele VLE sunfloV1, jeu de test jeuEQ15
* 
* Comparaison entre les resultats obtenus et les resultats attendus. 
* 
* Les verifications automatiques sont effectuees, tandis que celles qui sont a effectuer manuellement sont signalees. 
* 
* Le jeu de test jeuEQ15 s'appuie sur le scenario de test scnSunfloV1intact.
* 
***********************************************************************************

Les resultats de simulation sont recuperes dans le repertoire  ./../output/lesScenarios/scnSunfloV1intact/ 
Le repertoire des resultats des verifications est  ./../output/lesJeuxDeTest/jeuEQ15/ 
Notamment le repertoire des representations graphiques est  ./../output/lesJeuxDeTest/jeuEQ15/representationsGraphiques/ 
Le fichier des donnees obtenues en simulation de test est  ./../output/lesScenarios/scnSunfloV1intact/TST_sortiesObtenues_scnSunfloV1intact.csv .
Pour info, le fichier des donnees attendues  ./../test/TST_sortiesAttendues_jeuEQ15.csv N'EXISTE PAS 
Pour memo, resuAttendu[j,nomVar] vaut  -777  si aucune valeur ne lui a ete affectee (ni par fichier, ni par code R) 
Pour info, Le fichier des tolerances de test  ./../test/TST_toleranceTest_jeuEQ15.csv  N'EXISTE PAS.
Pour memo, le tableau des tolerances de test toleranceTest est interprete/utilise de la maniere suivante dans la comparaison des donnees obtenues (resuObtenu) aux donnees attendues (resuAttendu) : si toleranceTest[j,nomVar]= -111 (ie COMPARAISON_INACTIVE) alors resuAttendu[j,nomVar] ne sera pas compare a resuObtenu[j,nomVar]; si toleranceTest[j,nomVar]>=0 alors il sera verifie que resuObtenu[j,nomVar]=resuAttendu[j,nomVar] a toleranceTest[j,nomVar] pres. 

Le fichier  ./../output/lesScenarios/scnSunfloV1intact/TST_sortiesObtenues_scnSunfloV1intact.csv  contient les variables time AAAAnumerique JJnumerique MMnumerique jourDeLannee DBP Eb Ebp Ei LAI SFp TDM AP ChgtPhasePhenoPlante PhasePhenoPlante TT_A0 TT_A2 TT_F1 photo_DatePsgPhasePhenoPlante_CROISSANCEACTIVE_A_FLORAISON photo_DatePsgPhasePhenoPlante_DESSICATION_A_RECOLTEE photo_DatePsgPhasePhenoPlante_FLORAISON_A_MATURATION photo_DatePsgPhasePhenoPlante_GERMINATION_A_JUVENILE photo_DatePsgPhasePhenoPlante_JUVENILE_A_CROISSANCEACTIVE photo_DatePsgPhasePhenoPlante_MATURATION_A_DESSICATION photo_DatePsgPhasePhenoPlante_MATURATION_A_RECOLTEE photo_DatePsgPhasePhenoPlante_NONSEMEE_A_GERMINATION Teff Tmoy ISH1 ISH2 ISH3 ActionFerti ActionIrrig ActionRecolte ActionSemis DoseFerti DoseIrrig EtatConduite densite zSemis CCN CGR CN1 CN2 CN3 CRU CRUm DDM Ea Engrais F1 F2 F3 FE FHN FNIRUE FNLE FR INN INNI N1 N2 N3 Nabs Ncrit Ndenit Nmax Nmine Nsol TNp TNpcrit TNpmax vAA vAA1 vAA2 vDenit vMF vMF1 vMF2 vMine vNA vNAc ATSW C1 C2 C3 D1 D2 D3 ETMcumulDepuisLevee ETP ETPcumulDepuisLevee ETRETM ETRcumulDepuisLevee EVj FHLE FHRUE FHTR FTSW JS PluieCumulDepuisLevee RWCC1 STOCKC1 STOCKC2 STOCKC3 TEC1 TR TRPF TTSW fRacC1 vRac vTR vTRC1 vTRC2 vTRp zC2 zC3 zRac FLe PARi FT FTHN DSF P2 TH photo_TH_aFinMATURATION IRs JSE JSF JSM RDT photo_INN_CROISSANCEACTIVE_A_FLORAISON photo_IRs_aFinMATURATION photo_RDT_aFinMATURATION photo_TDM_CROISSANCEACTIVE_A_FLORAISON ETPP Pluie RG Tn Tx

resuAttendu (sauf jourDeLannee) est affecte par defaut a sa valeur d'invalidite  -777 
toleranceTest (sauf jourDeLannee) est affecte par defaut a sa valeur d'inactivation  -111  (=COMPARAISON_INACTIVE). 

******************************************
* Description du jeu de test  jeuEQ15 , et verifications a effectuer 
******************************************

Jeu de verification de l'equation EQ15 de la publi, ou les resultats attendus correspondent theoriquement a l'equation de la publi. 

********* Les donnees/informations de test :

EQ15 selon la publi : 
[EQ15] dTRC1 = fR * dPTR * W.TR if (zR > zC1) else dPTR * W.TR 
EQ15 traduite en noms du code vle : 
[EQ15] vi.vTRC1 = vi.fRacC1 * vi.vTRp * vs.FHTR if ( vs.zRac > p.zC1) else vi.vTRp *  vs.FHTR 

Pour memo, pseudo-code du traitement relatif a vTRC1 du cote du code du modele sunfloV1 : 
vi.vTRC1(j) = 0 for vi.C1(j) <= 0 ; 
vi.vTRC1(j) = vi.fRacC1(j) * vi.vTRp(j) * vs.FHTR(j) for vs.zRac(j) > p.zC1 ; 
vi.vTRC1(j) = vi.vTRp(j) * vs.FHTR(j) 

Entrees de test injectees, parametres modifies... : voir le rapport TST_rapportScenario_ scnSunfloV1intact .txt 

Definition des resultats attendus : 
Le calcul des resultats attendus [EQ15] servant dans la verification est code sous R. 
Les resultats attendus calcules correspondent theoriquement a l'equation EQ15 de la publi. 
Les indices y respectent vTRC1(j) = fonction_de(j). 

********* Les VERIFICATIONS (automatiques et/ou manuelles) :

Automatique :  Verification numerique de la donnee vs.vTRC1 (du modele contrainte_lumiere). Cette verification n'est faite que si C1(j) > 0.0 (voir pseudo-code du modele sunfloV1 plus haut). La tolerance d'ecart dans les comparaisons entre 'resu attendu' et 'resu obtenu' est 'resu attendu' x  7.5e-06 . 
Manuelle : Pas de verification manuelle supplementaire. 
Remarque :  La valeur de zC1 utilisee dans l'equation [EQ15] pour calculer les resultats attendus est zC1 =  300 (il n'y a pas de valeur zC1 donnee dans la publi).  On peut voir dans le rapport de simulation TST_rapportScenario_ scnSunfloV1intact .txt que le parametre zC1= 300 . 

******************************************

******************************************
* Verifications numeriques automatiques 
******************************************

Les resultats attendus et tolerances de test associees (resuAttendu, toleranceTest) sont archives dans les fichiers  ./../output/lesJeuxDeTest/jeuEQ15/complet_TST_sortiesAttendues_jeuEQ15.csv  et  ./../output/lesJeuxDeTest/jeuEQ15/complet_TST_toleranceTest_jeuEQ15.csv . Il s'agit des attendus complets, dont le contenu a ete defini par fichiers et/ou code R. 
ATTENTION, l'interpretation des resultats attendus (informations de resuAttendu) depend des tolerances de test (informations de toleranceTest), qui doivent avoir ete affectees (via fichier ou code R). 

**********************************************************************************
* debut rapport des comparaisons Verifications numeriques automatiques 
 
 test    ok   pour variable  vTRC1 
              aux indices (entre parentheses: jourDeLannee,ecart entre resultats obtenu et attendu) :    1 ( 80 , 0 )   2 ( 81 , 0 )   3 ( 82 , 0 )   4 ( 83 , 0 )   5 ( 84 , 0 )   6 ( 85 , 0 )   7 ( 86 , 0 )   8 ( 87 , 0 )   9 ( 88 , 0 )   10 ( 89 , 0 )   11 ( 90 , 0 )   12 ( 91 , 0 )   13 ( 92 , 0 )   14 ( 93 , 0 )   15 ( 94 , 0 )   16 ( 95 , 0 )   17 ( 96 , 0 )   18 ( 97 , 0 )   19 ( 98 , 0 )   20 ( 99 , 0 )   21 ( 100 , 0 )   22 ( 101 , 0 )   23 ( 102 , 0 )   24 ( 103 , 0 )   25 ( 104 , 0 )   26 ( 105 , 0 )   27 ( 106 , 0 )   28 ( 107 , 0 )   29 ( 108 , 0 )   30 ( 109 , 0 )   31 ( 110 , 0 )   32 ( 111 , 0 )   33 ( 112 , 0 )   34 ( 113 , 0 )   35 ( 114 , 0 )   36 ( 115 , 0 )   37 ( 116 , 0 )   38 ( 117 , 0 )   39 ( 118 , 0 )   40 ( 119 , 6.2450045135165e-17 )   41 ( 120 , 0 )   42 ( 121 , 1.38777878078145e-17 )   43 ( 122 , 9.71445146547012e-17 )   44 ( 123 , 6.93889390390723e-17 )   45 ( 124 , 6.38378239159465e-16 )   46 ( 125 , 3.05311331771918e-16 )   47 ( 126 , 2.08166817117217e-17 )   48 ( 127 , 6.38378239159465e-16 )   49 ( 128 , 3.60822483003176e-16 )   50 ( 129 , 3.05311331771918e-16 )   51 ( 130 , 9.9920072216264e-16 )   52 ( 131 , 5.55111512312578e-17 )   53 ( 132 , 9.43689570931383e-16 )   54 ( 133 , 1.11022302462516e-16 )   55 ( 134 , 1.11022302462516e-16 )   56 ( 135 , 2.77555756156289e-16 )   57 ( 136 , 5.55111512312578e-16 )   58 ( 137 , 3.33066907387547e-16 )   59 ( 138 , 4.44089209850063e-16 )   60 ( 139 , 3.33066907387547e-15 )   61 ( 140 , 8.88178419700125e-16 )   62 ( 141 , 3.33066907387547e-16 )   63 ( 142 , 2.66453525910038e-15 )   64 ( 143 , 5.10702591327572e-15 )   65 ( 144 , 5.55111512312578e-17 )   66 ( 145 , 1.44328993201270e-15 )   67 ( 146 , 4.9960036108132e-16 )   68 ( 147 , 1.77635683940025e-15 )   69 ( 148 , 1.11022302462516e-15 )   70 ( 149 , 2.22044604925031e-15 )   71 ( 150 , 1.11022302462516e-15 )   72 ( 151 , 2.66453525910038e-15 )   73 ( 152 , 3.10862446895044e-15 )   74 ( 153 , 1.33226762955019e-15 )   75 ( 154 , 4.44089209850063e-16 )   76 ( 155 , 8.88178419700125e-16 )   77 ( 156 , 2.22044604925031e-16 )   78 ( 157 , 1.33226762955019e-15 )   79 ( 158 , 3.10862446895044e-15 )   80 ( 159 , 2.66453525910038e-15 )   81 ( 160 , 8.88178419700125e-15 )   82 ( 161 , 1.33226762955019e-15 )   83 ( 162 , 1.33226762955019e-15 )   84 ( 163 , 3.10862446895044e-15 )   85 ( 164 , 3.99680288865056e-15 )   86 ( 165 , 1.33226762955019e-15 )   87 ( 166 , 2.22044604925031e-15 )   88 ( 167 , 1.77635683940025e-15 )   89 ( 168 , 8.88178419700125e-16 )   90 ( 169 , 3.5527136788005e-15 )   91 ( 170 , 3.77475828372553e-15 )   92 ( 171 , 1.77635683940025e-15 )   93 ( 172 , 3.10862446895044e-15 )   94 ( 173 , 4.44089209850063e-15 )   95 ( 174 , 1.77635683940025e-15 )   96 ( 175 , 4.88498130835069e-15 )   97 ( 176 , 4.44089209850063e-15 )   98 ( 177 , 2.66453525910038e-15 )   99 ( 178 , 2.22044604925031e-15 )   100 ( 179 , 3.10862446895044e-15 )   101 ( 180 , 1.77635683940025e-15 )   102 ( 181 , 3.99680288865056e-15 )   103 ( 182 , 3.5527136788005e-15 )   104 ( 183 , 1.44328993201270e-15 )   105 ( 184 , 3.5527136788005e-15 )   106 ( 185 , 8.88178419700125e-16 )   107 ( 186 , 2.88657986402541e-15 )   108 ( 187 , 3.33066907387547e-15 )   109 ( 188 , 4.88498130835069e-15 )   110 ( 189 , 2.66453525910038e-15 )   111 ( 190 , 2.22044604925031e-16 )   112 ( 191 , 8.88178419700125e-16 )   113 ( 192 , 4.44089209850063e-15 )   114 ( 193 , 3.33066907387547e-15 )   115 ( 194 , 2.22044604925031e-16 )   116 ( 195 , 3.33066907387547e-15 )   117 ( 196 , 4.44089209850063e-15 )   118 ( 197 , 2.88657986402541e-15 )   119 ( 198 , 2.44249065417534e-15 )   120 ( 199 , 7.7715611723761e-15 )   121 ( 200 , 0 )   122 ( 201 , 1.88737914186277e-15 )   123 ( 202 , 9.9920072216264e-16 )   124 ( 203 , 2.33146835171283e-15 )   125 ( 204 , 8.88178419700125e-16 )   126 ( 205 , 2.22044604925031e-16 )   127 ( 206 , 1.99840144432528e-15 )   128 ( 207 , 1.11022302462516e-15 )   129 ( 208 , 7.7715611723761e-16 )   130 ( 209 , 2.22044604925031e-16 )   131 ( 210 , 4.44089209850063e-16 )   132 ( 211 , 2.22044604925031e-16 )   133 ( 212 , 4.44089209850063e-16 )   134 ( 213 , 6.66133814775094e-16 )   135 ( 214 , 1.38777878078145e-15 )   136 ( 215 , 2.22044604925031e-16 )   137 ( 216 , 1.33226762955019e-15 )   138 ( 217 , 5.55111512312578e-16 )   139 ( 218 , 1.11022302462516e-16 )   140 ( 219 , 2.64697796016969e-22 )   141 ( 220 , 5.29395592033938e-23 )   142 ( 221 , 1.58818677610181e-22 )   143 ( 222 , 1.85288457211878e-22 )   144 ( 223 , 0 )   145 ( 224 , 7.27918939046664e-22 )   146 ( 225 , 1.85288457211878e-22 )   147 ( 226 , 1.05879118406788e-22 )   148 ( 227 , 9.59529510561512e-23 )   149 ( 228 , 2.81241408268029e-23 )   150 ( 229 , 5.79026428787119e-24 )   151 ( 230 , 1.15805285757424e-23 )   152 ( 231 , 9.92616735063633e-24 )   153 ( 232 , 9.0989867380833e-24 )   154 ( 233 , 0 )   155 ( 234 , 0 )   156 ( 235 , 0 )   157 ( 236 , 0 )   158 ( 237 , 0 )   159 ( 238 , 0 )   160 ( 239 , 0 )   161 ( 240 , 0 )   162 ( 241 , 0 )   163 ( 242 , 0 )   164 ( 243 , 0 )   165 ( 244 , 0 )   171 ( 250 , 0 )   172 ( 251 , 0 )   173 ( 252 , 0 )   174 ( 253 , 0 )   175 ( 254 , 0 )   176 ( 255 , 0 )   177 ( 256 , 0 )   178 ( 257 , 0 )   179 ( 258 , 0 )   180 ( 259 , 0 )   181 ( 260 , 0 )   182 ( 261 , 0 )   183 ( 262 , 0 )   184 ( 263 , 0 )   185 ( 264 , 0 )   186 ( 265 , 0 )   187 ( 266 , 0 )   188 ( 267 , 0 )   189 ( 268 , 0 )   190 ( 269 , 0 )   191 ( 270 , 0 )   192 ( 271 , 0 )   193 ( 272 , 0 )   194 ( 273 , 0 )   195 ( 274 , 0 )   196 ( 275 , 0 )   197 ( 276 , 0 )   198 ( 277 , 0 )   199 ( 278 , 0 )   200 ( 279 , 0 )   201 ( 280 , 0 )   202 ( 281 , 0 )   203 ( 282 , 0 )   204 ( 283 , 0 )   205 ( 284 , 0 )   206 ( 285 , 0 )   207 ( 286 , 0 )   208 ( 287 , 0 )   209 ( 288 , 0 )   210 ( 289 , 0 )   211 ( 290 , 0 )   212 ( 291 , 0 )   213 ( 292 , 0 )   214 ( 293 , 0 )   215 ( 294 , 0 )   216 ( 295 , 0 )   217 ( 296 , 0 )   218 ( 297 , 0 )   219 ( 298 , 0 )   220 ( 299 , 0 )   221 ( 300 , 0 )

Bilan : 
 Sur l'ensemble des comparaisons numeriques de la variable  vTRC1 , 
 l'ecart de test calcule maximal vaut  8.88178419700125e-15 , la tolerance de test maximale vaut  2.17280495944024e-05 . 


* fin rapport des comparaisons Verifications numeriques automatiques 
**********************************************************************************


******************************************
* Representations graphiques 
******************************************

Les representations graphiques des valeurs attendues par rapport aux valeurs obtenues (un graphique par donnee) sont rangees sous le repertoire  ./../output/lesJeuxDeTest/jeuEQ15/representationsGraphiques/ 
ATTENTION, toutes les valeurs lues dans resuAttendu sont tracees mais parmi elles, n'ont aucun sens celles qui n'ont pas ete affectees (valeur  -777  (=VALEUR_INVALIDE) (cf cas ou la tolerance de test n'a pas ete affectee (valeur  -111  (=COMPARAISON_INACTIVE)). 
