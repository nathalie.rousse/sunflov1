
[TST_sunfloV1_VerifJeuDeTest_jeuEQ19.R]
Jeu appele/deroule sans MODE_DEBUG. Jeu appele/deroule pour generer un RAPPORT_DETAILLE. 

***********************************************************************************
* 
* Modele VLE sunfloV1, jeu de test jeuEQ19
* 
* Comparaison entre les resultats obtenus et les resultats attendus. 
* 
* Les verifications automatiques sont effectuees, tandis que celles qui sont a effectuer manuellement sont signalees. 
* 
* Le jeu de test jeuEQ19 s'appuie sur le scenario de test scnSunfloV1intact.
* 
***********************************************************************************

Les resultats de simulation sont recuperes dans le repertoire  ./../output/lesScenarios/scnSunfloV1intact/ 
Le repertoire des resultats des verifications est  ./../output/lesJeuxDeTest/jeuEQ19/ 
Notamment le repertoire des representations graphiques est  ./../output/lesJeuxDeTest/jeuEQ19/representationsGraphiques/ 
Le fichier des donnees obtenues en simulation de test est  ./../output/lesScenarios/scnSunfloV1intact/TST_sortiesObtenues_scnSunfloV1intact.csv .
Pour info, le fichier des donnees attendues  ./../test/TST_sortiesAttendues_jeuEQ19.csv N'EXISTE PAS 
Pour memo, resuAttendu[j,nomVar] vaut  -777  si aucune valeur ne lui a ete affectee (ni par fichier, ni par code R) 
Pour info, Le fichier des tolerances de test  ./../test/TST_toleranceTest_jeuEQ19.csv  N'EXISTE PAS.
Pour memo, le tableau des tolerances de test toleranceTest est interprete/utilise de la maniere suivante dans la comparaison des donnees obtenues (resuObtenu) aux donnees attendues (resuAttendu) : si toleranceTest[j,nomVar]= -111 (ie COMPARAISON_INACTIVE) alors resuAttendu[j,nomVar] ne sera pas compare a resuObtenu[j,nomVar]; si toleranceTest[j,nomVar]>=0 alors il sera verifie que resuObtenu[j,nomVar]=resuAttendu[j,nomVar] a toleranceTest[j,nomVar] pres. 

Le fichier  ./../output/lesScenarios/scnSunfloV1intact/TST_sortiesObtenues_scnSunfloV1intact.csv  contient les variables time AAAAnumerique JJnumerique MMnumerique jourDeLannee DBP Eb Ebp Ei LAI SFp TDM AP ChgtPhasePhenoPlante PhasePhenoPlante TT_A0 TT_A2 TT_F1 photo_DatePsgPhasePhenoPlante_CROISSANCEACTIVE_A_FLORAISON photo_DatePsgPhasePhenoPlante_DESSICATION_A_RECOLTEE photo_DatePsgPhasePhenoPlante_FLORAISON_A_MATURATION photo_DatePsgPhasePhenoPlante_GERMINATION_A_JUVENILE photo_DatePsgPhasePhenoPlante_JUVENILE_A_CROISSANCEACTIVE photo_DatePsgPhasePhenoPlante_MATURATION_A_DESSICATION photo_DatePsgPhasePhenoPlante_MATURATION_A_RECOLTEE photo_DatePsgPhasePhenoPlante_NONSEMEE_A_GERMINATION Teff Tmoy ISH1 ISH2 ISH3 ActionFerti ActionIrrig ActionRecolte ActionSemis DoseFerti DoseIrrig EtatConduite densite zSemis CCN CGR CN1 CN2 CN3 CRU CRUm DDM Ea Engrais F1 F2 F3 FE FHN FNIRUE FNLE FR INN INNI N1 N2 N3 Nabs Ncrit Ndenit Nmax Nmine Nsol TNp TNpcrit TNpmax vAA vAA1 vAA2 vDenit vMF vMF1 vMF2 vMine vNA vNAc ATSW C1 C2 C3 D1 D2 D3 ETMcumulDepuisLevee ETP ETPcumulDepuisLevee ETRETM ETRcumulDepuisLevee EVj FHLE FHRUE FHTR FTSW JS PluieCumulDepuisLevee RWCC1 STOCKC1 STOCKC2 STOCKC3 TEC1 TR TRPF TTSW fRacC1 vRac vTR vTRC1 vTRC2 vTRp zC2 zC3 zRac FLe PARi FT FTHN DSF P2 TH photo_TH_aFinMATURATION IRs JSE JSF JSM RDT photo_INN_CROISSANCEACTIVE_A_FLORAISON photo_IRs_aFinMATURATION photo_RDT_aFinMATURATION photo_TDM_CROISSANCEACTIVE_A_FLORAISON ETPP Pluie RG Tn Tx

resuAttendu (sauf jourDeLannee) est affecte par defaut a sa valeur d'invalidite  -777 
toleranceTest (sauf jourDeLannee) est affecte par defaut a sa valeur d'inactivation  -111  (=COMPARAISON_INACTIVE). 

******************************************
* Description du jeu de test  jeuEQ19 , et verifications a effectuer 
******************************************

Jeu de verification de l'equation EQ19 de la publi, ou les resultats attendus correspondent theoriquement a l'equation de la publi. 

********* Les donnees/informations de test :

EQ19 selon la publi : 
[EQ19] W.RUE = -1 + 2 / ( 1 + exp( a   * FTSW ) ) 
[EQ19] W.LE  = -1 + 2 / ( 1 + exp( a'  * FTSW ) ) 
[EQ19] W.TR  = -1 + 2 / ( 1 + exp( a'' * FTSW ) ) 

EQ19 traduite en noms du code vle : 
[EQ19] vi.FHRUE = -1 + 2 / ( 1 + exp( (p.a_Pho + p.a_TR)  * vi.FTSW ) ) 
[EQ19] vi.FHLE  = -1 + 2 / ( 1 + exp( p.a_LE  * vi.FTSW ) ) 
[EQ19] vi.FHTR  = -1 + 2 / ( 1 + exp( p.a_TR * vi.FTSW ) ) 

Pour memo, pseudo-code du traitement relatif a FHRUE, FHLE, FHTR du cote du code du modele sunfloV1 : 
vs.FHRUE(j) =  1 for TT_A2 = 0 ; = -1 + 2 / ( 1 + exp( ( p.a_Pho + p.a_TR ) * vi.FTSW(j) ) ) by default 
vs.FHLE(j) = 1 for TT_A2 = 0 ; = -1 + 2 / ( 1 + exp( p.a_LE * vi.FTSW(j) ) ) by default 
vs.FHTR(j) = 1 for TT_A2 = 0 ; = -1 + 2 / ( 1 + exp( p.a_TR * vi.FTSW(j) ) ) by default 


Entrees de test injectees, parametres modifies... : voir le rapport TST_rapportScenario_ scnSunfloV1intact .txt 

Definition des resultats attendus : 
Le calcul des resultats attendus [EQ19] servant dans la verification est code sous R. 
Les resultats attendus calcules correspondent theoriquement a l'equation EQ19 de la publi. 
Les indices y respectent FHRUE(j) = fonction_de(j), FHLE(j) = fonction_de(j), FHTR(j) = fonction_de(j). 

********* Les VERIFICATIONS (automatiques et/ou manuelles) :

Automatique :  Verification numerique des donnees vi.FHRUE, vi.FHLE, vi.FHTR (du modele contrainte_eau). Cette verification n'est faite que pour TT_A2 non nul (voir pseudo-code du modele sunfloV1 plus haut). La tolerance d'ecart dans les comparaisons entre 'resu attendu' et 'resu obtenu' est 'resu attendu' x  3e-06 . 
Manuelle : Pas de verification manuelle supplementaire. 
Remarque :  Aucune valeur n'est donnee dans la publi pour aucun des parametres de EQ19 (a, a', a'').  Les valeurs de parametres utilisees dans l'equation [EQ19] pour calculer les resultats attendus sont a_Pho= -25 , a_TR= -5.651 , a_LE= -3.81 . 
 Les constantes PHASEPHENOPLANTE_JUVENILE= 2  et PHASEPHENOPLANTE_RECOLTEE= 7  sont utilisees dans l'equation [EQ19] pour calculer les resultats attendus. 
  On peut voir dans le rapport de simulation TST_rapportScenario_ scnSunfloV1intact .txt que tous ces parametres ont bien les valeurs annoncees ici. 

******************************************

******************************************
* Verifications numeriques automatiques 
******************************************

Les resultats attendus et tolerances de test associees (resuAttendu, toleranceTest) sont archives dans les fichiers  ./../output/lesJeuxDeTest/jeuEQ19/complet_TST_sortiesAttendues_jeuEQ19.csv  et  ./../output/lesJeuxDeTest/jeuEQ19/complet_TST_toleranceTest_jeuEQ19.csv . Il s'agit des attendus complets, dont le contenu a ete defini par fichiers et/ou code R. 
ATTENTION, l'interpretation des resultats attendus (informations de resuAttendu) depend des tolerances de test (informations de toleranceTest), qui doivent avoir ete affectees (via fichier ou code R). 

**********************************************************************************
* debut rapport des comparaisons Verifications numeriques automatiques 
 
 test    ok   pour variable  FHLE 
              aux indices (entre parentheses: jourDeLannee,ecart entre resultats obtenu et attendu) :    39 ( 118 , 5.55111512312578e-16 )   40 ( 119 , 3.33066907387547e-16 )   41 ( 120 , 1.11022302462516e-16 )   42 ( 121 , 2.22044604925031e-16 )   43 ( 122 , 5.55111512312578e-16 )   44 ( 123 , 5.55111512312578e-16 )   45 ( 124 , 3.33066907387547e-16 )   46 ( 125 , 1.11022302462516e-16 )   47 ( 126 , 4.44089209850063e-16 )   48 ( 127 , 4.44089209850063e-16 )   49 ( 128 , 5.55111512312578e-16 )   50 ( 129 , 1.11022302462516e-16 )   51 ( 130 , 3.33066907387547e-16 )   52 ( 131 , 3.33066907387547e-16 )   53 ( 132 , 3.33066907387547e-16 )   54 ( 133 , 1.11022302462516e-16 )   55 ( 134 , 3.33066907387547e-16 )   56 ( 135 , 1.11022302462516e-16 )   57 ( 136 , 4.44089209850063e-16 )   58 ( 137 , 3.33066907387547e-16 )   59 ( 138 , 5.55111512312578e-16 )   60 ( 139 , 4.44089209850063e-16 )   61 ( 140 , 5.55111512312578e-16 )   62 ( 141 , 1.11022302462516e-16 )   63 ( 142 , 2.22044604925031e-16 )   64 ( 143 , 5.55111512312578e-16 )   65 ( 144 , 1.11022302462516e-16 )   66 ( 145 , 2.22044604925031e-16 )   67 ( 146 , 6.66133814775094e-16 )   68 ( 147 , 3.33066907387547e-16 )   69 ( 148 , 1.11022302462516e-16 )   70 ( 149 , 2.22044604925031e-16 )   71 ( 150 , 3.33066907387547e-16 )   72 ( 151 , 2.22044604925031e-16 )   73 ( 152 , 0 )   74 ( 153 , 3.33066907387547e-16 )   75 ( 154 , 4.44089209850063e-16 )   76 ( 155 , 1.11022302462516e-16 )   77 ( 156 , 3.33066907387547e-16 )   78 ( 157 , 7.7715611723761e-16 )   79 ( 158 , 3.33066907387547e-16 )   80 ( 159 , 1.11022302462516e-16 )   81 ( 160 , 1.11022302462516e-16 )   82 ( 161 , 3.33066907387547e-16 )   83 ( 162 , 0 )   84 ( 163 , 3.33066907387547e-16 )   85 ( 164 , 1.11022302462516e-16 )   86 ( 165 , 1.11022302462516e-16 )   87 ( 166 , 5.55111512312578e-16 )   88 ( 167 , 3.33066907387547e-16 )   89 ( 168 , 5.55111512312578e-16 )   90 ( 169 , 1.11022302462516e-16 )   91 ( 170 , 3.88578058618805e-16 )   92 ( 171 , 1.11022302462516e-16 )   93 ( 172 , 4.44089209850063e-16 )   94 ( 173 , 7.7715611723761e-16 )   95 ( 174 , 4.44089209850063e-16 )   96 ( 175 , 1.11022302462516e-16 )   97 ( 176 , 2.22044604925031e-16 )   98 ( 177 , 7.7715611723761e-16 )   99 ( 178 , 1.11022302462516e-16 )   100 ( 179 , 1.11022302462516e-16 )   101 ( 180 , 2.22044604925031e-16 )   102 ( 181 , 0 )   103 ( 182 , 4.44089209850063e-16 )   104 ( 183 , 1.05471187339390e-15 )   105 ( 184 , 6.10622663543836e-16 )   106 ( 185 , 1.66533453693773e-16 )   107 ( 186 , 8.88178419700125e-16 )   108 ( 187 , 0 )   109 ( 188 , 2.77555756156289e-16 )   110 ( 189 , 3.88578058618805e-16 )   111 ( 190 , 1.11022302462516e-16 )   112 ( 191 , 2.22044604925031e-16 )   113 ( 192 , 1.11022302462516e-16 )   114 ( 193 , 2.22044604925031e-16 )   115 ( 194 , 3.33066907387547e-16 )   116 ( 195 , 1.11022302462516e-16 )   117 ( 196 , 5.55111512312578e-16 )   118 ( 197 , 3.33066907387547e-16 )   119 ( 198 , 1.11022302462516e-16 )   120 ( 199 , 3.33066907387547e-16 )   121 ( 200 , 0 )   122 ( 201 , 7.21644966006352e-16 )   123 ( 202 , 2.22044604925031e-16 )   124 ( 203 , 2.77555756156289e-16 )   125 ( 204 , 1.66533453693773e-16 )   126 ( 205 , 2.22044604925031e-16 )   127 ( 206 , 6.10622663543836e-16 )   128 ( 207 , 9.43689570931383e-16 )   129 ( 208 , 5.55111512312578e-17 )   130 ( 209 , 2.77555756156289e-16 )   131 ( 210 , 3.88578058618805e-16 )   132 ( 211 , 0 )   133 ( 212 , 6.10622663543836e-16 )   134 ( 213 , 3.88578058618805e-16 )   135 ( 214 , 2.77555756156289e-16 )   136 ( 215 , 4.44089209850063e-16 )   137 ( 216 , 4.44089209850063e-16 )   138 ( 217 , 1.11022302462516e-16 )   139 ( 218 , 6.10622663543836e-16 )   140 ( 219 , 0 )   141 ( 220 , 1.11022302462516e-15 )   142 ( 221 , 2.77555756156289e-16 )   143 ( 222 , 0 )   144 ( 223 , 3.88578058618805e-16 )   145 ( 224 , 3.33066907387547e-16 )   146 ( 225 , 3.88578058618805e-16 )   147 ( 226 , 2.77555756156289e-16 )   148 ( 227 , 2.22044604925031e-16 )   149 ( 228 , 5.55111512312578e-16 )   150 ( 229 , 6.10622663543836e-16 )   151 ( 230 , 0 )   152 ( 231 , 5.55111512312578e-16 )   153 ( 232 , 2.22044604925031e-16 )   154 ( 233 , 3.88578058618805e-16 )   155 ( 234 , 2.77555756156289e-16 ) 
 test    ok   pour variable  FHRUE 
              aux indices (entre parentheses: jourDeLannee,ecart entre resultats obtenu et attendu) :    39 ( 118 , 2.22044604925031e-16 )   40 ( 119 , 3.33066907387547e-16 )   41 ( 120 , 1.11022302462516e-16 )   42 ( 121 , 0 )   43 ( 122 , 3.33066907387547e-16 )   44 ( 123 , 0 )   45 ( 124 , 2.22044604925031e-16 )   46 ( 125 , 1.11022302462516e-16 )   47 ( 126 , 1.11022302462516e-16 )   48 ( 127 , 1.11022302462516e-16 )   49 ( 128 , 3.33066907387547e-16 )   50 ( 129 , 1.11022302462516e-16 )   51 ( 130 , 0 )   52 ( 131 , 1.11022302462516e-16 )   53 ( 132 , 4.44089209850063e-16 )   54 ( 133 , 5.55111512312578e-16 )   55 ( 134 , 2.22044604925031e-16 )   56 ( 135 , 3.33066907387547e-16 )   57 ( 136 , 2.22044604925031e-16 )   58 ( 137 , 0 )   59 ( 138 , 2.22044604925031e-16 )   60 ( 139 , 4.44089209850063e-16 )   61 ( 140 , 1.11022302462516e-16 )   62 ( 141 , 4.44089209850063e-16 )   63 ( 142 , 3.33066907387547e-16 )   64 ( 143 , 3.33066907387547e-16 )   65 ( 144 , 4.44089209850063e-16 )   66 ( 145 , 0 )   67 ( 146 , 1.11022302462516e-16 )   68 ( 147 , 3.33066907387547e-16 )   69 ( 148 , 5.55111512312578e-16 )   70 ( 149 , 3.33066907387547e-16 )   71 ( 150 , 1.11022302462516e-16 )   72 ( 151 , 3.33066907387547e-16 )   73 ( 152 , 5.55111512312578e-16 )   74 ( 153 , 4.44089209850063e-16 )   75 ( 154 , 2.22044604925031e-16 )   76 ( 155 , 1.11022302462516e-16 )   77 ( 156 , 4.44089209850063e-16 )   78 ( 157 , 5.55111512312578e-16 )   79 ( 158 , 1.11022302462516e-16 )   80 ( 159 , 4.44089209850063e-16 )   81 ( 160 , 3.33066907387547e-16 )   82 ( 161 , 2.22044604925031e-16 )   83 ( 162 , 1.11022302462516e-16 )   84 ( 163 , 0 )   85 ( 164 , 5.55111512312578e-16 )   86 ( 165 , 4.44089209850063e-16 )   87 ( 166 , 2.22044604925031e-16 )   88 ( 167 , 4.44089209850063e-16 )   89 ( 168 , 1.11022302462516e-16 )   90 ( 169 , 2.22044604925031e-16 )   91 ( 170 , 4.44089209850063e-16 )   92 ( 171 , 6.66133814775094e-16 )   93 ( 172 , 3.33066907387547e-16 )   94 ( 173 , 2.22044604925031e-16 )   95 ( 174 , 0 )   96 ( 175 , 2.22044604925031e-16 )   97 ( 176 , 6.66133814775094e-16 )   98 ( 177 , 2.22044604925031e-16 )   99 ( 178 , 4.44089209850063e-16 )   100 ( 179 , 1.11022302462516e-16 )   101 ( 180 , 3.33066907387547e-16 )   102 ( 181 , 1.11022302462516e-16 )   103 ( 182 , 2.22044604925031e-16 )   104 ( 183 , 7.7715611723761e-16 )   105 ( 184 , 1.11022302462516e-16 )   106 ( 185 , 3.33066907387547e-16 )   107 ( 186 , 2.22044604925031e-16 )   108 ( 187 , 3.33066907387547e-16 )   109 ( 188 , 1.11022302462516e-16 )   110 ( 189 , 3.33066907387547e-16 )   111 ( 190 , 0 )   112 ( 191 , 3.33066907387547e-16 )   113 ( 192 , 3.33066907387547e-16 )   114 ( 193 , 4.44089209850063e-16 )   115 ( 194 , 0 )   116 ( 195 , 2.22044604925031e-16 )   117 ( 196 , 4.44089209850063e-16 )   118 ( 197 , 6.66133814775094e-16 )   119 ( 198 , 3.33066907387547e-16 )   120 ( 199 , 3.33066907387547e-16 )   121 ( 200 , 1.11022302462516e-16 )   122 ( 201 , 4.44089209850063e-16 )   123 ( 202 , 0 )   124 ( 203 , 7.7715611723761e-16 )   125 ( 204 , 2.22044604925031e-16 )   126 ( 205 , 4.44089209850063e-16 )   127 ( 206 , 5.55111512312578e-16 )   128 ( 207 , 3.33066907387547e-16 )   129 ( 208 , 2.22044604925031e-16 )   130 ( 209 , 4.44089209850063e-16 )   131 ( 210 , 0 )   132 ( 211 , 2.22044604925031e-16 )   133 ( 212 , 3.33066907387547e-16 )   134 ( 213 , 3.33066907387547e-16 )   135 ( 214 , 1.11022302462516e-16 )   136 ( 215 , 1.11022302462516e-16 )   137 ( 216 , 2.22044604925031e-16 )   138 ( 217 , 1.11022302462516e-16 )   139 ( 218 , 2.22044604925031e-16 )   140 ( 219 , 0 )   141 ( 220 , 2.22044604925031e-16 )   142 ( 221 , 2.22044604925031e-16 )   143 ( 222 , 1.11022302462516e-16 )   144 ( 223 , 1.11022302462516e-16 )   145 ( 224 , 2.22044604925031e-16 )   146 ( 225 , 4.44089209850063e-16 )   147 ( 226 , 3.33066907387547e-16 )   148 ( 227 , 4.44089209850063e-16 )   149 ( 228 , 3.33066907387547e-16 )   150 ( 229 , 5.55111512312578e-16 )   151 ( 230 , 2.22044604925031e-16 )   152 ( 231 , 1.11022302462516e-16 )   153 ( 232 , 2.22044604925031e-16 )   154 ( 233 , 0 )   155 ( 234 , 1.11022302462516e-16 ) 
 test    ok   pour variable  FHTR 
              aux indices (entre parentheses: jourDeLannee,ecart entre resultats obtenu et attendu) :    39 ( 118 , 3.33066907387547e-16 )   40 ( 119 , 0 )   41 ( 120 , 2.22044604925031e-16 )   42 ( 121 , 1.11022302462516e-16 )   43 ( 122 , 0 )   44 ( 123 , 3.33066907387547e-16 )   45 ( 124 , 1.11022302462516e-16 )   46 ( 125 , 1.11022302462516e-16 )   47 ( 126 , 0 )   48 ( 127 , 4.44089209850063e-16 )   49 ( 128 , 3.33066907387547e-16 )   50 ( 129 , 1.11022302462516e-16 )   51 ( 130 , 4.44089209850063e-16 )   52 ( 131 , 4.44089209850063e-16 )   53 ( 132 , 2.22044604925031e-16 )   54 ( 133 , 2.22044604925031e-16 )   55 ( 134 , 4.44089209850063e-16 )   56 ( 135 , 4.44089209850063e-16 )   57 ( 136 , 4.44089209850063e-16 )   58 ( 137 , 1.11022302462516e-16 )   59 ( 138 , 4.44089209850063e-16 )   60 ( 139 , 3.33066907387547e-16 )   61 ( 140 , 3.33066907387547e-16 )   62 ( 141 , 3.33066907387547e-16 )   63 ( 142 , 1.11022302462516e-16 )   64 ( 143 , 2.22044604925031e-16 )   65 ( 144 , 5.55111512312578e-16 )   66 ( 145 , 3.33066907387547e-16 )   67 ( 146 , 2.22044604925031e-16 )   68 ( 147 , 0 )   69 ( 148 , 5.55111512312578e-16 )   70 ( 149 , 1.11022302462516e-16 )   71 ( 150 , 3.33066907387547e-16 )   72 ( 151 , 2.22044604925031e-16 )   73 ( 152 , 1.11022302462516e-16 )   74 ( 153 , 4.44089209850063e-16 )   75 ( 154 , 2.22044604925031e-16 )   76 ( 155 , 2.22044604925031e-16 )   77 ( 156 , 4.44089209850063e-16 )   78 ( 157 , 5.55111512312578e-16 )   79 ( 158 , 4.44089209850063e-16 )   80 ( 159 , 3.33066907387547e-16 )   81 ( 160 , 4.44089209850063e-16 )   82 ( 161 , 6.66133814775094e-16 )   83 ( 162 , 5.55111512312578e-16 )   84 ( 163 , 0 )   85 ( 164 , 3.33066907387547e-16 )   86 ( 165 , 1.11022302462516e-16 )   87 ( 166 , 4.44089209850063e-16 )   88 ( 167 , 6.66133814775094e-16 )   89 ( 168 , 0 )   90 ( 169 , 2.22044604925031e-16 )   91 ( 170 , 5.55111512312578e-16 )   92 ( 171 , 3.33066907387547e-16 )   93 ( 172 , 1.11022302462516e-16 )   94 ( 173 , 4.44089209850063e-16 )   95 ( 174 , 6.66133814775094e-16 )   96 ( 175 , 2.22044604925031e-16 )   97 ( 176 , 5.55111512312578e-16 )   98 ( 177 , 1.11022302462516e-16 )   99 ( 178 , 5.55111512312578e-16 )   100 ( 179 , 1.11022302462516e-16 )   101 ( 180 , 0 )   102 ( 181 , 1.11022302462516e-15 )   103 ( 182 , 5.55111512312578e-16 )   104 ( 183 , 3.33066907387547e-16 )   105 ( 184 , 1.22124532708767e-15 )   106 ( 185 , 1.11022302462516e-16 )   107 ( 186 , 1.11022302462516e-15 )   108 ( 187 , 9.9920072216264e-16 )   109 ( 188 , 7.7715611723761e-16 )   110 ( 189 , 7.7715611723761e-16 )   111 ( 190 , 1.11022302462516e-16 )   112 ( 191 , 2.22044604925031e-16 )   113 ( 192 , 5.55111512312578e-16 )   114 ( 193 , 0 )   115 ( 194 , 1.11022302462516e-16 )   116 ( 195 , 4.44089209850063e-16 )   117 ( 196 , 3.33066907387547e-16 )   118 ( 197 , 4.44089209850063e-16 )   119 ( 198 , 1.11022302462516e-16 )   120 ( 199 , 2.22044604925031e-16 )   121 ( 200 , 3.33066907387547e-16 )   122 ( 201 , 7.7715611723761e-16 )   123 ( 202 , 3.33066907387547e-16 )   124 ( 203 , 1.33226762955019e-15 )   125 ( 204 , 5.55111512312578e-16 )   126 ( 205 , 7.7715611723761e-16 )   127 ( 206 , 9.9920072216264e-16 )   128 ( 207 , 9.9920072216264e-16 )   129 ( 208 , 9.9920072216264e-16 )   130 ( 209 , 3.33066907387547e-16 )   131 ( 210 , 4.44089209850063e-16 )   132 ( 211 , 1.22124532708767e-15 )   133 ( 212 , 1.66533453693773e-16 )   134 ( 213 , 1.16573417585641e-15 )   135 ( 214 , 6.66133814775094e-16 )   136 ( 215 , 2.77555756156289e-16 )   137 ( 216 , 5.55111512312578e-17 )   138 ( 217 , 6.10622663543836e-16 )   139 ( 218 , 6.66133814775094e-16 )   140 ( 219 , 4.9960036108132e-16 )   141 ( 220 , 8.88178419700125e-16 )   142 ( 221 , 8.88178419700125e-16 )   143 ( 222 , 5.55111512312578e-16 )   144 ( 223 , 4.44089209850063e-16 )   145 ( 224 , 1.11022302462516e-16 )   146 ( 225 , 6.66133814775094e-16 )   147 ( 226 , 5.55111512312578e-16 )   148 ( 227 , 0 )   149 ( 228 , 6.66133814775094e-16 )   150 ( 229 , 3.33066907387547e-16 )   151 ( 230 , 1.11022302462516e-16 )   152 ( 231 , 1.11022302462516e-16 )   153 ( 232 , 5.55111512312578e-16 )   154 ( 233 , 1.11022302462516e-16 )   155 ( 234 , 5.55111512312578e-16 )

Bilan : 
 Sur l'ensemble des comparaisons numeriques de la variable  FHLE , 
 l'ecart de test calcule maximal vaut  1.11022302462516e-15 , la tolerance de test maximale vaut  2.89920319816377e-06 . 
 Sur l'ensemble des comparaisons numeriques de la variable  FHRUE , 
 l'ecart de test calcule maximal vaut  7.7715611723761e-16 , la tolerance de test maximale vaut  2.99999999999996e-06 . 
 Sur l'ensemble des comparaisons numeriques de la variable  FHTR , 
 l'ecart de test calcule maximal vaut  1.33226762955019e-15 , la tolerance de test maximale vaut  2.98568531852693e-06 . 


* fin rapport des comparaisons Verifications numeriques automatiques 
**********************************************************************************


******************************************
* Representations graphiques 
******************************************

Les representations graphiques des valeurs attendues par rapport aux valeurs obtenues (un graphique par donnee) sont rangees sous le repertoire  ./../output/lesJeuxDeTest/jeuEQ19/representationsGraphiques/ 
ATTENTION, toutes les valeurs lues dans resuAttendu sont tracees mais parmi elles, n'ont aucun sens celles qui n'ont pas ete affectees (valeur  -777  (=VALEUR_INVALIDE) (cf cas ou la tolerance de test n'a pas ete affectee (valeur  -111  (=COMPARAISON_INACTIVE)). 
