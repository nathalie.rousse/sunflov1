
[TST_sunfloV1_VerifJeuDeTest_jeuDeroulementOk.R]
Jeu appele/deroule sans MODE_DEBUG. Jeu appele/deroule pour generer un RAPPORT_DETAILLE. 

***********************************************************************************
* 
* Modele VLE sunfloV1, jeu de test jeuDeroulementOk
* 
* Comparaison entre les resultats obtenus et les resultats attendus. 
* 
* Les verifications automatiques sont effectuees, tandis que celles qui sont a effectuer manuellement sont signalees. 
* 
* Le jeu de test jeuDeroulementOk s'appuie sur le scenario de test scnSunfloV1intact.
* 
***********************************************************************************

Les resultats de simulation sont recuperes dans le repertoire  ./../output/lesScenarios/scnSunfloV1intact/ 
Le repertoire des resultats des verifications est  ./../output/lesJeuxDeTest/jeuDeroulementOk/ 
Notamment le repertoire des representations graphiques est  ./../output/lesJeuxDeTest/jeuDeroulementOk/representationsGraphiques/ 
Le fichier des donnees obtenues en simulation de test est  ./../output/lesScenarios/scnSunfloV1intact/TST_sortiesObtenues_scnSunfloV1intact.csv .
Pour info, le fichier des donnees attendues  ./../test/TST_sortiesAttendues_jeuDeroulementOk.csv N'EXISTE PAS 
Pour memo, resuAttendu[j,nomVar] vaut  -777  si aucune valeur ne lui a ete affectee (ni par fichier, ni par code R) 
Pour info, Le fichier des tolerances de test  ./../test/TST_toleranceTest_jeuDeroulementOk.csv  N'EXISTE PAS.
Pour memo, le tableau des tolerances de test toleranceTest est interprete/utilise de la maniere suivante dans la comparaison des donnees obtenues (resuObtenu) aux donnees attendues (resuAttendu) : si toleranceTest[j,nomVar]= -111 (ie COMPARAISON_INACTIVE) alors resuAttendu[j,nomVar] ne sera pas compare a resuObtenu[j,nomVar]; si toleranceTest[j,nomVar]>=0 alors il sera verifie que resuObtenu[j,nomVar]=resuAttendu[j,nomVar] a toleranceTest[j,nomVar] pres. 

Le fichier  ./../output/lesScenarios/scnSunfloV1intact/TST_sortiesObtenues_scnSunfloV1intact.csv  contient les variables time AAAAnumerique JJnumerique MMnumerique jourDeLannee DBP Eb Ebp Ei LAI SFp TDM AP ChgtPhasePhenoPlante PhasePhenoPlante TT_A0 TT_A2 TT_F1 photo_DatePsgPhasePhenoPlante_CROISSANCEACTIVE_A_FLORAISON photo_DatePsgPhasePhenoPlante_DESSICATION_A_RECOLTEE photo_DatePsgPhasePhenoPlante_FLORAISON_A_MATURATION photo_DatePsgPhasePhenoPlante_GERMINATION_A_JUVENILE photo_DatePsgPhasePhenoPlante_JUVENILE_A_CROISSANCEACTIVE photo_DatePsgPhasePhenoPlante_MATURATION_A_DESSICATION photo_DatePsgPhasePhenoPlante_MATURATION_A_RECOLTEE photo_DatePsgPhasePhenoPlante_NONSEMEE_A_GERMINATION Teff Tmoy ISH1 ISH2 ISH3 ActionFerti ActionIrrig ActionRecolte ActionSemis DoseFerti DoseIrrig EtatConduite densite zSemis CCN CGR CN1 CN2 CN3 CRU CRUm DDM Ea Engrais F1 F2 F3 FE FHN FNIRUE FNLE FR INN INNI N1 N2 N3 Nabs Ncrit Ndenit Nmax Nmine Nsol TNp TNpcrit TNpmax vAA vAA1 vAA2 vDenit vMF vMF1 vMF2 vMine vNA vNAc ATSW C1 C2 C3 D1 D2 D3 ETMcumulDepuisLevee ETP ETPcumulDepuisLevee ETRETM ETRcumulDepuisLevee EVj FHLE FHRUE FHTR FTSW JS PluieCumulDepuisLevee RWCC1 STOCKC1 STOCKC2 STOCKC3 TEC1 TR TRPF TTSW fRacC1 vRac vTR vTRC1 vTRC2 vTRp zC2 zC3 zRac FLe PARi FT FTHN DSF P2 TH photo_TH_aFinMATURATION IRs JSE JSF JSM RDT photo_INN_CROISSANCEACTIVE_A_FLORAISON photo_IRs_aFinMATURATION photo_RDT_aFinMATURATION photo_TDM_CROISSANCEACTIVE_A_FLORAISON ETPP Pluie RG Tn Tx

resuAttendu (sauf jourDeLannee) est affecte par defaut a sa valeur d'invalidite  -777 
toleranceTest (sauf jourDeLannee) est affecte par defaut a sa valeur d'inactivation  -111  (=COMPARAISON_INACTIVE). 

******************************************
* Description du jeu de test  jeuDeroulementOk , et verifications a effectuer 
******************************************

Jeu de verification du bon deroulement de la simulation. 

********* Les donnees/informations de test :

Les conditions de la simulation sont nominales : elles ne provoquent aucune erreur dans le deroulement de la simulation et elles conduisent la plante a evoluer entre le semis et la recolte de la maniere classique prevue par le modele. 

Entrees de test injectees, parametres modifies... : voir le rapport TST_rapportScenario_ scnSunfloV1intact .txt 

Definition des resultats attendus : Les resultats attendus ont trait au comportement de la simulation et du modele en cas de bon deroulement, ils reposent essentiellement sur le contenu des rapports de simulation. 

********* Les VERIFICATIONS (automatiques et/ou manuelles) :

Automatique : Pas de verification automatique. 

Verifications manuelles :  


--- Information descriptive pour memo : --------------------------------------
 Les traces d'erreur et d'avertissement : Lorsqu'une erreur est detectee en cours de simulation (erreur de parametrage, probleme de division par 0...), elle est signalee dans le rapport TST_rapportScenario_scnSunfloV1intact.txt par un message d'erreur contenant le mot cle 'ERREUR' et la simulation continue de se derouler malgre la situation erronee. Le mot cle 'Avertissement' quant a lui sert a signaler un fait important qui n'est pas considere comme une erreur. 
------------------------------------------------------------------------------ 
--- Information descriptive pour memo : --------------------------------------
 Cas de deroulement de simulation jusqu'a sa fin ou interruption/arret de simulation avant sa fin : En cas de survenue en cours de simulation d'une erreur bloquante non geree par le logiciel SUNFLO, la simulation stoppe avant la fin, ce qui est signale en fin du rapport TST_rapportScenario_scnSunfloV1intact.txt par un message d'erreur commencant par 'ERREUR vle::manager::RunQuiet :'. De plus il apparait dans ses 10 dernieres lignes les messages 'Failed' et '0% tests passed, 1 tests failed out of 1'. Par contre si la simulation s'est deroulee jusqu'a la fin sans etre interrompue pour cause d'erreur, alors le message contenant 'ERREUR vle::manager::RunQuiet :' n'apparait pas dans le rapport TST_rapportScenario_scnSunfloV1intact.txt et dans ses 10 dernieres lignes il apparait les messages 'No errors detected' et '100% tests passed, 0 tests failed out of 1'. 
------------------------------------------------------------------------------ 

Manuelle :  Verification de la fin/sortie normale de la simulation : verifier dans le rapport TST_rapportScenario_scnSunfloV1intact.txt que dans ses 10 dernieres lignes il apparait les messages 'No errors detected' et '100% tests passed, 0 tests failed out of 1'. Noter ici le resultat de la verification.
 
Manuelle :  Verification de l'absence de detection d'erreur en cours de simulation : verifier dans le rapport TST_rapportScenario_scnSunfloV1intact.txt l'absence du mot cle 'ERREUR' qui sert a signaler une erreur detectee. Noter ici le resultat de la verification.
 

--- Information descriptive pour memo : --------------------------------------
 Evolution de la phase phenologique de la plante : elle est notee dans le rapport RAPPORT_Phenologie.TXT, elle est egalement donnee en cours de simulation dans le rapport TST_rapportScenario_scnSunfloV1intact.txt par des messages du type 'Phase phenologique de la plante vs.PhasePhenoPlante() : ...' qui s'interpretent a l'aide de la legende (voir mot cle 'LEGENDE') 'Signification des valeurs que PhasePhenoPlante peut prendre : ...' egalement presente dans le rapport. 
Evolution 'classique' de la phase phenologique de la plante : la phase au depart est NONSEMEE (avant le semis) et a la fin RECOLTEE (apres la recolte). Entre les deux, elle evolue de NONSEMEE a GERMINATION puis de GERMINATION a JUVENILE puis de JUVENILE a CROISSANCEACTIVE puis de CROISSANCEACTIVE a FLORAISON puis de FLORAISON a MATURATION puis de MATURATION a DESSICATION puis de DESSICATION a RECOLTEE. Toutefois si la recolte a lieu alors que la plante est dans la phase MATURATION, alors la phase phenologique evolue directement de MATURATION a RECOLTEE (sans passer par DESSICATION). Ceci n'est pas considere comme une erreur, un message d'avertissement a ce sujet est donne dans le rapport TST_rapportScenario_scnSunfloV1intact.txt du type 'la recolte a lieu alors que la plante est en phase phenologique MATURATION' et contenant le mot cle 'Avertissement' (message egalement present dans le rapport RAPPORT_Phenologie.TXT). 
Toute autre evolution/comportement de la phase phenologique correspond a une situation 'anormale' qui peut correspondre a un probleme en terme agronomique (par exemple incoherences au niveau du parametrage, des donnees d'entree) ou bien a une anomalie/erreur de simulation (par exemple cas d'erreur ou la simulation continue de se derouler malgre la situation erronee signalee). Par exemple, le fait que la recolte (parametre jrecolte) ait lieu lors d'une phase anterieure a MATURATION est considere comme une erreur : la simulation continue de se derouler malgre ce 'probleme agronomique' et un message d'erreur a ce sujet est donne dans le rapport TST_rapportScenario_scnSunfloV1intact.txt du type 'la recolte a lieu alors que la plante est en phase phenologique ...' et contenant le mot cle 'ERREUR' (message egalement present dans le rapport RAPPORT_Phenologie.TXT). 
------------------------------------------------------------------------------ 
--- Information descriptive pour memo : --------------------------------------
 Atteinte de levee : La maniere dont le programme determine la levee (autrement dit le passage de la phase phenologique GERMINATION a JUVENILE ; cf variable PhasePhenoPlante) depend du parametre 'dateLevee_casForcee' (qui se trouve dans la condition 'CONFIG_SimuInit' du fichier vpz). Si 'dateLevee_casForcee' vaut '00/00' alors le programme calcule une estimation de la duree thermique de la phase semis-levee (estimationTTentreSemisEtLevee_casPhaseSemisLeveeSimulee) et decide que la levee est atteinte lorsque le temps thermique calcule depuis le semis (cf variable TT_A0) atteint estimationTTentreSemisEtLevee_casPhaseSemisLeveeSimulee. Si au contraire 'dateLevee_casForcee' est specifiee (vaut 'JJ/MM' autre que '00/00') alors le programme decide que la levee est atteinte lorsque la date de simulation atteint 'dateLevee_casForcee'. La maniere dont le programme a determine la levee (estimation ou forcage) est notee dans le rapport RAPPORT_Phenologie.TXT (messages du type 'la levee n'a pas ete forcee (elle a ete estimee), estimationTTentreSemisEtLevee_casPhaseSemisLeveeSimulee : ...' ou ' la levee a ete forcee (elle n'a pas ete estimee), elle a ete determinee a partir du parametre dateLevee_casForcee : JJ/MM'). 
------------------------------------------------------------------------------ 

Manuelle :  Verification de l'evolution classique de la phase phenologique de la plante, sachant qu'il s'agit d'une simulation avec presence de la phase DESSICATION et avec estimation de la phase semis-levee (date de levee non forcee) : verifier dans le rapport RAPPORT_Phenologie.TXT que la phase phenologique de la plante evolue bien de NONSEMEE a GERMINATION puis de GERMINATION a JUVENILE puis de JUVENILE a CROISSANCEACTIVE puis de CROISSANCEACTIVE a FLORAISON puis de FLORAISON a MATURATION puis de MATURATION a DESSICATION puis de DESSICATION a RECOLTEE. Y verifier de plus la presence du message signalant que la levee a ete estimee (message du type 'la levee n'a pas ete forcee (elle a ete estimee)').  Noter ici le resultat de la verification. 
 

Manuelle :  Verification du contenu du rapport RAPPORT_CroissancePlante.TXT : verifier qu'il contient bien les informations 'LAI, indice foliaire, lors du  passage de la phase phenologique plante JUVENILE a CROISSANCEACTIVE (stade E1, bouton etoile)', 'LAI, indice foliaire, lors du  passage de la phase phenologique plante CROISSANCEACTIVE a FLORAISON (stade F1, debut floraison)', 'TDM, biomasse totale (g/m2) lors du  passage de la phase phenologique plante DESSICATION a RECOLTEE (qui correspond a la recolte)'.  Noter ici le resultat de la verification. 
 
Manuelle :  Verification du contenu du rapport RAPPORT_ContrainteEau.TXT : verifier qu'il contient bien les informations de cumuls calcules depuis la levee 'ETR cumule (mm)', 'ETM cumule (mm)', 'rapport ETR cumule / ETM cumule', 'Pluie cumulee (mm)', 'ETP cumule (mm)'.  Noter ici le resultat de la verification. 
 
Manuelle :  Verification du contenu du rapport RAPPORT_ContrainteAzote.TXT : verifier qu'il contient bien les informations 'Nabs (kg N/ha), lors du  passage de la phase phenologique plante CROISSANCEACTIVE a FLORAISON (stade F1, debut floraison)', 'Nabs (kg N/ha), lors du  passage de la phase phenologique plante DESSICATION a RECOLTEE (qui correspond a la recolte)'.  Noter ici le resultat de la verification. 
 
Manuelle :  Verification du contenu du rapport RAPPORT_ElaborationQualite.TXT : verifier qu'il contient bien les informations 'TH, teneur en huile lors du  passage de la phase phenologique plante MATURATION a DESSICATION (stade M3)'.  Noter ici le resultat de la verification. 
 
Manuelle :  Verification du contenu du rapport RAPPORT_ElaborationRendement.TXT : verifier qu'il contient bien les informations 'TDM, biomasse totale (g/m2) lors du  passage de la phase phenologique plante CROISSANCEACTIVE a FLORAISON (stade F1, debut floraison)', 'INN, lors du  passage de la phase phenologique plante CROISSANCEACTIVE a FLORAISON (stade F1, debut floraison)', 'IRs, indice de recolte lors du  passage de la phase phenologique plante MATURATION a DESSICATION (stade M3)', 'RDT, rendement (q/ha) lors du  passage de la phase phenologique plante MATURATION a DESSICATION (stade M3)' et les jours de stress cumules (covariables statistiques) 'JSE correspondant aux Jours de stress en phase vegetative (E1 - F1)', 'JSF correspondant aux Jours de stress en floraison (F1 - M0)', 'JSM correspondant aux Jours de stress après la floraison (M0 - M3)'.  Noter ici le resultat de la verification. 
 
Manuelle :  Verification du contenu du rapport RAPPORT_Diagnostic.TXT : verifier qu'il contient bien les informations de jours de stress cumules relatifs a la floraison 'ISH1 correspondant aux Jours de stress jusqu'a la floraison', 'ISH2 correspondant aux Jours de stress autour de la floraison', 'ISH3 correspondant aux Jours de stress après la floraison'.  Noter ici le resultat de la verification. 
 

--- Information descriptive pour memo : --------------------------------------
 Legendes (interpretation/signification de valeurs numeriques prises par certaines donnees) : Le rapport TST_rapportScenario_scnSunfloV1intact.txt contient des explications sur la maniere dont doivent etre interpretees les valeurs numeriques que peuvent prendre certaines variables (PhasePhenoPlante, ActionSemis...). On retrouve ces explications dans le rapport en y recherchant le mot cle 'LEGENDE'. 
------------------------------------------------------------------------------ 
--- Information descriptive pour memo : --------------------------------------
 Les fichiers de representations graphiques contenus dans le repertoire representationsGraphiques de chaque jeu de test : Si une donnee nomDeLaDonnee fait l'objet de comparaisons numeriques automatiques dans le jeu de test, alors sa representation graphique se trouve dans le fichier nomDeLaDonnee.pdf (le fichier nomDeLaDonnee_bis.pdf a pour but d'en visualiser les eventuelles parties 'hors cadre' dans nomDeLaDonnee.pdf). Si une donnee nomDeLaDonnee ne fait pas l'objet de comparaisons numeriques automatiques dans le jeu de test, alors sa representation graphique se trouve dans le fichier nomDeLaDonnee_bis.pdf (son fichier nomDeLaDonnee.pdf n'a aucun sens). 
------------------------------------------------------------------------------ 

Manuelle :  Verification de la representation graphique de la phase phenologique de la plante : verifier a vue d'oeil que la representation graphique de PhasePhenoPlante (elle se trouve dans le fichier PhasePhenoPlante_bis.pdf puisque dans ce jeu de test PhasePhenoPlante ne fait pas l'objet de comparaisons numeriques automatiques) correspond bien a son evolution decrite dans le rapport RAPPORT_Phenologie.TXT ; pour interpreter les valeurs numeriques de PhasePhenoPlante s'aider de la legende donnee dans le rapport TST_rapportScenario_scnSunfloV1intact.txt. Noter ici le resultat de la verification.
 

Remarques supplementaires :  

Remarque : On peut observer dans le rapport de simulation TST_rapportScenario_scnSunfloV1intact.txt que le parametre dateLevee_casForcee=00/00.

******************************************

******************************************
* Verifications numeriques automatiques 
******************************************

Les resultats attendus et tolerances de test associees (resuAttendu, toleranceTest) sont archives dans les fichiers  ./../output/lesJeuxDeTest/jeuDeroulementOk/complet_TST_sortiesAttendues_jeuDeroulementOk.csv  et  ./../output/lesJeuxDeTest/jeuDeroulementOk/complet_TST_toleranceTest_jeuDeroulementOk.csv . Il s'agit des attendus complets, dont le contenu a ete defini par fichiers et/ou code R. 
ATTENTION, l'interpretation des resultats attendus (informations de resuAttendu) depend des tolerances de test (informations de toleranceTest), qui doivent avoir ete affectees (via fichier ou code R). 

**********************************************************************************
* debut rapport des comparaisons Verifications numeriques automatiques 


Bilan : 


* fin rapport des comparaisons Verifications numeriques automatiques 
**********************************************************************************


******************************************
* Representations graphiques 
******************************************

Les representations graphiques des valeurs attendues par rapport aux valeurs obtenues (un graphique par donnee) sont rangees sous le repertoire  ./../output/lesJeuxDeTest/jeuDeroulementOk/representationsGraphiques/ 
ATTENTION, toutes les valeurs lues dans resuAttendu sont tracees mais parmi elles, n'ont aucun sens celles qui n'ont pas ete affectees (valeur  -777  (=VALEUR_INVALIDE) (cf cas ou la tolerance de test n'a pas ete affectee (valeur  -111  (=COMPARAISON_INACTIVE)). 
