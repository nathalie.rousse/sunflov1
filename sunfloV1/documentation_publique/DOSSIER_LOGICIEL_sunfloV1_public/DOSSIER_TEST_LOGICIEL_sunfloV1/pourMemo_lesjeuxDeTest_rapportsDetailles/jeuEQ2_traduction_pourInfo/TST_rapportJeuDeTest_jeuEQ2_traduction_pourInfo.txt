
[TST_sunfloV1_VerifJeuDeTest_jeuEQ2_traduction_pourInfo.R]
Jeu appele/deroule sans MODE_DEBUG. Jeu appele/deroule pour generer un RAPPORT_DETAILLE. 

***********************************************************************************
* 
* Modele VLE sunfloV1, jeu de test jeuEQ2_traduction_pourInfo
* 
* Comparaison entre les resultats obtenus et les resultats attendus. 
* 
* Les verifications automatiques sont effectuees, tandis que celles qui sont a effectuer manuellement sont signalees. 
* 
* Le jeu de test jeuEQ2_traduction_pourInfo s'appuie sur le scenario de test scnSunfloV1intact.
* 
***********************************************************************************

Les resultats de simulation sont recuperes dans le repertoire  ./../output/lesScenarios/scnSunfloV1intact/ 
Le repertoire des resultats des verifications est  ./../output/lesJeuxDeTest/jeuEQ2_traduction_pourInfo/ 
Notamment le repertoire des representations graphiques est  ./../output/lesJeuxDeTest/jeuEQ2_traduction_pourInfo/representationsGraphiques/ 
Le fichier des donnees obtenues en simulation de test est  ./../output/lesScenarios/scnSunfloV1intact/TST_sortiesObtenues_scnSunfloV1intact.csv .
Pour info, le fichier des donnees attendues  ./../test/TST_sortiesAttendues_jeuEQ2_traduction_pourInfo.csv N'EXISTE PAS 
Pour memo, resuAttendu[j,nomVar] vaut  -777  si aucune valeur ne lui a ete affectee (ni par fichier, ni par code R) 
Pour info, Le fichier des tolerances de test  ./../test/TST_toleranceTest_jeuEQ2_traduction_pourInfo.csv  N'EXISTE PAS.
Pour memo, le tableau des tolerances de test toleranceTest est interprete/utilise de la maniere suivante dans la comparaison des donnees obtenues (resuObtenu) aux donnees attendues (resuAttendu) : si toleranceTest[j,nomVar]= -111 (ie COMPARAISON_INACTIVE) alors resuAttendu[j,nomVar] ne sera pas compare a resuObtenu[j,nomVar]; si toleranceTest[j,nomVar]>=0 alors il sera verifie que resuObtenu[j,nomVar]=resuAttendu[j,nomVar] a toleranceTest[j,nomVar] pres. 

Le fichier  ./../output/lesScenarios/scnSunfloV1intact/TST_sortiesObtenues_scnSunfloV1intact.csv  contient les variables time AAAAnumerique JJnumerique MMnumerique jourDeLannee DBP Eb Ebp Ei LAI SFp TDM AP ChgtPhasePhenoPlante PhasePhenoPlante TT_A0 TT_A2 TT_F1 photo_DatePsgPhasePhenoPlante_CROISSANCEACTIVE_A_FLORAISON photo_DatePsgPhasePhenoPlante_DESSICATION_A_RECOLTEE photo_DatePsgPhasePhenoPlante_FLORAISON_A_MATURATION photo_DatePsgPhasePhenoPlante_GERMINATION_A_JUVENILE photo_DatePsgPhasePhenoPlante_JUVENILE_A_CROISSANCEACTIVE photo_DatePsgPhasePhenoPlante_MATURATION_A_DESSICATION photo_DatePsgPhasePhenoPlante_MATURATION_A_RECOLTEE photo_DatePsgPhasePhenoPlante_NONSEMEE_A_GERMINATION Teff Tmoy ISH1 ISH2 ISH3 ActionFerti ActionIrrig ActionRecolte ActionSemis DoseFerti DoseIrrig EtatConduite densite zSemis CCN CGR CN1 CN2 CN3 CRU CRUm DDM Ea Engrais F1 F2 F3 FE FHN FNIRUE FNLE FR INN INNI N1 N2 N3 Nabs Ncrit Ndenit Nmax Nmine Nsol TNp TNpcrit TNpmax vAA vAA1 vAA2 vDenit vMF vMF1 vMF2 vMine vNA vNAc ATSW C1 C2 C3 D1 D2 D3 ETMcumulDepuisLevee ETP ETPcumulDepuisLevee ETRETM ETRcumulDepuisLevee EVj FHLE FHRUE FHTR FTSW JS PluieCumulDepuisLevee RWCC1 STOCKC1 STOCKC2 STOCKC3 TEC1 TR TRPF TTSW fRacC1 vRac vTR vTRC1 vTRC2 vTRp zC2 zC3 zRac FLe PARi FT FTHN DSF P2 TH photo_TH_aFinMATURATION IRs JSE JSF JSM RDT photo_INN_CROISSANCEACTIVE_A_FLORAISON photo_IRs_aFinMATURATION photo_RDT_aFinMATURATION photo_TDM_CROISSANCEACTIVE_A_FLORAISON ETPP Pluie RG Tn Tx

resuAttendu (sauf jourDeLannee) est affecte par defaut a sa valeur d'invalidite  -777 
toleranceTest (sauf jourDeLannee) est affecte par defaut a sa valeur d'inactivation  -111  (=COMPARAISON_INACTIVE). 

******************************************
* Description du jeu de test  jeuEQ2_traduction_pourInfo , et verifications a effectuer 
******************************************

Meme jeu de test que jeuEQ2_traduction sauf que la tolerance de test est nulle. L'objectif de ce test n'est pas d'etre verifie mais d'afficher l'ensemble des ecarts numeriques entre resultats obtenus et attendus. 


********* Les donnees/informations de test :

EQ2 selon la publi : 
[EQ2]    TTE = sum( (   Tm   -   Tb    ) * ( 1 + a ( 1 - W.TR    ) ) ) ; where      Tb = 4.8 °C and a = 0.1 
EQ2 traduite en noms du code vle : 
[EQ2] d/dt(TT_A2) = ( e.Tmoy - p.Tbase ) * ( 1 + a ( 1 - ve.FHTR ) )  ;  where p.Tbase = 4.8 °C and a = 0.1 

Pour memo, pseudo-code du traitement relatif a TT_A2 du cote du code du modele sunfloV1 : 
dTT_A2/dt = 0 for t >= jrecolte , 
 = 0 for TT_A0 < estimationTTentreSemisEtLevee_casPhaseSemisLeveeSimulee and dateLevee_casForcee = 0 , 
 = 0 for dateLevee_casForcee >0 and t < dateLevee_casForcee , 
 = Teff + AP by default 
avec :  
AP = 0.1 * Teff * (1-FHTR) 
Teff = 0 for Tmoy <Tbase ,  
 = Tmoy - Tbase by default 

Entrees de test injectees, parametres modifies... : voir le rapport TST_rapportScenario_ scnSunfloV1intact .txt 

Definition des resultats attendus : 
Le calcul des resultats attendus [EQ2] servant dans la verification est code sous R. 
Les resultats attendus calcules correspondent a l'equation EQ2 de la publi adaptee selon les indices avec lesquels elle a ete ecrite dans le code vle. 
Ce calcul ne commence qu'a partir de la levee. 
Les indices (j, j-1, j-2) y respectent la regle TRADUCTION_DE_MODELMAKER_EN_VLE de traduction du code modelMaker en code vle. 

********* Les VERIFICATIONS (automatiques et/ou manuelles) :

Aucune verification. Ce test a pour unique objectif d'afficher l'ensemble des ecarts numeriques entre resultats obtenus et attendus. 

******************************************

******************************************
* Verifications numeriques automatiques 
******************************************

Les resultats attendus et tolerances de test associees (resuAttendu, toleranceTest) sont archives dans les fichiers  ./../output/lesJeuxDeTest/jeuEQ2_traduction_pourInfo/complet_TST_sortiesAttendues_jeuEQ2_traduction_pourInfo.csv  et  ./../output/lesJeuxDeTest/jeuEQ2_traduction_pourInfo/complet_TST_toleranceTest_jeuEQ2_traduction_pourInfo.csv . Il s'agit des attendus complets, dont le contenu a ete defini par fichiers et/ou code R. 
ATTENTION, l'interpretation des resultats attendus (informations de resuAttendu) depend des tolerances de test (informations de toleranceTest), qui doivent avoir ete affectees (via fichier ou code R). 

**********************************************************************************
* debut rapport des comparaisons Verifications numeriques automatiques 

 test NOTOK   pour TT_A2 (indice 41 , ie jourDeLannee 120 )
              valeurs obtenue :  37.416591837229  / attendue :  37.416591837229 
              ecart calcule :  3.5527136788005e-14  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 42 , ie jourDeLannee 121 )
              valeurs obtenue :  45.2853882108239  / attendue :  45.2853882108239 
              ecart calcule :  3.5527136788005e-14  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 43 , ie jourDeLannee 122 )
              valeurs obtenue :  59.1486051305419  / attendue :  59.1486051305419 
              ecart calcule :  3.5527136788005e-14  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 44 , ie jourDeLannee 123 )
              valeurs obtenue :  68.723488275925  / attendue :  68.723488275925 
              ecart calcule :  1.4210854715202e-14  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 46 , ie jourDeLannee 125 )
              valeurs obtenue :  91.9172765441104  / attendue :  91.9172765441104 
              ecart calcule :  2.8421709430404e-14  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 47 , ie jourDeLannee 126 )
              valeurs obtenue :  98.1960222832104  / attendue :  98.1960222832104 
              ecart calcule :  4.2632564145606e-14  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 48 , ie jourDeLannee 127 )
              valeurs obtenue :  108.65671649253  / attendue :  108.656716492530 
              ecart calcule :  3.12638803734444e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 49 , ie jourDeLannee 128 )
              valeurs obtenue :  119.770668237032  / attendue :  119.770668237032 
              ecart calcule :  1.13686837721616e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 51 , ie jourDeLannee 130 )
              valeurs obtenue :  138.892182032059  / attendue :  138.892182032059 
              ecart calcule :  2.8421709430404e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 52 , ie jourDeLannee 131 )
              valeurs obtenue :  149.102756600026  / attendue :  149.102756600026 
              ecart calcule :  4.2632564145606e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 53 , ie jourDeLannee 132 )
              valeurs obtenue :  161.814931097904  / attendue :  161.814931097904 
              ecart calcule :  5.6843418860808e-14  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 54 , ie jourDeLannee 133 )
              valeurs obtenue :  173.781180803754  / attendue :  173.781180803754 
              ecart calcule :  3.69482222595252e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 55 , ie jourDeLannee 134 )
              valeurs obtenue :  182.097730746517  / attendue :  182.097730746517 
              ecart calcule :  1.13686837721616e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 56 , ie jourDeLannee 135 )
              valeurs obtenue :  188.960109189684  / attendue :  188.960109189684 
              ecart calcule :  2.55795384873636e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 57 , ie jourDeLannee 136 )
              valeurs obtenue :  200.421265949787  / attendue :  200.421265949787 
              ecart calcule :  1.13686837721616e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 58 , ie jourDeLannee 137 )
              valeurs obtenue :  212.741164739991  / attendue :  212.741164739991 
              ecart calcule :  2.8421709430404e-14  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 59 , ie jourDeLannee 138 )
              valeurs obtenue :  228.503416680114  / attendue :  228.503416680114 
              ecart calcule :  3.41060513164848e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 60 , ie jourDeLannee 139 )
              valeurs obtenue :  244.429259772092  / attendue :  244.429259772092 
              ecart calcule :  8.5265128291212e-14  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 61 , ie jourDeLannee 140 )
              valeurs obtenue :  254.26039109401  / attendue :  254.260391094010 
              ecart calcule :  3.97903932025656e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 62 , ie jourDeLannee 141 )
              valeurs obtenue :  264.130534020167  / attendue :  264.130534020167 
              ecart calcule :  1.70530256582424e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 63 , ie jourDeLannee 142 )
              valeurs obtenue :  276.603094979276  / attendue :  276.603094979276 
              ecart calcule :  3.41060513164848e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 64 , ie jourDeLannee 143 )
              valeurs obtenue :  291.985225905188  / attendue :  291.985225905188 
              ecart calcule :  3.41060513164848e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 65 , ie jourDeLannee 144 )
              valeurs obtenue :  301.977910737704  / attendue :  301.977910737704 
              ecart calcule :  3.41060513164848e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 66 , ie jourDeLannee 145 )
              valeurs obtenue :  311.110050792239  / attendue :  311.110050792239 
              ecart calcule :  2.8421709430404e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 67 , ie jourDeLannee 146 )
              valeurs obtenue :  317.724273921522  / attendue :  317.724273921522 
              ecart calcule :  5.6843418860808e-14  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 68 , ie jourDeLannee 147 )
              valeurs obtenue :  330.477423151446  / attendue :  330.477423151446 
              ecart calcule :  1.70530256582424e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 69 , ie jourDeLannee 148 )
              valeurs obtenue :  344.833574513804  / attendue :  344.833574513804 
              ecart calcule :  3.41060513164848e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 70 , ie jourDeLannee 149 )
              valeurs obtenue :  362.243977393756  / attendue :  362.243977393756 
              ecart calcule :  3.41060513164848e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 71 , ie jourDeLannee 150 )
              valeurs obtenue :  380.959569180706  / attendue :  380.959569180706 
              ecart calcule :  1.13686837721616e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 72 , ie jourDeLannee 151 )
              valeurs obtenue :  399.530622222127  / attendue :  399.530622222127 
              ecart calcule :  5.11590769747272e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 73 , ie jourDeLannee 152 )
              valeurs obtenue :  418.957698426901  / attendue :  418.957698426901 
              ecart calcule :  5.6843418860808e-14  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 74 , ie jourDeLannee 153 )
              valeurs obtenue :  435.794090370338  / attendue :  435.794090370338 
              ecart calcule :  2.8421709430404e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 75 , ie jourDeLannee 154 )
              valeurs obtenue :  453.614382299121  / attendue :  453.614382299121 
              ecart calcule :  3.41060513164848e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 76 , ie jourDeLannee 155 )
              valeurs obtenue :  472.244271865963  / attendue :  472.244271865963 
              ecart calcule :  1.70530256582424e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 77 , ie jourDeLannee 156 )
              valeurs obtenue :  486.731989401766  / attendue :  486.731989401766 
              ecart calcule :  5.11590769747272e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 79 , ie jourDeLannee 158 )
              valeurs obtenue :  519.916698708749  / attendue :  519.916698708749 
              ecart calcule :  4.54747350886464e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 80 , ie jourDeLannee 159 )
              valeurs obtenue :  536.86619173422  / attendue :  536.86619173422 
              ecart calcule :  4.54747350886464e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 81 , ie jourDeLannee 160 )
              valeurs obtenue :  554.631595574116  / attendue :  554.631595574116 
              ecart calcule :  3.41060513164848e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 82 , ie jourDeLannee 161 )
              valeurs obtenue :  574.423433704829  / attendue :  574.423433704829 
              ecart calcule :  3.41060513164848e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 83 , ie jourDeLannee 162 )
              valeurs obtenue :  594.540087728565  / attendue :  594.540087728565 
              ecart calcule :  3.41060513164848e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 84 , ie jourDeLannee 163 )
              valeurs obtenue :  615.140849556349  / attendue :  615.140849556349 
              ecart calcule :  1.13686837721616e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 85 , ie jourDeLannee 164 )
              valeurs obtenue :  637.697492003092  / attendue :  637.697492003092 
              ecart calcule :  4.54747350886464e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 86 , ie jourDeLannee 165 )
              valeurs obtenue :  661.246897737849  / attendue :  661.24689773785 
              ecart calcule :  1.13686837721616e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 87 , ie jourDeLannee 166 )
              valeurs obtenue :  683.079499455754  / attendue :  683.079499455754 
              ecart calcule :  4.54747350886464e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 88 , ie jourDeLannee 167 )
              valeurs obtenue :  705.15816800753  / attendue :  705.15816800753 
              ecart calcule :  3.41060513164848e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 89 , ie jourDeLannee 168 )
              valeurs obtenue :  723.585171373829  / attendue :  723.585171373829 
              ecart calcule :  4.54747350886464e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 90 , ie jourDeLannee 169 )
              valeurs obtenue :  740.977631104844  / attendue :  740.977631104844 
              ecart calcule :  2.27373675443232e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 91 , ie jourDeLannee 170 )
              valeurs obtenue :  759.504067286904  / attendue :  759.504067286904 
              ecart calcule :  1.13686837721616e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 92 , ie jourDeLannee 171 )
              valeurs obtenue :  780.904821547365  / attendue :  780.904821547365 
              ecart calcule :  3.41060513164848e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 93 , ie jourDeLannee 172 )
              valeurs obtenue :  804.060517193216  / attendue :  804.060517193216 
              ecart calcule :  1.13686837721616e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 94 , ie jourDeLannee 173 )
              valeurs obtenue :  825.52205162211  / attendue :  825.52205162211 
              ecart calcule :  4.54747350886464e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 95 , ie jourDeLannee 174 )
              valeurs obtenue :  846.763022053589  / attendue :  846.763022053589 
              ecart calcule :  3.41060513164848e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 96 , ie jourDeLannee 175 )
              valeurs obtenue :  868.544851858416  / attendue :  868.544851858416 
              ecart calcule :  4.54747350886464e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 97 , ie jourDeLannee 176 )
              valeurs obtenue :  888.893885245187  / attendue :  888.893885245187 
              ecart calcule :  3.41060513164848e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 98 , ie jourDeLannee 177 )
              valeurs obtenue :  907.315547033538  / attendue :  907.315547033538 
              ecart calcule :  3.41060513164848e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 99 , ie jourDeLannee 178 )
              valeurs obtenue :  924.866247711685  / attendue :  924.866247711685 
              ecart calcule :  2.27373675443232e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 100 , ie jourDeLannee 179 )
              valeurs obtenue :  944.49147289256  / attendue :  944.491472892559 
              ecart calcule :  2.27373675443232e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 101 , ie jourDeLannee 180 )
              valeurs obtenue :  964.881620619585  / attendue :  964.881620619585 
              ecart calcule :  3.41060513164848e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 102 , ie jourDeLannee 181 )
              valeurs obtenue :  986.426615461078  / attendue :  986.426615461078 
              ecart calcule :  2.27373675443232e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 103 , ie jourDeLannee 182 )
              valeurs obtenue :  1004.7100416822  / attendue :  1004.71004168220 
              ecart calcule :  1.70530256582424e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 104 , ie jourDeLannee 183 )
              valeurs obtenue :  1021.23383237515  / attendue :  1021.23383237515 
              ecart calcule :  1.93267624126747e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 105 , ie jourDeLannee 184 )
              valeurs obtenue :  1036.84534239887  / attendue :  1036.84534239887 
              ecart calcule :  2.27373675443232e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 106 , ie jourDeLannee 185 )
              valeurs obtenue :  1052.2904176897  / attendue :  1052.29041768970 
              ecart calcule :  4.32009983342141e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 107 , ie jourDeLannee 186 )
              valeurs obtenue :  1067.71417175554  / attendue :  1067.71417175554 
              ecart calcule :  3.41060513164848e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 108 , ie jourDeLannee 187 )
              valeurs obtenue :  1084.87672040081  / attendue :  1084.87672040081 
              ecart calcule :  4.32009983342141e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 110 , ie jourDeLannee 189 )
              valeurs obtenue :  1126.81180497609  / attendue :  1126.81180497609 
              ecart calcule :  2.04636307898909e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 111 , ie jourDeLannee 190 )
              valeurs obtenue :  1148.04207233082  / attendue :  1148.04207233082 
              ecart calcule :  1.36424205265939e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 112 , ie jourDeLannee 191 )
              valeurs obtenue :  1170.26875456618  / attendue :  1170.26875456618 
              ecart calcule :  3.63797880709171e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 113 , ie jourDeLannee 192 )
              valeurs obtenue :  1191.29790750033  / attendue :  1191.29790750033 
              ecart calcule :  1.36424205265939e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 114 , ie jourDeLannee 193 )
              valeurs obtenue :  1213.36091541263  / attendue :  1213.36091541263 
              ecart calcule :  3.63797880709171e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 115 , ie jourDeLannee 194 )
              valeurs obtenue :  1236.38626987948  / attendue :  1236.38626987948 
              ecart calcule :  3.63797880709171e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 117 , ie jourDeLannee 196 )
              valeurs obtenue :  1281.5162674629  / attendue :  1281.51626746290 
              ecart calcule :  1.59161572810262e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 118 , ie jourDeLannee 197 )
              valeurs obtenue :  1300.07385679231  / attendue :  1300.07385679231 
              ecart calcule :  6.82121026329696e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 119 , ie jourDeLannee 198 )
              valeurs obtenue :  1318.51782877094  / attendue :  1318.51782877094 
              ecart calcule :  4.32009983342141e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 120 , ie jourDeLannee 199 )
              valeurs obtenue :  1338.71915981776  / attendue :  1338.71915981776 
              ecart calcule :  4.54747350886464e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 121 , ie jourDeLannee 200 )
              valeurs obtenue :  1359.61035422217  / attendue :  1359.61035422217 
              ecart calcule :  3.18323145620525e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 122 , ie jourDeLannee 201 )
              valeurs obtenue :  1380.72011124762  / attendue :  1380.72011124762 
              ecart calcule :  3.41060513164848e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 123 , ie jourDeLannee 202 )
              valeurs obtenue :  1401.53515261233  / attendue :  1401.53515261233 
              ecart calcule :  2.04636307898909e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 124 , ie jourDeLannee 203 )
              valeurs obtenue :  1421.88827086531  / attendue :  1421.88827086531 
              ecart calcule :  3.41060513164848e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 125 , ie jourDeLannee 204 )
              valeurs obtenue :  1443.75972218807  / attendue :  1443.75972218807 
              ecart calcule :  2.04636307898909e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 126 , ie jourDeLannee 205 )
              valeurs obtenue :  1463.65820097556  / attendue :  1463.65820097556 
              ecart calcule :  4.54747350886464e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 127 , ie jourDeLannee 206 )
              valeurs obtenue :  1484.26523221078  / attendue :  1484.26523221078 
              ecart calcule :  4.54747350886464e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 128 , ie jourDeLannee 207 )
              valeurs obtenue :  1506.84044387531  / attendue :  1506.84044387531 
              ecart calcule :  1.59161572810262e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 129 , ie jourDeLannee 208 )
              valeurs obtenue :  1527.06912411116  / attendue :  1527.06912411116 
              ecart calcule :  2.27373675443232e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 130 , ie jourDeLannee 209 )
              valeurs obtenue :  1544.16755083414  / attendue :  1544.16755083414 
              ecart calcule :  2.04636307898909e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 131 , ie jourDeLannee 210 )
              valeurs obtenue :  1562.04766690397  / attendue :  1562.04766690397 
              ecart calcule :  2.95585778076202e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 132 , ie jourDeLannee 211 )
              valeurs obtenue :  1581.17302171594  / attendue :  1581.17302171594 
              ecart calcule :  2.95585778076202e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 133 , ie jourDeLannee 212 )
              valeurs obtenue :  1600.17502041727  / attendue :  1600.17502041727 
              ecart calcule :  2.50111042987555e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 134 , ie jourDeLannee 213 )
              valeurs obtenue :  1618.88854583346  / attendue :  1618.88854583346 
              ecart calcule :  4.54747350886464e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 135 , ie jourDeLannee 214 )
              valeurs obtenue :  1640.7057875815  / attendue :  1640.70578758150 
              ecart calcule :  2.27373675443232e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 136 , ie jourDeLannee 215 )
              valeurs obtenue :  1664.65804302381  / attendue :  1664.65804302381 
              ecart calcule :  9.09494701772928e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 137 , ie jourDeLannee 216 )
              valeurs obtenue :  1691.22838936377  / attendue :  1691.22838936377 
              ecart calcule :  4.54747350886464e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 138 , ie jourDeLannee 217 )
              valeurs obtenue :  1716.63924278104  / attendue :  1716.63924278104 
              ecart calcule :  1.36424205265939e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 139 , ie jourDeLannee 218 )
              valeurs obtenue :  1742.63865783281  / attendue :  1742.63865783281 
              ecart calcule :  2.27373675443232e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 140 , ie jourDeLannee 219 )
              valeurs obtenue :  1770.32324408901  / attendue :  1770.32324408901 
              ecart calcule :  2.04636307898909e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 141 , ie jourDeLannee 220 )
              valeurs obtenue :  1795.08475329792  / attendue :  1795.08475329792 
              ecart calcule :  2.95585778076202e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 142 , ie jourDeLannee 221 )
              valeurs obtenue :  1820.22376377275  / attendue :  1820.22376377275 
              ecart calcule :  6.82121026329696e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 143 , ie jourDeLannee 222 )
              valeurs obtenue :  1845.80428315444  / attendue :  1845.80428315444 
              ecart calcule :  2.72848410531878e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 144 , ie jourDeLannee 223 )
              valeurs obtenue :  1871.92198295518  / attendue :  1871.92198295518 
              ecart calcule :  9.09494701772928e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 145 , ie jourDeLannee 224 )
              valeurs obtenue :  1899.78149179019  / attendue :  1899.78149179019 
              ecart calcule :  3.41060513164848e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 146 , ie jourDeLannee 225 )
              valeurs obtenue :  1927.78593719186  / attendue :  1927.78593719186 
              ecart calcule :  1.81898940354586e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 147 , ie jourDeLannee 226 )
              valeurs obtenue :  1952.20694826481  / attendue :  1952.20694826481 
              ecart calcule :  9.09494701772928e-13  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 148 , ie jourDeLannee 227 )
              valeurs obtenue :  1972.8825583958  / attendue :  1972.88255839580 
              ecart calcule :  4.77484718430787e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 149 , ie jourDeLannee 228 )
              valeurs obtenue :  1995.4072381392  / attendue :  1995.40723813920 
              ecart calcule :  3.63797880709171e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 150 , ie jourDeLannee 229 )
              valeurs obtenue :  2013.92729540836  / attendue :  2013.92729540836 
              ecart calcule :  4.09272615797818e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 151 , ie jourDeLannee 230 )
              valeurs obtenue :  2035.35543699448  / attendue :  2035.35543699448 
              ecart calcule :  3.63797880709171e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 152 , ie jourDeLannee 231 )
              valeurs obtenue :  2057.44869016295  / attendue :  2057.44869016295 
              ecart calcule :  5.00222085975111e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 153 , ie jourDeLannee 232 )
              valeurs obtenue :  2078.82646729985  / attendue :  2078.82646729985 
              ecart calcule :  2.27373675443232e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 154 , ie jourDeLannee 233 )
              valeurs obtenue :  2099.19808621552  / attendue :  2099.19808621552 
              ecart calcule :  4.54747350886464e-12  / tolerance de test :  0
 test NOTOK   pour TT_A2 (indice 155 , ie jourDeLannee 234 )
              valeurs obtenue :  2120.2460429805  / attendue :  2120.2460429805 
              ecart calcule :  9.09494701772928e-13  / tolerance de test :  0 
 test    ok   pour variable  TT_A2 
              aux indices (entre parentheses: jourDeLannee,ecart entre resultats obtenu et attendu) :    39 ( 118 , 0 )   40 ( 119 , 0 )   45 ( 124 , 0 )   50 ( 129 , 0 )   78 ( 157 , 0 )   109 ( 188 , 0 )   116 ( 195 , 0 )

Bilan : 
 Sur l'ensemble des comparaisons numeriques de la variable  TT_A2 , 
 l'ecart de test calcule maximal vaut  5.00222085975111e-12 , la tolerance de test maximale vaut  0 . 


* fin rapport des comparaisons Verifications numeriques automatiques 
**********************************************************************************


******************************************
* Representations graphiques 
******************************************

Les representations graphiques des valeurs attendues par rapport aux valeurs obtenues (un graphique par donnee) sont rangees sous le repertoire  ./../output/lesJeuxDeTest/jeuEQ2_traduction_pourInfo/representationsGraphiques/ 
ATTENTION, toutes les valeurs lues dans resuAttendu sont tracees mais parmi elles, n'ont aucun sens celles qui n'ont pas ete affectees (valeur  -777  (=VALEUR_INVALIDE) (cf cas ou la tolerance de test n'a pas ete affectee (valeur  -111  (=COMPARAISON_INACTIVE)). 
