
[TST_sunfloV1_VerifJeuDeTest_jeuErreurFichierInexistant.R]
Jeu appele/deroule sans MODE_DEBUG. Jeu appele/deroule pour generer un RAPPORT_DETAILLE. 

***********************************************************************************
* 
* Modele VLE sunfloV1, jeu de test jeuErreurFichierInexistant
* 
* Comparaison entre les resultats obtenus et les resultats attendus. 
* 
* Les verifications automatiques sont effectuees, tandis que celles qui sont a effectuer manuellement sont signalees. 
* 
* Le jeu de test jeuErreurFichierInexistant s'appuie sur le scenario de test scnFichierInexistant.
* 
***********************************************************************************

Les resultats de simulation sont recuperes dans le repertoire  ./../output/lesScenarios/scnFichierInexistant/ 
Le repertoire des resultats des verifications est  ./../output/lesJeuxDeTest/jeuErreurFichierInexistant/ 
Notamment le repertoire des representations graphiques est  ./../output/lesJeuxDeTest/jeuErreurFichierInexistant/representationsGraphiques/ 

ERREUR : Le fichier des donnees obtenues en simulation de test  ./../output/lesScenarios/scnFichierInexistant/TST_sortiesObtenues_scnFichierInexistant.csv N'EXISTE PAS 

Pour info, le fichier des donnees attendues  ./../test/TST_sortiesAttendues_jeuErreurFichierInexistant.csv N'EXISTE PAS 
Pour memo, resuAttendu[j,nomVar] vaut  -777  si aucune valeur ne lui a ete affectee (ni par fichier, ni par code R) 
Pour info, Le fichier des tolerances de test  ./../test/TST_toleranceTest_jeuErreurFichierInexistant.csv  N'EXISTE PAS.
Pour memo, le tableau des tolerances de test toleranceTest est interprete/utilise de la maniere suivante dans la comparaison des donnees obtenues (resuObtenu) aux donnees attendues (resuAttendu) : si toleranceTest[j,nomVar]= -111 (ie COMPARAISON_INACTIVE) alors resuAttendu[j,nomVar] ne sera pas compare a resuObtenu[j,nomVar]; si toleranceTest[j,nomVar]>=0 alors il sera verifie que resuObtenu[j,nomVar]=resuAttendu[j,nomVar] a toleranceTest[j,nomVar] pres. 

resuAttendu (sauf jourDeLannee) est affecte par defaut a sa valeur d'invalidite  -777 
toleranceTest (sauf jourDeLannee) est affecte par defaut a sa valeur d'inactivation  -111  (=COMPARAISON_INACTIVE). 

******************************************
* Description du jeu de test  jeuErreurFichierInexistant , et verifications a effectuer 
******************************************

Jeu de verification du comportement de la simulation en cas de survenue d'une erreur bloquante non geree par le logiciel SUNFLO : deroulement anormal de la simulation cause par l'absence du fichier meteo. Autre maniere d'exprimer la cause d'erreur : nom du fichier meteo mal defini dans le parametre prevu pour du fichier vpz. 

********* Les donnees/informations de test :

Les conditions de la simulation provoquent une erreur de simulation pour cause d'absence du fichier meteo au bon endroit. Autre maniere d'exprimer la cause d'erreur : nom du fichier meteo mal defini dans le parametre 'datas_file' du fichier vpz (dans la condition 'CONFIG_NomFichierClimat'). 

Entrees de test injectees, parametres modifies... : voir le rapport TST_rapportScenario_ scnFichierInexistant .txt 

Definition des resultats attendus : Les resultats attendus ont trait au comportement de la simulation en cas de deroulement anormal, ils reposent essentiellement sur le contenu des rapports de simulation. 

********* Les VERIFICATIONS (automatiques et/ou manuelles) :

Automatique : Pas de verification automatique. 

Verifications manuelles :  


--- Information descriptive pour memo : --------------------------------------
 Le fichier meteo de la simulation : La simulation du modele SUNFLO utilise/lit un fichier meteo dont le nom est defini dans le fichier vpz (voir parametre 'datas_file' dans la condition 'CONFIG_NomFichierClimat'). Le logiciel SUNFLO ne gere pas d'anomalies relatives au fichier meteo, celles-ci peuvent provoquer un arret de la simulation. Differentes erreurs peuvent se produire, notamment si les conditions suivantes ne sont pas respectees : 
(a) Le fichier doit exister dans le repertoire 'data' du paquet de la simulation (le nom du fichier donne dans 'datas_file' comprend son eventuelle extension .txt ou autre). 
(b) Le fichier doit contenir au moins les colonnes : 'Annee', Mois', 'Jour', 'ETP', 'Pluie', 'RAD', 'Tmax', 'Tmin' (les noms des colonnes doivent etre respectes a la lettre, majuscules comprises). 
(c) Le fichier doit contenir exactement (nombreDeColonnes x nombreDeLignes) valeurs. 
(d) Les valeurs d'une meme ligne sont separees les unes des autres par une tabulation, et les lignes par un retour chariot. 
(e) La virgule des nombres a virgule est representee par un point '.'. 
(f) Le fichier doit couvrir toute la plage de la simulation definie par sa date de debut 'Begin' et sa duree 'Duration' (dans le vpz sous le menu 'Project'/'Experiment'). 
Attention, le fichier meteo peut contenir une anomalie qui ne provoque pas d'arret de la simulation, auquel cas les resultats de la simulation sont errones sans que l'utilisateur n'en soit averti.
Exemples de messages d'erreurs relatifs au fichier meteo : 
- Fichier introuvable : 'Error while loading project. Dynamic library loading problem: cannot get dynamics 'dynLectureFichier', model 'GenericWithHeader' in module '.../pkgs/lecture_fichier/lib/liblecture_fichier.so': atomic model 'sunfloV1,sunfloV1_climat,LectureFichierClimat:Climat' throw error: [Climat] Meteo: column ETP missing' 
- Mauvais entete : 'Error while loading project. Dynamic library loading problem: cannot get dynamics 'dynLectureFichier', model 'GenericWithHeader' in module '.../pkgs/lecture_fichier/lib/liblecture_fichier.so': atomic model 'sunfloV1,sunfloV1_climat,LectureFichierClimat:Climat' throw error: [Climat] Meteo: column Tmin missing' si colonne Tmin manquante, ou encore si un autre nom de colonne a la place de Tmin comme par exemple tmin. 
- Mauvais nombre de valeurs : 
 'Simulator error. vector::_M_range_check' dans un cas ou il manque une valeur numerique dans le fichier meteo. 
 'Simulator error.  [Climat] ClimateReader - bad format or bad index for day column' dans un cas ou il manque une valeur dans la colonne Mois, ou encore dans la colonne Jour. 
- Mauvais format de valeur numerique : 
 'Simulator error. vector::_M_range_check' dans un cas de fichier contenant des ',' et non pas '.'. 
 'Simulator error. bad lexical cast: source type value could not be interpreted as target' dans un cas de fichier contenant une valeur non numerique. 
 'Simulator error. vector::_M_range_check' dans un cas de fichier contenant une valeur non numerique. 
- Non couverture de la plage de simulation : 
 'Simulator error. [Climat] Meteo: invalid date 2003-Mar-21 00:00:00 or invalid data' dans un cas ou l'annee 2003 de la simulation (qui commence le 21/03/2003) ne correspond pas a celle 2004 du fichier meteo, ou encore dans un cas de fichier meteo 'commencant' apres la date de debut de simulation 21/03/2003. 
 'Simulator error. [Climat] Meteo: invalid date 2003-Apr-19 00:00:00 or invalid data' dans un cas de fichier meteo s'arretant avant la fin de la plage de simulation (au 18/04/2003), ou encore dans un cas de fichier meteo auquel il manque une ligne/date a l'interieur de la plage de simulation (18/04/2003). 
------------------------------------------------------------------------------ 
--- Information descriptive pour memo : --------------------------------------
 Cas de deroulement de simulation jusqu'a sa fin ou interruption/arret de simulation avant sa fin : En cas de survenue en cours de simulation d'une erreur bloquante non geree par le logiciel SUNFLO, la simulation stoppe avant la fin, ce qui est signale en fin du rapport TST_rapportScenario_scnFichierInexistant.txt par un message d'erreur commencant par 'ERREUR vle::manager::RunQuiet :'. De plus il apparait dans ses 10 dernieres lignes les messages 'Failed' et '0% tests passed, 1 tests failed out of 1'. Par contre si la simulation s'est deroulee jusqu'a la fin sans etre interrompue pour cause d'erreur, alors le message contenant 'ERREUR vle::manager::RunQuiet :' n'apparait pas dans le rapport TST_rapportScenario_scnFichierInexistant.txt et dans ses 10 dernieres lignes il apparait les messages 'No errors detected' et '100% tests passed, 0 tests failed out of 1'. 
------------------------------------------------------------------------------ 

Manuelle :  Verification de la fin/sortie anormale de la simulation : verifier dans le rapport TST_rapportScenario_scnFichierInexistant.txt que dans ses 10 dernieres lignes il apparait les messages 'Failed' et '0% tests passed, 1 tests failed out of 1'. Noter ici le resultat de la verification.
 
Manuelle :  Verification du comportement de la simulation dans le cas ou le fichier meteo manque (erreur non geree par le logiciel SUNFLO) : verifier dans le rapport TST_rapportScenario_scnFichierInexistant.txt la presence du message d'erreur commencant par 'ERREUR vle::manager::RunQuiet :' qui signale que la simulation a stoppe avant la fin pour cause d'erreur, et refletant le fait que le fichier meteo n'a pas ete trouve dans le repertoire 'data' du paquet, ce qui se manifeste par une explication mentionnant 'dynamics 'dynLectureFichier'', 'model 'GenericWithHeader'', 'atomic model 'sunfloV1,sunfloV1_climat,LectureFichierClimat:Climat'' (le simulateur ne trouve pas une certaine colonne ('column missing') du fait que le fichier meteo cense la contenir n'est pas la). Noter ici le resultat de la verification.
 

******************************************
