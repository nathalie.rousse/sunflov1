

*****************************************************************************
*
*
*   RAPPORT PRODUIT PAR Phenologie
*
*
*****************************************************************************
-----------------------------------------------------------------------------
Date du  passage de la phase phenologique plante NONSEMEE a GERMINATION (qui correspond au semis)  : 2003-04-16
Cette date correspond, en jour de l'annee, a : 106
-----------------------------------------------------------------------------
Date du  passage de la phase phenologique plante GERMINATION a JUVENILE (stade levee)  : 2003-04-28
Cette date correspond, en jour de l'annee, a : 118
-----------------------------------------------------------------------------
Pour memo, la levee n'a pas ete forcee (elle a ete estimee).  vi.estimationTTentreSemisEtLevee_casPhaseSemisLeveeSimulee : 121.9
-----------------------------------------------------------------------------
Date du  passage de la phase phenologique plante JUVENILE a CROISSANCEACTIVE (stade E1, bouton etoile)  : 2003-06-09
Cette date correspond, en jour de l'annee, a : 160
-----------------------------------------------------------------------------
Date du  passage de la phase phenologique plante CROISSANCEACTIVE a FLORAISON (stade F1, debut floraison)  : 2003-06-27
Cette date correspond, en jour de l'annee, a : 178
-----------------------------------------------------------------------------
Date du  passage de la phase phenologique plante FLORAISON a MATURATION (stade M0)  : 2003-07-10
Cette date correspond, en jour de l'annee, a : 191
-----------------------------------------------------------------------------
Date du  passage de la phase phenologique plante MATURATION a DESSICATION (stade M3)  : 2003-08-20
Cette date correspond, en jour de l'annee, a : 232
-----------------------------------------------------------------------------
Date du  passage de la phase phenologique plante DESSICATION a RECOLTEE (qui correspond a la recolte)  : 2003-08-23
Cette date correspond, en jour de l'annee, a : 235