
[TST_sunfloV1_VerifJeuDeTest_jeuErreurFichierInexistant.R]
Jeu appele/deroule sans MODE_DEBUG. Jeu appele/deroule pour generer un RAPPORT_ALLEGE. 

***********************************************************************************
* 
* Modele VLE sunfloV1, jeu de test jeuErreurFichierInexistant
* 
* Comparaison entre les resultats obtenus et les resultats attendus. 
* 
* Les verifications automatiques sont effectuees, tandis que celles qui sont a effectuer manuellement sont signalees. 
* 
* Le jeu de test jeuErreurFichierInexistant s'appuie sur le scenario de test scnFichierInexistant.
* 
***********************************************************************************


******************************************
* Description du jeu de test  jeuErreurFichierInexistant , et verifications a effectuer 
******************************************

Jeu de verification du comportement de la simulation en cas de survenue d'une erreur bloquante non geree par le logiciel SUNFLO : deroulement anormal de la simulation cause par l'absence du fichier meteo. Autre maniere d'exprimer la cause d'erreur : nom du fichier meteo mal defini dans le parametre prevu pour du fichier vpz. 

********* Les donnees/informations de test :

Les conditions de la simulation provoquent une erreur de simulation pour cause d'absence du fichier meteo au bon endroit. Autre maniere d'exprimer la cause d'erreur : nom du fichier meteo mal defini dans le parametre 'datas_file' du fichier vpz (dans la condition 'CONFIG_NomFichierClimat'). 

Entrees de test injectees, parametres modifies... : voir le rapport TST_rapportScenario_ scnFichierInexistant .txt 

Definition des resultats attendus : Les resultats attendus ont trait au comportement de la simulation en cas de deroulement anormal, ils reposent essentiellement sur le contenu des rapports de simulation. 

********* Les VERIFICATIONS (automatiques et/ou manuelles) :

Automatique : Pas de verification automatique. 

Verifications manuelles :  


Manuelle :  Verification de la fin/sortie anormale de la simulation : verifier dans le rapport TST_rapportScenario_scnFichierInexistant.txt que dans ses 10 dernieres lignes il apparait les messages 'Failed' et '0% tests passed, 1 tests failed out of 1'. Noter ici le resultat de la verification : verification OK.

Manuelle :  Verification du comportement de la simulation dans le cas ou le fichier meteo manque (erreur non geree par le logiciel SUNFLO) : verifier dans le rapport TST_rapportScenario_scnFichierInexistant.txt la presence du message d'erreur commencant par 'ERREUR vle::manager::RunQuiet :' qui signale que la simulation a stoppe avant la fin pour cause d'erreur, et refletant le fait que le fichier meteo n'a pas ete trouve dans le repertoire 'data' du paquet, ce qui se manifeste par une explication mentionnant 'dynamics 'dynLectureFichier'', 'model 'GenericWithHeader'', 'atomic model 'sunfloV1,sunfloV1_climat,LectureFichierClimat:Climat'' (le simulateur ne trouve pas une certaine colonne ('column missing') du fait que le fichier meteo cense la contenir n'est pas la). Noter ici le resultat de la verification.
Verification NOTOK : dans le rapport TST_rapportScenario_scnFichierInexistant.txt pas de message d'erreur commencant par 'ERREUR vle::manager::RunQuiet :' ni d'explication mentionnant 'dynamics 'dynLectureFichier'', 'model 'GenericWithHeader'', 'atomic model 'sunfloV1,sunfloV1_climat,LectureFichierClimat:Climat'', 'column missing'. En fait l'anomalie d'absence du fichier meteo a provoque une sortie brutale de la simulation avec 'Segmentation fault' sans remontee de message d'erreur.
Rappel pour memo du resultat obtenu lors des essais precedents (il s'agissait du test de sunfloV1-0.0.1 sous vle 0.8.9) : verification OK (le message releve est : 'ERREUR vle::manager::RunQuiet : /!\ vle error reported: Dynamic library loading problem: cannot get dynamics 'dynLectureFichier', model 'GenericWithHeader' in module '/home/nrousse/.vle/pkgs/lecture_fichier/lib/liblecture_fichier.so': atomic model 'sunfloV1,sunfloV1_climat,LectureFichierClimat:Climat' throw error: [Climat] Meteo: column ETP missing').

******************************************
