
[TST_sunfloV1_VerifJeuDeTest_jeuEQ20.R]
Jeu appele/deroule sans MODE_DEBUG. Jeu appele/deroule pour generer un RAPPORT_ALLEGE. 

***********************************************************************************
* 
* Modele VLE sunfloV1, jeu de test jeuEQ20
* 
* Comparaison entre les resultats obtenus et les resultats attendus. 
* 
* Les verifications automatiques sont effectuees, tandis que celles qui sont a effectuer manuellement sont signalees. 
* 
* Le jeu de test jeuEQ20 s'appuie sur le scenario de test scnSunfloV1intact.
* 
***********************************************************************************


******************************************
* Description du jeu de test  jeuEQ20 , et verifications a effectuer 
******************************************

Jeu de verification de l'equation EQ20 de la publi, ou les resultats attendus correspondent theoriquement a l'equation de la publi. 

********* Les donnees/informations de test :

EQ20 selon la publi : 
[EQ20] W.NM = 1 - ( 1 - Fpf ) * ( 1 - RWCC1 ) where Fpf = 0.2 

EQ20 traduite en noms du code vle : 
[EQ20] vi.FHN = 1 - (1 - p.Fpf ) * (1 - ve.RWCC1) where p.Fpf = 0.2 

Pour memo, pseudo-code du traitement relatif a FHN du cote du code du modele sunfloV1 : 
vi.FHN = 0 for 1 - ( 1 - p.Fpf ) * ( 1 - ve.RWCC1(j) ) < 0 
vi.FHN = 1 - ( 1 - p.Fpf ) * ( 1 - ve.RWCC1(j) ) by default 


Entrees de test injectees, parametres modifies... : voir le rapport TST_rapportScenario_ scnSunfloV1intact .txt 

Definition des resultats attendus : 
Le calcul des resultats attendus [EQ20] servant dans la verification est code sous R. 
Les resultats attendus calcules correspondent theoriquement a l'equation EQ20 de la publi. 
Les indices y respectent FHN(j) = fonction_de(j). 

********* Les VERIFICATIONS (automatiques et/ou manuelles) :

Automatique :  Verification numerique de la donnee vi.FHN (du modele contrainte_azote). Cette verification n'est faite que si la valeur FHN calculee selon EQ20 >= 0.0 (voir pseudo-code du modele sunfloV1 plus haut). La tolerance d'ecart dans les comparaisons entre 'resu attendu' et 'resu obtenu' est 'resu attendu' x  4e-06 . 
Manuelle : Pas de verification manuelle supplementaire. 
Remarque :  On peut voir dans le rapport de simulation TST_rapportScenario_ scnSunfloV1intact .txt que le parametre Fpf= 0.2 . Observation OK.

******************************************

******************************************
* Verifications numeriques automatiques 
******************************************

Les resultats attendus et tolerances de test associees sont archives dans les fichiers  ./../output/lesJeuxDeTest/jeuEQ20/complet_TST_sortiesAttendues_jeuEQ20.csv  et  ./../output/lesJeuxDeTest/jeuEQ20/complet_TST_toleranceTest_jeuEQ20.csv . Il s'agit des attendus complets, dont le contenu a ete defini par fichiers et/ou code R. 
ATTENTION, l'interpretation des resultats attendus depend des tolerances de test , qui doivent avoir ete affectees (via fichier ou code R). 

**********************************************************************************
* debut rapport des comparaisons Verifications numeriques automatiques 
 
 test    ok   pour variable  FHN

Bilan : 
 Sur l'ensemble des comparaisons numeriques de la variable  FHN , 
 l'ecart de test calcule maximal vaut  7.99360577730113e-15 , la tolerance de test maximale vaut  5.9515074362616e-06 . 


* fin rapport des comparaisons Verifications numeriques automatiques 
**********************************************************************************


******************************************
* Representations graphiques 
******************************************

Les representations graphiques des valeurs attendues par rapport aux valeurs obtenues (un graphique par donnee) sont rangees sous le repertoire  ./../output/lesJeuxDeTest/jeuEQ20/representationsGraphiques/ 
ATTENTION, toutes les valeurs lues dans resuAttendu sont tracees mais parmi elles, n'ont aucun sens celles qui n'ont pas ete affectees (valeur  -777  (=VALEUR_INVALIDE) (cf cas ou la tolerance de test n'a pas ete affectee (valeur  -111  (=COMPARAISON_INACTIVE)). 
