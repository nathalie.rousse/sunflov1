
[TST_sunfloV1_VerifJeuDeTest_jeuErreurAbsenceParametre.R]
Jeu appele/deroule sans MODE_DEBUG. Jeu appele/deroule pour generer un RAPPORT_ALLEGE. 

***********************************************************************************
* 
* Modele VLE sunfloV1, jeu de test jeuErreurAbsenceParametre
* 
* Comparaison entre les resultats obtenus et les resultats attendus. 
* 
* Les verifications automatiques sont effectuees, tandis que celles qui sont a effectuer manuellement sont signalees. 
* 
* Le jeu de test jeuErreurAbsenceParametre s'appuie sur le scenario de test scnSansRh2.
* 
***********************************************************************************


******************************************
* Description du jeu de test  jeuErreurAbsenceParametre , et verifications a effectuer 
******************************************

Jeu de verification d'un deroulement anormal de la simulation avec detection d'une erreur causee par la mauvaise initialisation d'un parametre. 

********* Les donnees/informations de test :

Les conditions de la simulation provoquent une erreur de simulation pour cause de mauvaise initialisation d'un parametre. 

Entrees de test injectees, parametres modifies... : voir le rapport TST_rapportScenario_ scnSansRh2 .txt 

Definition des resultats attendus : Les resultats attendus ont trait au comportement de la simulation en cas de deroulement anormal avec detection d'une erreur, ils reposent essentiellement sur le contenu des rapports de simulation. 

********* Les VERIFICATIONS (automatiques et/ou manuelles) :

Automatique : Pas de verification automatique. 

Verifications manuelles :  


Manuelle :  Verification de la fin/sortie normale de la simulation : verifier dans le rapport TST_rapportScenario_scnSansRh2.txt que dans ses 10 dernieres lignes il apparait les messages 'No errors detected' et '100% tests passed, 0 tests failed out of 1'. Noter ici le resultat de la verification : verification OK.
 
Manuelle :  Verification de la detection en cours de simulation de l'erreur due a l'absence de configuration du parametre rh2 : verifier dans le rapport TST_rapportScenario_scnSansRh2.txt la presence du message d'erreur contenant le mot cle 'ERREUR', signalant l'absence du parametre rh2 (message du type 'absence d'evenement 'rh2', parametre rh2 non initialise'). Noter ici le resultat de la verification : verification OK (dans le sous-modele ContrainteAzote dans lequel il est utilise, rh2 a pour valeur rh2 : -7.98173e-42).

******************************************

******************************************
* Verifications numeriques automatiques 
******************************************

Les resultats attendus et tolerances de test associees sont archives dans les fichiers  ./../output/lesJeuxDeTest/jeuErreurAbsenceParametre/complet_TST_sortiesAttendues_jeuErreurAbsenceParametre.csv  et  ./../output/lesJeuxDeTest/jeuErreurAbsenceParametre/complet_TST_toleranceTest_jeuErreurAbsenceParametre.csv . Il s'agit des attendus complets, dont le contenu a ete defini par fichiers et/ou code R. 
ATTENTION, l'interpretation des resultats attendus depend des tolerances de test , qui doivent avoir ete affectees (via fichier ou code R). 

**********************************************************************************
* debut rapport des comparaisons Verifications numeriques automatiques 


Bilan : 


* fin rapport des comparaisons Verifications numeriques automatiques 
**********************************************************************************


******************************************
* Representations graphiques 
******************************************

Les representations graphiques des valeurs attendues par rapport aux valeurs obtenues (un graphique par donnee) sont rangees sous le repertoire  ./../output/lesJeuxDeTest/jeuErreurAbsenceParametre/representationsGraphiques/ 
ATTENTION, toutes les valeurs lues dans resuAttendu sont tracees mais parmi elles, n'ont aucun sens celles qui n'ont pas ete affectees (valeur  -777  (=VALEUR_INVALIDE) (cf cas ou la tolerance de test n'a pas ete affectee (valeur  -111  (=COMPARAISON_INACTIVE)). 
