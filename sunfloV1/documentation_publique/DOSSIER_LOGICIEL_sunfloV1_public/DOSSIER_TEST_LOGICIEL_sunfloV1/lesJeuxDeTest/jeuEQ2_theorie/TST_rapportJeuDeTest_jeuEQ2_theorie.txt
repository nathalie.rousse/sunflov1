
[TST_sunfloV1_VerifJeuDeTest_jeuEQ2_theorie.R]
Jeu appele/deroule sans MODE_DEBUG. Jeu appele/deroule pour generer un RAPPORT_ALLEGE. 

***********************************************************************************
* 
* Modele VLE sunfloV1, jeu de test jeuEQ2_theorie
* 
* Comparaison entre les resultats obtenus et les resultats attendus. 
* 
* Les verifications automatiques sont effectuees, tandis que celles qui sont a effectuer manuellement sont signalees. 
* 
* Le jeu de test jeuEQ2_theorie s'appuie sur le scenario de test scnSunfloV1intact.
* 
***********************************************************************************


******************************************
* Description du jeu de test  jeuEQ2_theorie , et verifications a effectuer 
******************************************

Jeu de verification de l'equation EQ2 de la publi, ou les resultats attendus correspondent theoriquement a l'equation de la publi. 

********* Les donnees/informations de test :

EQ2 selon la publi : 
[EQ2]    TTE = sum( (   Tm   -   Tb    ) * ( 1 + a ( 1 - W.TR    ) ) ) ; where      Tb = 4.8 °C and a = 0.1 
EQ2 traduite en noms du code vle : 
[EQ2] d/dt(TT_A2) = ( e.Tmoy - p.Tbase ) * ( 1 + a ( 1 - ve.FHTR ) )  ;  where p.Tbase = 4.8 °C and a = 0.1 

Pour memo, pseudo-code du traitement relatif a TT_A2 du cote du code du modele sunfloV1 : 
dTT_A2/dt = 0 for t >= jrecolte , 
 = 0 for TT_A0 < estimationTTentreSemisEtLevee_casPhaseSemisLeveeSimulee and dateLevee_casForcee = 0 , 
 = 0 for dateLevee_casForcee >0 and t < dateLevee_casForcee , 
 = Teff + AP by default 
avec :  
AP = 0.1 * Teff * (1-FHTR) 
Teff = 0 for Tmoy <Tbase ,  
 = Tmoy - Tbase by default 

Entrees de test injectees, parametres modifies... : voir le rapport TST_rapportScenario_ scnSunfloV1intact .txt 

Definition des resultats attendus : 
Le calcul des resultats attendus [EQ2] servant dans la verification est code sous R. 
Les resultats attendus calcules correspondent theoriquement a l'equation EQ2 de la publi. 
Ce calcul ne commence qu'a partir de la levee. 
Les indices y respectent TT_A2(j) = TT_A2(j-1) + ddt(j). 

********* Les VERIFICATIONS (automatiques et/ou manuelles) :

Automatique :  Verification numerique de la donnee vs.TT_A2 (du modele phenologie). Cette verification n'est faite qu'entre la levee et la recolte (voir pseudo-code du modele sunfloV1 plus haut). La tolerance d'ecart dans les comparaisons entre 'resu attendu' et 'resu obtenu' est 'resu attendu' x  0.002 . 

Manuelle : Pas de verification manuelle supplementaire. 

Remarque :  On peut voir dans le rapport de simulation TST_rapportScenario_ scnSunfloV1intact .txt que le parametre Tbase= 4.8 . Observation OK.

******************************************

******************************************
* Verifications numeriques automatiques 
******************************************

Les resultats attendus et tolerances de test associees sont archives dans les fichiers  ./../output/lesJeuxDeTest/jeuEQ2_theorie/complet_TST_sortiesAttendues_jeuEQ2_theorie.csv  et  ./../output/lesJeuxDeTest/jeuEQ2_theorie/complet_TST_toleranceTest_jeuEQ2_theorie.csv . Il s'agit des attendus complets, dont le contenu a ete defini par fichiers et/ou code R. 
ATTENTION, l'interpretation des resultats attendus depend des tolerances de test , qui doivent avoir ete affectees (via fichier ou code R). 

**********************************************************************************
* debut rapport des comparaisons Verifications numeriques automatiques 
 
 test    ok   pour variable  TT_A2

Bilan : 
 Sur l'ensemble des comparaisons numeriques de la variable  TT_A2 , 
 l'ecart de test calcule maximal vaut  2.37110035626597 , la tolerance de test maximale vaut  4.24405993647307 . 


* fin rapport des comparaisons Verifications numeriques automatiques 
**********************************************************************************


******************************************
* Representations graphiques 
******************************************

Les representations graphiques des valeurs attendues par rapport aux valeurs obtenues (un graphique par donnee) sont rangees sous le repertoire  ./../output/lesJeuxDeTest/jeuEQ2_theorie/representationsGraphiques/ 
ATTENTION, toutes les valeurs lues dans resuAttendu sont tracees mais parmi elles, n'ont aucun sens celles qui n'ont pas ete affectees (valeur  -777  (=VALEUR_INVALIDE) (cf cas ou la tolerance de test n'a pas ete affectee (valeur  -111  (=COMPARAISON_INACTIVE)). 
