
[TST_sunfloV1_VerifJeuDeTest_jeuEQ7.R]
Jeu appele/deroule sans MODE_DEBUG. Jeu appele/deroule pour generer un RAPPORT_ALLEGE. 

***********************************************************************************
* 
* Modele VLE sunfloV1, jeu de test jeuEQ7
* 
* Comparaison entre les resultats obtenus et les resultats attendus. 
* 
* Les verifications automatiques sont effectuees, tandis que celles qui sont a effectuer manuellement sont signalees. 
* 
* Le jeu de test jeuEQ7 s'appuie sur le scenario de test scnSunfloV1intact.
* 
***********************************************************************************


******************************************
* Description du jeu de test  jeuEQ7 , et verifications a effectuer 
******************************************

Jeu de verification de l'equation EQ7 de la publi, ou les resultats attendus correspondent theoriquement a l'equation de la publi. 

********* Les donnees/informations de test :

EQ7 selon la publi : 
[EQ7] zR = sum(dR * Tm) if zR < zPR ; else zR = zPR ; where dR = 0.7 

EQ7 traduite en noms du code vle : 
[EQ7] d/dt(vs.zRac) = p.VitCroiRac * ve.Tmoy if vs.zRac < p.zRac_max ; 
      else vs.zRac  = p.zRac_max ; 
      where p.VitCroiRac = 0.7 

Pour memo, pseudo-code du traitement relatif a zRac du cote du code du modele sunfloV1 : 
dzRac/dt = vRac ; si t = jsemis alors zRac=zSemis ; 
Valeur initiale de zRac = 0 
avec : 
vRac = 0 for t<=jsemis or t>=jrecolte 
     = 0 for Tmoy<=0 
     = 0 for zRac>=zBilan 
     = VitCroiRac * TMOY by default 

Entrees de test injectees, parametres modifies... : voir le rapport TST_rapportScenario_ scnSunfloV1intact .txt 

Definition des resultats attendus : 
Le calcul des resultats attendus [EQ7] servant dans la verification est code sous R. 
Les resultats attendus calcules correspondent theoriquement a l'equation EQ7 de la publi. 
La valeur initiale donnee a zRac est 0. 
Le calcul ne commence qu'a partir du semis. 
Les indices y respectent zRac(j) = zRac(j-1) + ddt(j). 

********* Les VERIFICATIONS (automatiques et/ou manuelles) :

Automatique :  Verification numerique de la donnee vi.zRac (du modele contrainte_eau). Etant donnees les differences entre l'algorithme du code vle et l'equation EQ7 de la publi, cette verification ne peut pas etre faite directement (voir pseudo-code du modele sunfloV1 plus haut). Ici la verification n'est faite que pour l'instant du semis. La tolerance d'ecart dans les comparaisons entre 'resu attendu' et 'resu obtenu' est definie dans le fichier csv de la tolerance de test. 

Manuelle : Pas de verification manuelle supplementaire. 

Remarque :  On peut voir dans le rapport de simulation TST_rapportScenario_ scnSunfloV1intact .txt que les parametres VitCroiRac= 0.7  et zRac_max= 1800  et zSemis= 30 . Observations OK.

******************************************

******************************************
* Verifications numeriques automatiques 
******************************************

Les resultats attendus et tolerances de test associees sont archives dans les fichiers  ./../output/lesJeuxDeTest/jeuEQ7/complet_TST_sortiesAttendues_jeuEQ7.csv  et  ./../output/lesJeuxDeTest/jeuEQ7/complet_TST_toleranceTest_jeuEQ7.csv . Il s'agit des attendus complets, dont le contenu a ete defini par fichiers et/ou code R. 
ATTENTION, l'interpretation des resultats attendus depend des tolerances de test , qui doivent avoir ete affectees (via fichier ou code R). 

**********************************************************************************
* debut rapport des comparaisons Verifications numeriques automatiques 

 test NOTOK   pour zRac (indice 27 , ie jourDeLannee 106 )
              valeurs obtenue :  30  / attendue :  11.165 
              ecart calcule :  18.835  / tolerance de test :  0 
 test    ok   pour variable  zRac

Bilan : 
 Sur l'ensemble des comparaisons numeriques de la variable  zRac , 
 l'ecart de test calcule maximal vaut  18.835 , la tolerance de test maximale vaut  0 . 


* fin rapport des comparaisons Verifications numeriques automatiques 
**********************************************************************************


******************************************
* Representations graphiques 
******************************************

Les representations graphiques des valeurs attendues par rapport aux valeurs obtenues (un graphique par donnee) sont rangees sous le repertoire  ./../output/lesJeuxDeTest/jeuEQ7/representationsGraphiques/ 
ATTENTION, toutes les valeurs lues dans resuAttendu sont tracees mais parmi elles, n'ont aucun sens celles qui n'ont pas ete affectees (valeur  -777  (=VALEUR_INVALIDE) (cf cas ou la tolerance de test n'a pas ete affectee (valeur  -111  (=COMPARAISON_INACTIVE)). 
