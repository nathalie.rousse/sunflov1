
[TST_sunfloV1_VerifJeuDeTest_jeuEQ15.R]
Jeu appele/deroule sans MODE_DEBUG. Jeu appele/deroule pour generer un RAPPORT_ALLEGE. 

***********************************************************************************
* 
* Modele VLE sunfloV1, jeu de test jeuEQ15
* 
* Comparaison entre les resultats obtenus et les resultats attendus. 
* 
* Les verifications automatiques sont effectuees, tandis que celles qui sont a effectuer manuellement sont signalees. 
* 
* Le jeu de test jeuEQ15 s'appuie sur le scenario de test scnSunfloV1intact.
* 
***********************************************************************************


******************************************
* Description du jeu de test  jeuEQ15 , et verifications a effectuer 
******************************************

Jeu de verification de l'equation EQ15 de la publi, ou les resultats attendus correspondent theoriquement a l'equation de la publi. 

********* Les donnees/informations de test :

EQ15 selon la publi : 
[EQ15] dTRC1 = fR * dPTR * W.TR if (zR > zC1) else dPTR * W.TR 
EQ15 traduite en noms du code vle : 
[EQ15] vi.vTRC1 = vi.fRacC1 * vi.vTRp * vs.FHTR if ( vs.zRac > p.zC1) else vi.vTRp *  vs.FHTR 

Pour memo, pseudo-code du traitement relatif a vTRC1 du cote du code du modele sunfloV1 : 
vi.vTRC1(j) = 0 for vi.C1(j) <= 0 ; 
vi.vTRC1(j) = vi.fRacC1(j) * vi.vTRp(j) * vs.FHTR(j) for vs.zRac(j) > p.zC1 ; 
vi.vTRC1(j) = vi.vTRp(j) * vs.FHTR(j) 

Entrees de test injectees, parametres modifies... : voir le rapport TST_rapportScenario_ scnSunfloV1intact .txt 

Definition des resultats attendus : 
Le calcul des resultats attendus [EQ15] servant dans la verification est code sous R. 
Les resultats attendus calcules correspondent theoriquement a l'equation EQ15 de la publi. 
Les indices y respectent vTRC1(j) = fonction_de(j). 

********* Les VERIFICATIONS (automatiques et/ou manuelles) :

Automatique :  Verification numerique de la donnee vs.vTRC1 (du modele contrainte_lumiere). Cette verification n'est faite que si C1(j) > 0.0 (voir pseudo-code du modele sunfloV1 plus haut). La tolerance d'ecart dans les comparaisons entre 'resu attendu' et 'resu obtenu' est 'resu attendu' x  7.5e-06 . 
Manuelle : Pas de verification manuelle supplementaire. 
Remarque :  La valeur de zC1 utilisee dans l'equation [EQ15] pour calculer les resultats attendus est zC1 =  300 (il n'y a pas de valeur zC1 donnee dans la publi).  On peut voir dans le rapport de simulation TST_rapportScenario_ scnSunfloV1intact .txt que le parametre zC1= 300 . Observation OK.

******************************************

******************************************
* Verifications numeriques automatiques 
******************************************

Les resultats attendus et tolerances de test associees sont archives dans les fichiers  ./../output/lesJeuxDeTest/jeuEQ15/complet_TST_sortiesAttendues_jeuEQ15.csv  et  ./../output/lesJeuxDeTest/jeuEQ15/complet_TST_toleranceTest_jeuEQ15.csv . Il s'agit des attendus complets, dont le contenu a ete defini par fichiers et/ou code R. 
ATTENTION, l'interpretation des resultats attendus depend des tolerances de test , qui doivent avoir ete affectees (via fichier ou code R). 

**********************************************************************************
* debut rapport des comparaisons Verifications numeriques automatiques 
 
 test    ok   pour variable  vTRC1

Bilan : 
 Sur l'ensemble des comparaisons numeriques de la variable  vTRC1 , 
 l'ecart de test calcule maximal vaut  8.88178419700125e-15 , la tolerance de test maximale vaut  2.17280495944024e-05 . 


* fin rapport des comparaisons Verifications numeriques automatiques 
**********************************************************************************


******************************************
* Representations graphiques 
******************************************

Les representations graphiques des valeurs attendues par rapport aux valeurs obtenues (un graphique par donnee) sont rangees sous le repertoire  ./../output/lesJeuxDeTest/jeuEQ15/representationsGraphiques/ 
ATTENTION, toutes les valeurs lues dans resuAttendu sont tracees mais parmi elles, n'ont aucun sens celles qui n'ont pas ete affectees (valeur  -777  (=VALEUR_INVALIDE) (cf cas ou la tolerance de test n'a pas ete affectee (valeur  -111  (=COMPARAISON_INACTIVE)). 
