
[TST_sunfloV1_VerifJeuDeTest_jeuSansDessication.R]
Jeu appele/deroule sans MODE_DEBUG. Jeu appele/deroule pour generer un RAPPORT_ALLEGE. 

***********************************************************************************
* 
* Modele VLE sunfloV1, jeu de test jeuSansDessication
* 
* Comparaison entre les resultats obtenus et les resultats attendus. 
* 
* Les verifications automatiques sont effectuees, tandis que celles qui sont a effectuer manuellement sont signalees. 
* 
* Le jeu de test jeuSansDessication s'appuie sur le scenario de test scnSansDessication.
* 
***********************************************************************************


******************************************
* Description du jeu de test  jeuSansDessication , et verifications a effectuer 
******************************************

Jeu de verification du bon deroulement de la simulation dans le cas ou la recolte a lieu des la phase de maturation. 

********* Les donnees/informations de test :

Les conditions de la simulation sont nominales : elles ne provoquent aucune erreur dans le deroulement de la simulation et elles conduisent la plante a evoluer entre le semis et la recolte de la maniere classique prevue par le modele dans le cas ou elle ne passe pas par la phase dessication. 

Entrees de test injectees, parametres modifies... : voir le rapport TST_rapportScenario_ scnSansDessication .txt 

Definition des resultats attendus : Les resultats attendus ont trait au comportement de la simulation et du modele en cas de bon deroulement, ils reposent essentiellement sur le contenu des rapports de simulation. 

********* Les VERIFICATIONS (automatiques et/ou manuelles) :

Automatique : Pas de verification automatique. 

Verifications manuelles :  


Manuelle :  Verification de la fin/sortie normale de la simulation : verifier dans le rapport TST_rapportScenario_scnSansDessication.txt que dans ses 10 dernieres lignes il apparait les messages 'No errors detected' et '100% tests passed, 0 tests failed out of 1'. Noter ici le resultat de la verification : verification OK.
 
Manuelle :  Verification de l'absence de detection d'erreur en cours de simulation : verifier dans le rapport TST_rapportScenario_scnSansDessication.txt l'absence du mot cle 'ERREUR' qui sert a signaler une erreur detectee. Noter ici le resultat de la verification : verification OK.

Manuelle :  Verification de l'evolution classique de la phase phenologique de la plante, sachant qu'il s'agit d'une simulation sans phase DESSICATION : verifier dans le rapport RAPPORT_Phenologie.TXT que la phase phenologique de la plante evolue bien de NONSEMEE a GERMINATION puis de GERMINATION a JUVENILE puis de JUVENILE a CROISSANCEACTIVE puis de CROISSANCEACTIVE a FLORAISON puis de FLORAISON a MATURATION puis de MATURATION a RECOLTEE. Verifier de plus dans le rapport TST_rapportScenario_ scnSansDessication .txt  ainsi que dans le rapport RAPPORT_Phenologie.TXT la presence du message d'avertissement contenant le mot cle ' Avertissement ', correspondant au fait qu'il s'agit d'une simulation sans phase DESSICATION (message du type 'la recolte a lieu alors que la plante est en phase phenologique MATURATION').  Noter ici le resultat de la verification : verification OK. 
 
Manuelle :  Verification du contenu du rapport RAPPORT_CroissancePlante.TXT : verifier qu'il contient bien les informations 'LAI, indice foliaire, lors du  passage de la phase phenologique plante JUVENILE a CROISSANCEACTIVE (stade E1, bouton etoile)', 'LAI, indice foliaire, lors du  passage de la phase phenologique plante CROISSANCEACTIVE a FLORAISON (stade F1, debut floraison)', 'TDM, biomasse totale (g/m2) lors du  passage de la phase phenologique plante MATURATION a RECOLTEE (qui correspond a la recolte)'.  Noter ici le resultat de la verification : verification OK. 
 
Manuelle :  Verification du contenu du rapport RAPPORT_ContrainteEau.TXT : verifier qu'il contient bien les informations de cumuls calcules depuis la levee 'ETR cumule (mm)', 'ETM cumule (mm)', 'rapport ETR cumule / ETM cumule', 'Pluie cumulee (mm)', 'ETP cumule (mm)'.  Noter ici le resultat de la verification : verification OK. 
 
Manuelle :  Verification du contenu du rapport RAPPORT_ContrainteAzote.TXT : verifier qu'il contient bien les informations 'Nabs (kg N/ha), lors du  passage de la phase phenologique plante CROISSANCEACTIVE a FLORAISON (stade F1, debut floraison)', 'Nabs (kg N/ha), lors du  passage de la phase phenologique plante MATURATION a RECOLTEE (qui correspond a la recolte)'.  Noter ici le resultat de la verification : verification OK. 
 
Manuelle :  Verification du contenu du rapport RAPPORT_ElaborationQualite.TXT : verifier qu'il contient bien les informations 'TH, teneur en huile lors du  passage de la phase phenologique plante MATURATION a RECOLTEE (qui correspond a la recolte)'.  Noter ici le resultat de la verification : verification OK. 
 
Manuelle :  Verification du contenu du rapport RAPPORT_ElaborationRendement.TXT : verifier qu'il contient bien les informations 'TDM, biomasse totale (g/m2) lors du  passage de la phase phenologique plante CROISSANCEACTIVE a FLORAISON (stade F1, debut floraison)', 'INN, lors du  passage de la phase phenologique plante CROISSANCEACTIVE a FLORAISON (stade F1, debut floraison)', 'IRs, indice de recolte lors du  passage de la phase phenologique plante MATURATION a RECOLTEE (qui correspond a la recolte)', 'RDT, rendement (q/ha) lors du  passage de la phase phenologique plante MATURATION a RECOLTEE (qui correspond a la recolte)' et les jours de stress cumules (covariables statistiques) 'JSE correspondant aux Jours de stress en phase vegetative (E1 - F1)', 'JSF correspondant aux Jours de stress en floraison (F1 - M0)', 'JSM correspondant aux Jours de stress après la floraison (M0 - M3)'.  Noter ici le resultat de la verification : verification OK. 
 
Manuelle :  Verification du contenu du rapport RAPPORT_Diagnostic.TXT : verifier qu'il contient bien les informations de jours de stress cumules relatifs a la floraison 'ISH1 correspondant aux Jours de stress jusqu'a la floraison', 'ISH2 correspondant aux Jours de stress autour de la floraison', 'ISH3 correspondant aux Jours de stress après la floraison'.  Noter ici le resultat de la verification : verification OK. 
 
Manuelle :  Verification de la representation graphique de la phase phenologique de la plante : verifier a vue d'oeil que la representation graphique de PhasePhenoPlante (elle se trouve dans le fichier PhasePhenoPlante_bis.pdf puisque dans ce jeu de test PhasePhenoPlante ne fait pas l'objet de comparaisons numeriques automatiques) correspond bien a son evolution decrite dans le rapport RAPPORT_Phenologie.TXT ; pour interpreter les valeurs numeriques de PhasePhenoPlante s'aider de la legende donnee dans le rapport TST_rapportScenario_scnSansDessication.txt. Noter ici le resultat de la verification : verification OK (notamment on passe directement de valeur 5 a 7 sans passer par 6 qui correspond a DESSICATION).
 

******************************************

******************************************
* Verifications numeriques automatiques 
******************************************

Les resultats attendus et tolerances de test associees sont archives dans les fichiers  ./../output/lesJeuxDeTest/jeuSansDessication/complet_TST_sortiesAttendues_jeuSansDessication.csv  et  ./../output/lesJeuxDeTest/jeuSansDessication/complet_TST_toleranceTest_jeuSansDessication.csv . Il s'agit des attendus complets, dont le contenu a ete defini par fichiers et/ou code R. 
ATTENTION, l'interpretation des resultats attendus depend des tolerances de test , qui doivent avoir ete affectees (via fichier ou code R). 

**********************************************************************************
* debut rapport des comparaisons Verifications numeriques automatiques 


Bilan : 


* fin rapport des comparaisons Verifications numeriques automatiques 
**********************************************************************************


******************************************
* Representations graphiques 
******************************************

Les representations graphiques des valeurs attendues par rapport aux valeurs obtenues (un graphique par donnee) sont rangees sous le repertoire  ./../output/lesJeuxDeTest/jeuSansDessication/representationsGraphiques/ 
ATTENTION, toutes les valeurs lues dans resuAttendu sont tracees mais parmi elles, n'ont aucun sens celles qui n'ont pas ete affectees (valeur  -777  (=VALEUR_INVALIDE) (cf cas ou la tolerance de test n'a pas ete affectee (valeur  -111  (=COMPARAISON_INACTIVE)). 
