
### Accès à la **[documentation en ligne](https://nathalie.rousse.pages.mia.inra.fr/sunflov1)  du logiciel sunfloV1**

# Projet logiciel sunfloV1

Le modèle tournesol **SUNFLO** est un modèle de culture pour le tournesol, permettant de simuler des interactions entre variétés, milieu et conduite de culture. Pour les besoins de divers projets de recherche et de développement, l'**Unité Mixte Technologique Tournesol** avait besoin de disposer du modèle SUNFLO sous une forme informatique réutilisable et évolutive telle que le permet la plate-forme **RECORD** (http://www.inra.fr/record). Le modèle logiciel **sunfloV1** correspond au développement du modèle tournesol SUNFLO sous la plate-forme RECORD.

Le code source du logiciel sunfloV1 se trouve dans le dépôt de sources Git du projet 'sunfloV1' hébergé sous la forge forgemia https://forgemia.inra.fr/record/recordsunflo.

### Documentation

La documentation produite en cours de projet (fin 2009 - début 2011) est en partie privée (réservée à l'équipe projet) et en partie publique. Seule la partie publique est disponible ici.

La documentation du logiciel sunfloV1 est contenue dans le répertoire sunfloV1.

Elle comprend :

- Dossier d'information

  - La liste des documents du projet logiciel sunfloV1, qui recense l'ensemble des documents qui suivent *(et documents privés)*.

  - La documentation logicielle, qui repose en partie sur le dossier logiciel sunfloV1.

  - Le dossier logiciel sunfloV1 dans sa version allégée.

- Formation

  Une journée de formation a eu lieu en mars 2011 pour les tout premiers utilisateurs du modèle logiciel sunfloV1 (une dizaine d'agents du CETIOM et de l'INRA) :

  - Formation Utiliser le modèle tournesol SUNFLO sous l'interface graphique GVLE de la plate-forme RECORD.


### Mise en ligne de la documentation du logiciel sunfloV1 :

Copier le répertoire sunfloV1 (qui contient index.html et le reste de la documentation).

**CI/CD** : La documentation du logiciel sunfloV1 est publiée sous : **[Online Documentation](https://nathalie.rousse.pages.mia.inra.fr/sunflov1)**.

